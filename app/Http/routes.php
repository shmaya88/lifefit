<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::group(['middleware' => 'auth.middleware'], function () {
//     Route::any('/', 'Main/MainController@index');

//     // Route::get('user/profile', function () {
//     //     // Uses Auth Middleware
//     // });
// });

// Route::post('posts/{something}', function () {
//     //
// })->middleware(['first', 'second']);

// Route::get('/', 'WelcomeController@index');

// Route::get('home', 'HomeController@index');
// Route::get('/', 'HomeController@index');

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

// Route::get('auth/login', 'Auth\AuthController@getLogin');
// Route::post('auth/login', 'Auth\AuthController@postLogin');
// Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Route::get('auth/register', 'Auth\AuthController@getRegister');
// Route::post('auth/register', 'Auth\AuthController@postRegister');