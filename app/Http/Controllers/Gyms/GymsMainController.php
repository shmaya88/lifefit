<?php namespace App\Http\Controllers\Gyms;

use App\Http\Requests;
use App\Http\Controllers\Controller;

# Services
use App\Services\Common\MainCommonService;
use App\Services\Gyms\GymsService;

use Illuminate\Http\Request;

use View;

class GymsMainController extends Controller {

	# ---------------------------------------------------------------------------
	public function gymsPage(){
		return view('modules/gyms/gyms_main', array('SVARS' => MainCommonService::getSvars('json')));
	}

	# ---------------------------------------------------------------------------
	public function gymsBranchesPage(){
		return view('modules/gyms/gyms_branches_main', array('SVARS' => MainCommonService::getSvars('json')));
	}

	# ---------------------------------------------------------------------------
	public function getGymsList(Request $request){
		$request_params = $request->all();
		
		$gymsService = new GymsService;

		$gyms_data = $gymsService->getGymsList($request_params);
		if(!$gyms_data) return response()->json(['success' => false, 'msg' => $gymsService->getError()]);
		
		$gyms_data['success'] = true;
		$gyms_data['msg'] = '';

		return response()->json($gyms_data);
	}

	# ---------------------------------------------------------------------------
	public function getGymsBranchesList(Request $request){
		$request_params = $request->all();

		$gymsService = new GymsService;

		$gyms_data = $gymsService->getGymsBranchesList($request_params);
		if(!$gyms_data) return response()->json(['success' => false, 'msg' => $gymsService->getError()]);
		
		$gyms_data['success'] = true;
		$gyms_data['msg'] = '';

		return response()->json($gyms_data);
	}

	# ---------------------------------------------------------------------------
	public function addGym(Request $request){
		$request_params = $request->all();

		$gymsService = new GymsService;

		$gym_id = $gymsService->addGym($request_params);
		if(!$gym_id) return response()->json(['success' => false, 'msg' => $gymsService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "חברה/רשת נוספה בהצלחה", 'gym_id' => $gym_id]);
	}

	# ---------------------------------------------------------------------------
	public function addGymBranch(Request $request){
		$request_params = $request->all();

		$gymsService = new GymsService;

		$gym_branch_id = $gymsService->addGymBranch($request_params);
		if(!$gym_branch_id) return response()->json(['success' => false, 'msg' => $gymsService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "סניף נוסף בהצלחה", 'gym_branch_id' => $gym_branch_id]);
	}

	# ---------------------------------------------------------------------------
	public function editGym(Request $request){
		$request_params = $request->all();

		$gymsService = new GymsService;

		$result = $gymsService->editGym($request_params);
		if(!$result) return response()->json(['success' => false, 'msg' => $gymsService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "חברה/רשת עודכנה בהצלחה"]);
	}

	# ---------------------------------------------------------------------------
	public function editGymBranch(Request $request){
		$request_params = $request->all();

		$gymsService = new GymsService;

		$result = $gymsService->editGymBranch($request_params);
		if(!$result) return response()->json(['success' => false, 'msg' => $gymsService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "הסניף עודכן בהצלחה"]);
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreGym(Request $request){
		$request_params = $request->all();

		$gymsService = new GymsService;

		$result = $gymsService->deleteRestoreGym($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $gymsService->getError()]);
		
		if($request_params['action'] == 'delete_gym') $msg = 'המחיקה בוצעה בהצלחה';
		else $msg = 'השחזור בוצע בהצלחה';

		return response()->json([
				'success' => true, 
				'msg' => $msg
			]);
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreGymBranch(Request $request){
		$request_params = $request->all();

		$gymsService = new GymsService;

		$result = $gymsService->deleteRestoreGymBranch($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $gymsService->getError()]);
		
		if($request_params['action'] == 'delete_gym_branch') $msg = 'המחיקה בוצעה בהצלחה';
		else $msg = 'השחזור בוצע בהצלחה';

		return response()->json([
				'success' => true, 
				'msg' => $msg
			]);
	}
}
