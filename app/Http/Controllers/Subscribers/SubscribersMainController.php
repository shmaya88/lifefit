<?php namespace App\Http\Controllers\Subscribers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

# Services
use App\Services\Common\MainCommonService;
use App\Services\Subscribers\SubscribersService;

use Illuminate\Http\Request;

use View;

class SubscribersMainController extends Controller {

	# ---------------------------------------------------------------------------
	public function subscribersPage(){
		return view('modules/subscribers/subscribers_main', array('SVARS' => MainCommonService::getSvars('json')));
	}

	# ---------------------------------------------------------------------------
	public function getSubscribersList(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$subscribers_data = $subscribersService->subscribersList($request_params);
		if(!$subscribers_data) return response()->json(['success' => false, 'msg' => $subscribersService->getError()]);
		
		$subscribers_data['success'] = true;
		$subscribers_data['msg'] = '';

		return response()->json($subscribers_data);
	}

	# ---------------------------------------------------------------------------
	public function addSubscription(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$subscription_id = $subscribersService->addSubscription($request_params);
		if(!$subscription_id) return response()->json(['success' => false, 'msg' => $subscribersService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "המנוי נוסף בהצלחה", 'subscription_id' => $subscription_id]);
	}

	# ---------------------------------------------------------------------------
	public function editSubscription(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$result = $subscribersService->editSubscription($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $subscribersService->getError()]);

		return response()->json(['success' => true, 'msg' => "המנוי עודכן בהצלחה"]);
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreSubscription(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$result = $subscribersService->deleteRestoreSubscription($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $subscribersService->getError()]);
		
		if($request_params['action'] == 'delete_subscription') $msg = 'המחיקה בוצעה בהצלחה';
		else $msg = 'השחזור בוצע בהצלחה';

		return response()->json([
				'success' => true, 
				'msg' => $msg
			]);
	}

	# ---------------------------------------------------------------------------
	public function setSubscriptionUser(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$result = $subscribersService->setSubscriptionUser($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $subscribersService->getError()]);

		return response()->json(['success' => true, 'msg' => "המטפל במנוי עודכן בהצלחה"]);
	}

	# ---------------------------------------------------------------------------
	public function addSubscriptionTraining(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$subscription_traning_id = $subscribersService->addSubscriptionTraining($request_params);
		if(!$subscription_traning_id) {
			return response()->json(['success' => false, 'msg' => $subscribersService->getError()]);
		}
		
		return response()->json([
			'success' => true, 
			'msg' => "האימון/ביקור נוסף בהצלחה", 
			'subscription_traning_id' => $subscription_traning_id
		]);
	}

	# ---------------------------------------------------------------------------
	public function editSubscriptionTraining(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$result = $subscribersService->editSubscriptionTraining($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $subscribersService->getError()]);

		return response()->json(['success' => true, 'msg' => "האימון/ביקור עודכן בהצלחה"]);
	}

	# ---------------------------------------------------------------------------
	public function deleteSubscriptionTraining(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$result = $subscribersService->deleteSubscriptionTraining($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $subscribersService->getError()]);

		return response()->json([
				'success' => true, 
				'msg' => 'המחיקה בוצעה בהצלחה'
			]);
	}

	# ---------------------------------------------------------------------------
	public function getSubscribersTrainingList(Request $request){
		$request_params = $request->all();

		$subscribersService = new SubscribersService;

		$subscribers_data = $subscribersService->subscribersTrainingList($request_params);
		if(!$subscribers_data) return response()->json(['success' => false, 'msg' => $subscribersService->getError()]);
		
		$subscribers_data['success'] = true;
		$subscribers_data['msg'] = '';

		return response()->json($subscribers_data);
	}
}
