<?php namespace App\Http\Controllers\SharedMessages;

use App\Http\Requests;
use App\Http\Controllers\Controller;

# Services
use App\Services\Common\MainCommonService;
use App\Services\SharedMessages\SharedMessagesService;

use Illuminate\Http\Request;

use View;

class SharedMessagesController extends Controller {

	# ---------------------------------------------------------------------------
	public function sharedMessagesPage(){
		return view(
			'modules/shared_messages/shared_messages_main', 
			array('SVARS' => MainCommonService::getSvars('json'))
		);
	}

	# ---------------------------------------------------------------------------
	public function getSharedMessageList(Request $request){
		$request_params = $request->all();
		
		$sharedMessagesService = new SharedMessagesService;

		$shared_messages_data = $sharedMessagesService->sharedMessageList($request_params);
		if(!$shared_messages_data) {
			return response()->json([
				'success' => false, 
				'msg' => $sharedMessagesService->getError()
			]);
		}
		
		$shared_messages_data['success'] = true;
		$shared_messages_data['msg'] = '';

		return response()->json($shared_messages_data);
	}

	# ---------------------------------------------------------------------------
	public function addSharedMessage(Request $request){
		$request_params = $request->all();
		
		$sharedMessagesService = new SharedMessagesService;

		$message_id = $sharedMessagesService->addSharedMessage($request_params);
		if(!$message_id) return response()->json(['success' => false, 'msg' => $sharedMessagesService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "ההודעה נוספה בהצלחה", 'message_id' => $message_id]);
	}

	# ---------------------------------------------------------------------------
	public function editSharedMessage(Request $request){
		$request_params = $request->all();
		
		$sharedMessagesService = new SharedMessagesService;

		$result = $sharedMessagesService->editSharedMessage($request_params);
		if(!$result) return response()->json(['success' => false, 'msg' => $sharedMessagesService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "ההודעה עודכנה בהצלחה"]);
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreSharedMessage(Request $request){
		$request_params = $request->all();
		
		$sharedMessagesService = new SharedMessagesService;

		$result = $sharedMessagesService->deleteRestoreSharedMessage($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $sharedMessagesService->getError()]);
		
		if($request_params['action'] == 'delete_shared_message') $msg = 'המחיקה בוצעה בהצלחה';
		else $msg = 'השחזור בוצע בהצלחה';

		return response()->json([
				'success' => true, 
				'msg' => $msg
			]);
	}
}
