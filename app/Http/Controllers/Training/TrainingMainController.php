<?php namespace App\Http\Controllers\Training;

use App\Http\Requests;
use App\Http\Controllers\Controller;

# Services
use App\Services\Common\MainCommonService;
use App\Services\Training\TrainingService;

use Illuminate\Http\Request;

use View;

class TrainingMainController extends Controller {

	# ---------------------------------------------------------------------------
	public function trainingPage(){
		return view('modules/training/training_main', array('SVARS' => MainCommonService::getSvars('json')));
	}

	# ---------------------------------------------------------------------------
	public function getTrainingList(Request $request){
		$request_params = $request->all();

		$trainingService = new TrainingService;

		$training_data = $trainingService->trainingList($request_params);
		if(!$training_data) return response()->json(['success' => false, 'msg' => $trainingService->getError()]);
		
		$training_data['success'] = true;
		$training_data['msg'] = '';

		return response()->json($training_data);
	}

	# ---------------------------------------------------------------------------
	public function addTraining(Request $request){
		$request_params = $request->all();

		$trainingService = new TrainingService;

		$training_id = $trainingService->addTraining($request_params);
		if(!$training_id) return response()->json(['success' => false, 'msg' => $trainingService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "תבנית האימון נוספה בהצלחה", 'training_id' => $training_id]);
	}

	# ---------------------------------------------------------------------------
	public function editTraining(Request $request){
		$request_params = $request->all();

		$trainingService = new TrainingService;

		$result = $trainingService->editTraining($request_params);
		if(!$result) return response()->json(['success' => false, 'msg' => $trainingService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "תבנית האימון עודכנה בהצלחה"]);
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreTraining(Request $request){
		$request_params = $request->all();

		$trainingService = new TrainingService;

		$result = $trainingService->deleteRestoreTraining($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $trainingService->getError()]);
		
		if($request_params['action'] == 'delete_training') $msg = 'המחיקה בוצעה בהצלחה';
		else $msg = 'השחזור בוצע בהצלחה';

		return response()->json([
				'success' => true, 
				'msg' => $msg
			]);
	}
}
