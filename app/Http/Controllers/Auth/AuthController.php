<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

# Services
use App\Services\Users\UserService;

use Illuminate\Http\Request;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users.
	|
	*/

	public function loginUser(Request $request){
		$request_params = $request->all();

		$userService = new UserService;

		$result = $userService->loginUser($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $userService->getError()]);

		return response()->json(['success' => true,'msg' => '']);
	}


}
