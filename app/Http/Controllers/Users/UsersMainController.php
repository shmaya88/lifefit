<?php namespace App\Http\Controllers\Users;

use App\Http\Requests;
use App\Http\Controllers\Controller;

# Services
use App\Services\Users\UserService;
use App\Services\Common\MainCommonService;

use Illuminate\Http\Request;

use View;

class UsersMainController extends Controller {

	# ---------------------------------------------------------------------------
	public function usersPage(){
		return view('modules/users/users_main', array('SVARS' => MainCommonService::getSvars('json')));
	}	

	# ---------------------------------------------------------------------------
	public function getUsersList(Request $request){
		$request_params = $request->all();

		$userService = new UserService;

		$users_data = $userService->usersList($request_params);
		if(!$users_data) return response()->json(['success' => false, 'msg' => $userService->getError()]);
		
		$users_data['success'] = true;
		$users_data['msg'] = '';

		return response()->json($users_data);
	}

	# ---------------------------------------------------------------------------
	public function addUser(Request $request){
		$request_params = $request->all();

		$userService = new UserService;

		$user_id = $userService->addUser($request_params);
		if(!$user_id) return response()->json(['success' => false, 'msg' => $userService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "המשתמש נוסף בהצלחה", 'user_id' => $user_id]);
	}

	# ---------------------------------------------------------------------------
	public function editUser(Request $request){
		$request_params = $request->all();

		$userService = new UserService;

		$result = $userService->editUser($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $userService->getError()]);

		return response()->json(['success' => true, 'msg' => "המשתמש עודכן בהצלחה"]);
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreUser(Request $request){
		$request_params = $request->all();

		$userService = new UserService;

		$result = $userService->deleteRestoreUser($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $userService->getError()]);
		
		if(in_array($request_params['action'], ['delete_user','delete_forever_user'])) $msg = 'המחיקה בוצעה בהצלחה';
		else $msg = 'השחזור בוצע בהצלחה';

		return response()->json([
				'success' => true, 
				'msg' => $msg
			]);
	}
}
