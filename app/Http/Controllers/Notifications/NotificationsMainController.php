<?php namespace App\Http\Controllers\Notifications;

use App\Http\Requests;
use App\Http\Controllers\Controller;

# Services
use App\Services\Common\MainCommonService;
use App\Services\Notifications\NotificationsService;

use Illuminate\Http\Request;

use View;

class NotificationsMainController extends Controller {

	# ---------------------------------------------------------------------------
	public function notificationsServicesPage(){
		return view(
			'modules/support/notifications_services', 
			array('SVARS' => MainCommonService::getSvars('json'))
		);
	}

	# ---------------------------------------------------------------------------
	public function supportPage(){
		return view(
			'modules/support/support_page', 
			array('SVARS' => MainCommonService::getSvars('json'))
		);
	}

	# ---------------------------------------------------------------------------
	public function getNotificationsList(Request $request){
		$request_params = $request->all();

		$notificationsService = new NotificationsService;

		$notifications_data = $notificationsService->notificationsList($request_params);
		if(!$notifications_data) return response()->json(['success' => false, 'msg' => $notificationsService->getError()]);
		
		$notifications_data['success'] = true;
		$notifications_data['msg'] = '';

		return response()->json($notifications_data);
	}

	# ---------------------------------------------------------------------------
	public function addNotification(Request $request){
		$request_params = $request->all();

		$notificationsService = new NotificationsService;

		$notification_id = $notificationsService->addNotification($request_params);
		if(!$notification_id) return response()->json(['success' => false, 'msg' => $notificationsService->getError()]);
		
		return response()->json(['success' => true, 'msg' => "ההודעה נשלחה בהצלחה", 'notification_id' => $notification_id]);
	}

	# ---------------------------------------------------------------------------
	public function setNotificationStatus(Request $request){
		$request_params = $request->all();

		$notificationsService = new NotificationsService;

		$result = $notificationsService->setNotificationStatus($request_params);
		if(!$result) return response()->json(['success' => false,'msg' => $notificationsService->getError()]);

		return response()->json([
				'success' => true, 
				'msg' => 'הפעולה בוצעה בהצלחה'
			]);
	}
}
