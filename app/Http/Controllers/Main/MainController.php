<?php namespace App\Http\Controllers\Main;

use App\Http\Requests;
use App\Http\Controllers\Controller;

# Services
use App\Services\Common\MainCommonService;
use App\Services\SharedMessages\SharedMessagesService;

# Helpers
use App\Helpers\UserHelper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use DB;

class MainController extends Controller {

	public function startPage(){
		if(Auth::user()) {
			\Session::forget('SVARS'.Auth::user()->id);
		}

		Auth::logout();

		return view('start_page/start_page');
	}

	public function homePage(){
		$svars = MainCommonService::getSvars();
		$auth_user = $svars['user_data'];

		$sharedMessagesService = new SharedMessagesService;
		$shared_messages_data = $sharedMessagesService->sharedMessageList(['get_general_messages' => true]);

		return view(
			'modules/home/home', 
			array(
				'SVARS' => json_encode($svars),
				'SharedMessagesList' => json_encode($shared_messages_data['data'])
			)
		);
	}

}
