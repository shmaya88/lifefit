<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::any('/', 'Main\MainController@startPage');
Route::any('/logout', 'Main\MainController@startPage');
Route::any('/logout_tools', 'Main\MainController@startPage');
Route::post('/login_user', 'Auth\AuthController@loginUser');

Route::group(['middleware' => 'auth.middleware'], function () { 

    Route::any('/home_page', 'Main\MainController@homePage');
    
    # Users routes
    Route::group(['prefix' => 'users'], function () {
        $users_c = 'Users\UsersMainController';

        Route::any('/', "$users_c@usersPage");
        Route::post('/get_users_list', "$users_c@getUsersList");
        Route::post('/add_user', "$users_c@addUser");
        Route::post('/edit_user', "$users_c@editUser");
        Route::post('/do_delete_restore_user_action', "$users_c@deleteRestoreUser"); 
    });

    # Support routes
    Route::group(['prefix' => 'support'], function () {
        $notification_c = 'Notifications\NotificationsMainController';

        Route::any('/notifications_services', "$notification_c@notificationsServicesPage");
        Route::any('/support_page', "$notification_c@supportPage");
        Route::post('/get_notifications_list', "$notification_c@getNotificationsList");
        Route::post('/add_notification', "$notification_c@addNotification");
        Route::post('/set_notification_status', "$notification_c@setNotificationStatus");
    });
    
    # Subscribers routes
    Route::group(['prefix' => 'subscribers'], function () {
        $subscribers_c = 'Subscribers\SubscribersMainController';

        Route::any('/', "$subscribers_c@subscribersPage");
        Route::post('/get_subscribers_list', "$subscribers_c@getSubscribersList");
        Route::post('/add_subscription', "$subscribers_c@addSubscription");
        Route::post('/edit_subscription', "$subscribers_c@editSubscription");
        Route::post('/do_delete_restore_subscription_action', "$subscribers_c@deleteRestoreSubscription");
        Route::post('/set_subscription_user', "$subscribers_c@setSubscriptionUser");
        Route::post('/add_sub_training', "$subscribers_c@addSubscriptionTraining");
        Route::post('/edit_sub_training', "$subscribers_c@editSubscriptionTraining");
        Route::post('/delete_sub_training', "$subscribers_c@deleteSubscriptionTraining");
        Route::post('/get_subscription_training_list', "$subscribers_c@getSubscribersTrainingList");
    });

    # Training routes
    Route::group(['prefix' => 'training'], function () {
        $training_c = 'Training\TrainingMainController';

        Route::any('/', "$training_c@trainingPage");
        Route::post('/get_training_list', "$training_c@getTrainingList");
        Route::post('/add_training', "$training_c@addTraining");
        Route::post('/edit_training', "$training_c@editTraining");
        Route::post('/do_delete_restore_training_action', "$training_c@deleteRestoreTraining");
    });

    # Gyms routes
    Route::group(['prefix' => 'gyms'], function () {
        $gyms_c = 'Gyms\GymsMainController';

        Route::any('/', "$gyms_c@gymsPage");
        Route::post('/get_gyms_list', "$gyms_c@getGymsList");
        Route::post('/add_gym', "$gyms_c@addGym");
        Route::post('/edit_gym', "$gyms_c@editGym");
        Route::post('/do_delete_restore_gym_action', "$gyms_c@deleteRestoreGym");
    });

    # Gyms branches routes
    Route::group(['prefix' => 'gyms_branches'], function () {
        $gyms_branches_c = 'Gyms\GymsMainController';

        Route::any('/', "$gyms_branches_c@gymsBranchesPage");
        Route::post('/get_gyms_branches_list', "$gyms_branches_c@getGymsBranchesList");
        Route::post('/add_gym_branch', "$gyms_branches_c@addGymBranch");
        Route::post('/edit_gym_branch', "$gyms_branches_c@editGymBranch");
        Route::post('/do_delete_restore_gym_branch_action', "$gyms_branches_c@deleteRestoreGymBranch");
    });

    # Shared messages branches routes
    Route::group(['prefix' => 'shared_messages'], function () {
        $shared_messages_c = 'SharedMessages\SharedMessagesController';

        Route::any('/', "$shared_messages_c@sharedMessagesPage");
        Route::post('/get_shared_messages_list', "$shared_messages_c@getSharedMessageList");
        Route::post('/add_shared_message', "$shared_messages_c@addSharedMessage");
        Route::post('/edit_shared_message', "$shared_messages_c@editSharedMessage");
        Route::post('/do_delete_restore_shared_message_action', "$shared_messages_c@deleteRestoreSharedMessage");
    });
});
