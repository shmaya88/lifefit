<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

# Services
use App\Services\Users\UserService;

class AuthenticateMiddleware {

	protected $auth;

	public function __construct(Guard $auth) {
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {

		$UserService = new UserService;

		if ($this->auth->guest() OR !$UserService->checkLastLogin()) {
			if($request->ajax()) return response('Unauthorized.', 401);
			else return redirect()->guest('/');
		}
		
		if(!$UserService->updateLastLogin()) return false;

		return $next($request);
	}

}
