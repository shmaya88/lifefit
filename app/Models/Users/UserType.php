<?php namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model{

	protected $table = 'users_types';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
