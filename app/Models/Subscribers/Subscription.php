<?php namespace App\Models\Subscribers;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model{

	protected $table = 'subscribers';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
