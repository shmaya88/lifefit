<?php namespace App\Models\Subscribers;

use Illuminate\Database\Eloquent\Model;

class ‏‏SubscriptionTraning extends Model{

	protected $table = 'subscribers_traning';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
