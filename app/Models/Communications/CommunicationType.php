<?php namespace App\Models\Communications;

use Illuminate\Database\Eloquent\Model;

class CommunicationType extends Model{

	protected $table = 'communications_types';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
