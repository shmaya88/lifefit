<?php namespace App\Models\Communications;

use Illuminate\Database\Eloquent\Model;

class Communication extends Model{

	protected $table = 'communications';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
