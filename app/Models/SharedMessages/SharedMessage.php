<?php namespace App\Models\SharedMessages;

use Illuminate\Database\Eloquent\Model;

class SharedMessage extends Model{

	protected $table = 'shared_messages';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
