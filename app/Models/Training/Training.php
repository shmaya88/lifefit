<?php namespace App\Models\Training;

use Illuminate\Database\Eloquent\Model;

class Training extends Model{

	protected $table = 'training_templates';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
