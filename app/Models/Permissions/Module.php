<?php namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Model;

class Module extends Model{

	protected $table = 'modules';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
