<?php namespace App\Models\Permissions;

use Illuminate\Database\Eloquent\Model;

class ModulePermission extends Model{

	protected $table = 'modules_permissions';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
