<?php namespace App\Models\Notifications;

use Illuminate\Database\Eloquent\Model;

class ServiceNotifications extends Model{

	protected $table = 'service_notifications';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
