<?php namespace App\Models\Addresses;

use Illuminate\Database\Eloquent\Model;

class Address extends Model{

	protected $table = 'addresses';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
