<?php namespace App\Models\Addresses;

use Illuminate\Database\Eloquent\Model;

class ‏‏AddressType extends Model{

	protected $table = 'addresses_types';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
