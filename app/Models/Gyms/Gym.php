<?php namespace App\Models\Gyms;

use Illuminate\Database\Eloquent\Model;

class Gym extends Model{

	protected $table = 'gyms';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
