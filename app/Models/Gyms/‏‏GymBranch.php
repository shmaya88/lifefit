<?php namespace App\Models\Gyms;

use Illuminate\Database\Eloquent\Model;

class ‏‏GymBranch extends Model{

	protected $table = 'gyms_branches';
	protected $connection = 'life_fit';

	public $timestamps = false;
}
