<?php namespace App\Services\Communications;

use Auth;

# Services
use App\Services\MainService as MS;

# Models
use App\Models\Communications\Communication;
use App\Models\Communications\CommunicationType;

# Helpers
use App\Helpers\ArrHelper;
use App\Helpers\StrHelper;

class CommunicationsService extends MS {
	public $max_communications_count = 30;
	public $delete_old_communications = true;
	public $communications_types = [];

	private static $communications_data = null;
	private static $owner_id = null;
	private static $owners_ids_array = null;
	private static $owner_type = null;

	function __construct($construct_data=null) {
		self::$communications_data = ArrHelper::getValByIndex($construct_data, 'communications_data', null);
		self::$owner_id = (int)ArrHelper::getValByIndex($construct_data, 'owner_id', null);
		self::$owners_ids_array = (array)ArrHelper::getValByIndex($construct_data, 'owners_ids_array', []);
		self::$owner_type = ArrHelper::getValByIndex($construct_data, 'owner_type', null);
	}

	# ---------------------------------------------------------------------------
	private function validateOwnerId(){
		if(!self::$owner_id) return self::setError('ההמזהה של בעל הרשומה אינו תקין');
		return true;
	}

	# ---------------------------------------------------------------------------
	private function validateOwnersIdsArray(){
		if(!self::$owners_ids_array OR !is_array(self::$owners_ids_array)) {
			return self::setError('המזהים של בעלי הרשומות אינם תקינים');
		}
		return true;
	}

	# ---------------------------------------------------------------------------
	private function validateOwnerType(){
		if(!self::$owner_type) return self::setError('ההמזהה של בעל הרשומה אינו תקין.');
		elseif(!StrHelper::isString(self::$owner_type)) return self::setError('סוג המשתנה "comm:owner_type" אינו תקין.'); 

		return true;
	}

	# ---------------------------------------------------------------------------
	private function validateCommunicationsData(){
		if(!self::$communications_data) return self::setError('חסרים נתוני התקשרויות.');
		elseif(!ArrHelper::isArray(self::$communications_data)) return self::setError('נתוני ההתקשרויות "adr:communications_data" אינם תקינים.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function getUniversalCommunicationsTypes($get_deleted=false){
		$this->communications_types = array(
			'by_name' => [], 
			'by_code' => [] 
		);

		$this->communications_types['by_name'] = $this->getCommunicationsTypes('name_eng',$get_deleted);
		if(!$this->communications_types['by_name']) return $this->communications_types;

		foreach ($this->communications_types['by_name'] as $k => $type_data) {
			$this->communications_types['by_code'][$type_data['code']] = $type_data;
		}

		return $this->communications_types;
	}

	# ---------------------------------------------------------------------------
	public function getCommunicationsTypes($key_by='name_eng', $get_deleted=false){
		if(!in_array($key_by, ['name_eng','code'])) $key_by='name_eng';

		$CommunicationType = CommunicationType::select();

		if(!$get_deleted) $CommunicationType = $CommunicationType->where('deleted',0);
		$CommunicationType = $CommunicationType->get()->keyBy($key_by)->toArray();
		
		return $CommunicationType;
	}

	# ---------------------------------------------------------------------------
	public function getCommunicationsList(){
		if(!self::validateOwnersIdsArray() OR !self::validateOwnerType()) return false;

		$communications_by_owner = $counts_by_user = [];

		$communications_list = Communication::select()
			->whereIn('owner_id', self::$owners_ids_array)
			->where('owner_type', self::$owner_type)
			->orderBy('communication_id')
			->get()->toArray();

		if($communications_list) {
			# <!--- Group communications by owner --!>
			foreach ($communications_list as $k => $type_data) {
				$c_owner_id = $type_data['owner_id'];

				if(!isset($counts_by_user[$c_owner_id])) $counts_by_user[$c_owner_id] = 1;

				$communications_by_owner[$c_owner_id]["tel_type".$counts_by_user[$c_owner_id]] = $type_data['type_code'];
				$communications_by_owner[$c_owner_id]["tel_num".$counts_by_user[$c_owner_id]] = $type_data['communication'];
				
				$counts_by_user[$c_owner_id]++;
			}
		}

		return $communications_by_owner;
	}

	# ---------------------------------------------------------------------------
	public function addCommunicationsIfExists(){
		if(!self::validateOwnerId() OR !self::validateOwnerType()) return false;

		if(self::$communications_data) {
			$communications_insert_array = $this->setCommunicationsInsertArray();

			if($communications_insert_array) $this->insertToDB($communications_insert_array);
			else $this->error_msg = '';
		}

		return true;		
	}

	# ---------------------------------------------------------------------------
	public function addCommunications(){
		if(!self::validateOwnerId() OR !self::validateOwnerType() OR !self::validateCommunicationsData()) return false;

		$communications_insert_array = $this->setCommunicationsInsertArray();

		if(!$communications_insert_array OR !$this->insertToDB($communications_insert_array)) return false;

		return true;
	}

	# ---------------------------------------------------------------------------
	public function setCommunicationsInsertArray(){
		if(!self::validateOwnerId() OR !self::validateOwnerType() OR !self::validateCommunicationsData()) return false;

		$communications_insert_array = array();

		for ($i=1; $i <= $this->max_communications_count ; $i++) {

			if(empty(self::$communications_data["tel_type$i"]) OR !isset(self::$communications_data["tel_num$i"])) continue;

			$curr_tel_type = (int)self::$communications_data["tel_type$i"];
			$curr_tel_num = trim(self::$communications_data["tel_num$i"]);

			if(!$curr_tel_type) continue;

			$communications_insert_array[] = array(
				'type_code' => $curr_tel_type, 
				'owner_id' => self::$owner_id, 
				'owner_type' => self::$owner_type, 
				'communication' => $curr_tel_num,
				'created_by' => Auth::user()->id,
				'updated_by' => Auth::user()->id
			);
		}

		if(!$communications_insert_array OR !count($communications_insert_array)) return self::setError('לא אותרו כתובות להכנסה');

		return $communications_insert_array;
	}

	# ---------------------------------------------------------------------------
	public function insertToDB($communications_insert_array=null){
		if(!$communications_insert_array) return self::setError('לא אותרו כתובות להכנסה.');

		if($this->delete_old_communications AND !$this->deleteOwnerCommunications()) return false;
		
		$insert_success = Communication::insert($communications_insert_array);
		if(!$insert_success) return self::setError('פעולת שמירת הכתובות נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteOwnerCommunications(){
		if(!self::validateOwnerId() OR !self::validateOwnerType()) return false;

		$delete_success = Communication::where([
				'owner_id' => self::$owner_id, 
				'owner_type' => self::$owner_type
			])->delete();

		if(!$delete_success AND $delete_success!=0) return self::setError('פעולת מחיקה הכתובות נכשלה.');

		return true;
	}
}
