<?php namespace App\Services\Common;

use Auth;

# Services
use App\Services\MainService as MS;
use App\Services\Users\UserService;
use App\Services\Gyms\GymsService;
use App\Services\Addresses\AddressesService;
use App\Services\Communications\CommunicationsService;

# Models
use App\Models\Permissions\Module;
use App\Models\Permissions\ModulePermission;

class MainCommonService extends MS {
	public static $SVARS = [];
	public static $user_data = [];
	public static $modules_list = [];
	public static $modules_permissions = [];
	public static $users_types = [];
	public static $addresses_types = [];
	public static $communications_types = [];
	public static $gyms_list = [];
	public static $gyms_branches_list = [];
	private static $constructor_runed = false;
	private static $UserService = null;
	private static $GymsService = null;
	private static $AddressesService = null;
	private static $CommunicationsService = null;

	function __construct($construct_data=null) {
		self::constructActions($construct_data);
	}

	# ---------------------------------------------------------------------------
	private static function constructActions($construct_data=null){
		self::$constructor_runed = true;
		self::$UserService = new UserService;
		self::$GymsService = new GymsService;
		self::$AddressesService = new AddressesService;
		self::$CommunicationsService = new CommunicationsService;
	}

	# ---------------------------------------------------------------------------
	public static function getSvars($return_data_type='array'){
		if(!in_array($return_data_type, ['array','json'])) $return_data_type='array';

		if(!(self::$SVARS = \Session::get('SVARS'.Auth::user()->id))) {
			if(!self::$constructor_runed) self::constructActions();
			
			self::$SVARS = array(
				'user_data' => self::getUserData(), 
				'users_types' => self::getUsersTypes(), 
				'modules' => self::getModules(),
				'gyms_list' => self::getGyms(),
				'gyms_branches_list' => self::getGymsBranches(),
				'addresses_types' => self::getAddressesTypes(),
				'communications_types' => self::getCommunicationsTypes()
			);

			\Session::set('SVARS'.Auth::user()->id, self::$SVARS);
		}

		$SVARS = self::$SVARS;
		if($return_data_type == 'json') $SVARS = json_encode(self::$SVARS);

		return $SVARS;
	}

	# ---------------------------------------------------------------------------
	public static function getGyms(){
		if(self::$gyms_list) return self::$gyms_list;
		if(!self::$constructor_runed) self::constructActions();
		
		$gyms_list_result = self::$GymsService->getGymsList([
			'rows_limit' => 'unlimited'
		]);
		
		if($gyms_list_result) self::$gyms_list = $gyms_list_result['data'];
		
		return self::$gyms_list;
	}

	# ---------------------------------------------------------------------------
	public static function getGymsBranches(){
		if(self::$gyms_branches_list) return self::$gyms_branches_list;
		if(!self::$constructor_runed) self::constructActions();

		$gyms_branches_list_result = self::$GymsService->getGymsBranchesList([
			'rows_limit' => 'unlimited'
		]);
		
		if($gyms_branches_list_result) self::$gyms_branches_list = $gyms_branches_list_result['data'];
		
		return self::$gyms_branches_list;
	}

	# ---------------------------------------------------------------------------
	public static function getUserData(){
		if(self::$user_data) return self::$user_data;
		if(!self::$constructor_runed) self::constructActions();

		$users_types = self::getUsersTypes();
		if(!$users_types) return [];

		self::$user_data = Auth::user()->toArray();

		if(isset($users_types['by_code'][self::$user_data['type_code']])) {
			self::$user_data['type_name'] = $users_types['by_code'][self::$user_data['type_code']]['name_eng'];
			self::$user_data['type_name_heb'] = $users_types['by_code'][self::$user_data['type_code']]['name_heb'];
		}

		self::$user_data['full_name'] = trim(self::$user_data['first_name'].' '.self::$user_data['last_name']);
		self::$user_data['user_id'] = self::$user_data['id'];
		
		return self::$user_data;
	}

	# ---------------------------------------------------------------------------
	public static function getAddressesTypes(){
		if(self::$addresses_types) return self::$addresses_types;
		if(!self::$constructor_runed) self::constructActions();
		
		if(self::$AddressesService->addresses_types) self::$addresses_types = self::$AddressesService->addresses_types;
		else self::$addresses_types = self::$AddressesService->getUniversalAddressesTypes();

		if(!self::$addresses_types) self::$addresses_types = [];
		return self::$addresses_types;
	}

	# ---------------------------------------------------------------------------
	public static function getCommunicationsTypes(){
		if(self::$communications_types) return self::$communications_types;
		if(!self::$constructor_runed) self::constructActions();
		
		if(self::$CommunicationsService->communications_types) {
			self::$communications_types = self::$CommunicationsService->communications_types;
		}
		else self::$communications_types = self::$CommunicationsService->getUniversalCommunicationsTypes();

		if(!self::$communications_types) self::$communications_types = [];
		return self::$communications_types;
	}

	# ---------------------------------------------------------------------------
	public static function getUsersTypes(){
		if(self::$users_types) return self::$users_types;
		if(!self::$constructor_runed) self::constructActions();
		
		if(self::$UserService->users_types) self::$users_types = self::$UserService->users_types;
		else self::$users_types = self::$UserService->getUniversalUsersTypes();

		if(!self::$users_types) self::$users_types = [];
		
		return self::$users_types;
	}

	# ---------------------------------------------------------------------------
	public static function getModules($key_by='name_eng', $get_deleted=false){
		if(!in_array($key_by, ['name_eng','code','id'])) $key_by='name_eng';
		if(!self::$constructor_runed) self::constructActions();

		$modules_permissions = self::getModulesPermissions();
		if(!$modules_permissions) return [];

		$Modules = Module::select();
		
		if(!$get_deleted) $Modules = $Modules->where('deleted',0);
		
		$Modules = $Modules->orderBy('sort')->get()->keyBy('code')->toArray();
		if(!$Modules) return null;

		foreach ($Modules as $module_key => $module_data) {
			$curr_m_key = (isset($module_data[$key_by]) ? $module_data[$key_by] : null);
			if(!$curr_m_key) continue;			

			$allowed_users_types = [];

			if(isset($modules_permissions[$module_data['code']])) {
				$allowed_users_types = $modules_permissions[$module_data['code']]['users_types'];
			}

			$module_data['allowed_users_types'] = $allowed_users_types;

			# Is parent module
			if($module_data['parent_code'] == 0) {
				if(!isset(self::$modules_list[$curr_m_key])) self::$modules_list[$curr_m_key] = [];
				self::$modules_list[$curr_m_key] = array_merge(self::$modules_list[$curr_m_key], $module_data);
			}
			# Is child module
			else {
				if(isset($Modules[$module_data['parent_code']])) $parent_data = $Modules[$module_data['parent_code']];
				else continue;

				if(!isset(self::$modules_list[$parent_data[$key_by]])) self::$modules_list[$parent_data[$key_by]] = [];
				self::$modules_list[$parent_data[$key_by]]['children'][$curr_m_key] = $module_data;
			}
		}

		usort(self::$modules_list, function($a, $b) {
			return $a['sort'] - $b['sort'];
		});
		
		return self::$modules_list;
	}

	# ---------------------------------------------------------------------------
	public static function getModulesPermissions(){
		if(self::$modules_permissions) return self::$modules_permissions;
		if(!self::$constructor_runed) self::constructActions();
		
		$all_users_types = self::getUsersTypes();
		if(!$all_users_types OR !isset($all_users_types['by_name'])) return [];

		$modules_permissions_list = ModulePermission::select()->get()->toArray();
		if(!$modules_permissions_list) return [];

		foreach ($modules_permissions_list as $k => $prm_data) {
			$module_code = (int)$prm_data['module_code'];
			$user_type_code = (int)$prm_data['user_type_code'];

			self::$modules_permissions[$module_code]['module_code'] = $module_code;

			if($user_type_code) {
				self::$modules_permissions[$module_code]['users_types'][$user_type_code] = $user_type_code;
			}
			# This module, is open to everyone
			else {
				foreach ($all_users_types['by_name'] as $ut_key => $ut_val) {				
					self::$modules_permissions[$module_code]['users_types'][$ut_val['code']] = $ut_val['code'];
				}
			}
		}
		
		return self::$modules_permissions;
	}

	# ---------------------------------------------------------------------------
	public static function addAddressesAndCommunications($owner_id=null, $owner_type=null, $data=[]){
		$adr_comm_params = array(
			'addresses_data' => $data,
			'communications_data' => $data,
			'owner_id' => $owner_id, 
			'owner_type' => $owner_type
		);
		
		$addresses_service = new AddressesService($adr_comm_params);
		if(!$addresses_service->addAddressesIfExists()) return self::setError($addresses_service->getError());

		$communications_service = new CommunicationsService($adr_comm_params);
		if(!$communications_service->addCommunicationsIfExists()) return self::setError($communications_service->getError());
		
		return true;
	}
}
