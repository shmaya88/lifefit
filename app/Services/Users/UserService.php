<?php namespace App\Services\Users;

# Models
use App\Models\Users\User;
use App\Models\Users\UserType;

# Services
use App\Services\MainService as MS;
use App\Services\Addresses\AddressesService;
use App\Services\Communications\CommunicationsService;
use App\Services\Common\MainCommonService;

# Helpers
use App\Helpers\ValidationHelper;
use App\Helpers\ArrHelper as Arr;
use App\Helpers\StrHelper as Str;
use App\Helpers\QueryHelper as QH;

use Illuminate\Support\Facades\Auth;
use DateTime;
use DB;


class UserService extends MS {
	public $users_types = [];

	# ---------------------------------------------------------------------------
	public function loginUser($user_data=null){
		# <!--- Required variables definition --!>
		$username = trim(Arr::getValByIndex($user_data, 'username', ''));
		if(!$username) return self::setError('לא אותר שם משתמש.');
		elseif(strlen($username) < 5) return self::setError('השם משתמש חייב להכיל לפחות 5 תווים.');

		$password = trim(Arr::getValByIndex($user_data, 'password', ''));
		if(!$password) return self::setError('לא אותרה סיסמא.');
		elseif(strlen($password) < 8) return self::setError('הסיסמא חייבת להכיל לפחות 8 תווים.');
		
		# <!--- Select user from DB --!>
		$user = $this->getUserByUsernameOrEmail($username, '', ['to_array'=>false]);
		if(!$user) return self::setError('השם משתמש או/ו הכתובת דוא"ל לא קיימים במערכת.');
		
		# <!--- Password Verify --!>
		$password_verify = password_verify($password, $user->password);
		if(!$password_verify) return self::setError('הסיסמא אינה תקינה.');

		if(!$this->updateLastLogin($user->id)) return false;

		# <!--- Set user to server session --!>
		Auth::login($user, true);

		return $user;
	}

	# ---------------------------------------------------------------------------
	public function usersList($properties=[]){
		# <!--- Set variables default values --!>
		$users_ids = $other_users_list = $addresses_list = $communications_list = $prohibited_types = [];

		$q_params = QH::listQueryPrepareParams($properties);
		if(!$q_params) return self::setError('שגיאה בעיבוד נתוני משתמשים.');

		$gym_id_filtering = (int)Arr::getValByIndex($properties, 'gym_id', 0);
		$gym_branch_id_filtering = (int)Arr::getValByIndex($properties, 'gym_branch_id', 0);

		if(!$q_params['sort_by']) $q_params['sort_by'] = 'full_name';
		$deleted = ($q_params['deleted'] == 'deleted'?1:0);

		$svars = MainCommonService::getSvars();
		$users_types_by_name = $svars['users_types']['by_name'];
		$auth_user = $svars['user_data'];

		$user_modal = new User();

		# <!--- Create users list query --!>
		$users_list_query = $user_modal->select([DB::raw(
				"SQL_CALC_FOUND_ROWS "
				."users.id AS user_id, users.gym_id, users.gym_branch_id, users.type_code, "
				."users.username, users.email, users.first_name, users.last_name, "
				."CONCAT(users.first_name, ' ', users.last_name) AS full_name, "
				."users.id_number, users.created_at, users.created_by, "
				."users.updated_at, users.updated_by, users_types.name_eng AS type_name_eng, "
				."users_types.name_heb AS type_name_heb, gyms.name AS gym_name , "
				."gyms_branches.name AS gym_branch_name "
			)])
			->leftJoin('users_types', 'users.type_code', '=', 'users_types.code')
			->leftJoin('gyms', 'gyms.gym_id', '=', 'users.gym_id')
			->leftJoin('gyms_branches', 'gyms_branches.gym_branch_id', '=', 'users.gym_branch_id')
			->where('users.deleted','=', $deleted)
			->orderBy($q_params['sort_by'], $q_params['sort_direction'])
			->skip($q_params['rows_start'])->take($q_params['rows_limit']);
		
		# <!--- Permissions conditions --!>
		if($auth_user['type_name'] != 'super_user') {
			if($auth_user['type_name'] == 'basic_user') return self::setError('חסרות הרשאות להצגת הרשימה.');

			$prohibited_types[] = $users_types_by_name['super_user']['code'];

			if($auth_user['type_name'] == 'branch_user') {
				$prohibited_types[] = $users_types_by_name['manager_user']['code'];
				$gym_branch_id_filtering = $auth_user['gym_branch_id'];
			}

			$users_list_query->whereNotIn('users.type_code', $prohibited_types);
			$gym_id_filtering = $auth_user['gym_id'];
		}

		# <!--- Filtering conditions --!>
		if($gym_id_filtering) $users_list_query->where('users.gym_id', $gym_id_filtering);
		if($gym_branch_id_filtering) $users_list_query->where('users.gym_branch_id', $gym_branch_id_filtering);

		# <!--- Search text conditions --!>
		QH::makeWhereSearch(
			$users_list_query, 
			$q_params['search_text'], 
			['users.first_name', 'users.last_name', 'users.id_number', 'users.username', 'users.email']
		);
		
		# <!--- Run query --!>
		$users_list = $users_list_query->get()->toArray();

		if($users_list) {
			# <!--- Get total rows count --!>
			$q_params['total_rows_count'] = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;

			foreach ($users_list as $k => $user) {
				# <!--- Set users ids array --!>
				$users_ids[$user['user_id']] = $user['user_id'];
				$users_ids[$user['created_by']] = $user['created_by'];
				$users_ids[$user['updated_by']] = $user['updated_by'];
			}

			if($users_ids) {
				$users_ids = array_keys($users_ids);

				# <!--- Get users names --!>
				$other_users_list = $user_modal->select()
					->whereIn('id', $users_ids)
					->get()->keyBy('id')->toArray();

				# <!--- Get users addresses and communications --!>
				$adr_comm_params = array(
					'owners_ids_array' => $users_ids, 
					'owner_type' => 'user'
				);

				$addresses_service = new AddressesService($adr_comm_params);
				$addresses_list = $addresses_service->getAddressesList();

				$communications_service = new CommunicationsService($adr_comm_params);
				$communications_list = $communications_service->getCommunicationsList();

				if(!$addresses_list AND $addresses_service->getError()) {
					return self::setError($addresses_service->getError());
				}

				if(!$communications_list AND $communications_service->getError()) {
					return self::setError($communications_service->getError());
				}
			}

			foreach ($users_list as $user_k => $user) {
				$curr_created_by_user = Arr::getValByIndex($other_users_list, $user['created_by'], '');
				$users_list[$user_k]['created_by_name'] = Str::generateFullName($curr_created_by_user);

				$curr_updated_by_user = Arr::getValByIndex($other_users_list, $user['updated_by'], '');
				$users_list[$user_k]['updated_by_name'] = Str::generateFullName($curr_updated_by_user);				

				if($addresses_list AND isset($addresses_list[$user['user_id']])) {					
					$users_list[$user_k] = array_merge($users_list[$user_k], $addresses_list[$user['user_id']]);
				}

				if($communications_list AND isset($communications_list[$user['user_id']])) {					
					$users_list[$user_k] = array_merge($users_list[$user_k], $communications_list[$user['user_id']]);
				}

				if(!$user['gym_branch_name']) $users_list[$user_k]['gym_branch_name'] = 'לא מוגדר';
				if(!$user['gym_name']) $users_list[$user_k]['gym_name'] = 'לא מוגדר';
			}
		}

		return QH::listQueryReturnData($q_params, $users_list);
	}	

	# ---------------------------------------------------------------------------
	public function addUser($user_data=null){
		if(!$user_data) return self::setError('לא אותרו נתוני משתמש.');

		$common_service = new MainCommonService;
		$svars = $common_service->getSvars();
		$users_types_by_code = $svars['users_types']['by_code'];
		$auth_user = $svars['user_data'];

		# <!--- Set variables default values --!>
		$type_code = (int)Arr::getValByIndex($user_data, 'type_code', 0);
		$gym_id = (int)Arr::getValByIndex($user_data, 'gym_id', 0);
		$gym_branch_id = (int)Arr::getValByIndex($user_data, 'gym_branch_id', 0);

		$username = trim(Arr::getValByIndex($user_data, 'username', ''));
		$password = trim(Arr::getValByIndex($user_data, 'password', ''));
		$password_repeat = trim(Arr::getValByIndex($user_data, 'password_repeat', ''));
		$email = trim(Arr::getValByIndex($user_data, 'email', ''));
		$first_name = trim(Arr::getValByIndex($user_data, 'first_name', ''));
		$last_name = trim(Arr::getValByIndex($user_data, 'last_name', ''));
		$id_number = trim(Arr::getValByIndex($user_data, 'id_number', ''));

		# <!--- Required variables check --!>
		if(!$type_code OR !isset($users_types_by_code[$type_code])) return self::setError('לא אותר סוג משתמש.');
		elseif(!$username) return self::setError('לא אותר שם משתמש.');
		elseif(!$email) return self::setError('לא אותרה כתובת דוא"ל.');
		elseif(!$password) return self::setError('לא אותרה סיסמא.');
		elseif(!$password_repeat) return self::setError('לא אותרה חזרה על הסיסמא.');

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') {
			if($auth_user['gym_id'] != $gym_id) {
				return self::setError('שגיאת הרשאה! לא ניתן להוסיף את המשתמש.');
			}
			elseif($auth_user['type_name'] != 'manager_user' AND $auth_user['gym_branch_id'] != $gym_branch_id){
				return self::setError('שגיאת הרשאה! לא ניתן להוסיף משתמש מסניף אחר.');
			}
		}

		# <!--- Data validation --!>
		$validation_helper = new ValidationHelper;

		# Username validation
		if(!$validation_helper->userNameValidate($username)) {
			return self::setError($validation_helper->getError());
		}
		elseif($validation_helper->emailValidate($username)) {
			return self::setError('השם משתמש לא יכול להיות במבנה של כתובת דוא"ל');
		}
		# Email validation
		elseif(!$validation_helper->emailValidate($email)) {
			return self::setError('כתובת דוא"ל אינה תקינה');
		}
		# Password validation
		elseif(!$validation_helper->passwordValidate($password, $password_repeat)) {
			return self::setError($validation_helper->getError());
		}
		
		# <!--- Check if the user does not exist in the system --!>
		$existing_user = $this->getUserByUsernameOrEmail($username, $email);
		if($existing_user) {
			if($existing_user['username'] == $username) return self::setError("שם משתמש זהה קיים במערכת.");
			elseif($existing_user['email'] == $email) return self::setError('כתובת דוא"ל זהה קיים במערכת.');
		}

		$user_id = User::insertGetId([
			'gym_id' => $gym_id,
			'gym_branch_id' => $gym_branch_id,
			'type_code' => $type_code,
			'username' => $username,
			'email' => $email,
			'password' => password_hash($password, PASSWORD_DEFAULT),
			'first_name' => $first_name,
			'last_name' => $last_name,
			'id_number' => $id_number,
			'created_by' => Auth::user()->id,
			'updated_by' => Auth::user()->id
		]);

		if(!$user_id) return self::setError('פעולת הוספת המשתמש נכשלה.');

		# <!--- Insert user addresses & communications --!>
		$action_result = $common_service->addAddressesAndCommunications($user_id, 'user', $user_data);
		if(!$action_result) return self::setError($common_service->getError());

		return $user_id;
	}

	# ---------------------------------------------------------------------------
	public function editUser($user_data=null){
		if(!$user_data) return self::setError('לא אותרו נתוני משתמש.');
		$update_conn_data_array = [];

		$common_service = new MainCommonService;
		$svars = $common_service->getSvars();
		$users_types_by_code = $svars['users_types']['by_code'];
		$auth_user = $svars['user_data'];

		# <!--- Set variables default values --!>
		$user_id = (int)Arr::getValByIndex($user_data, 'user_id', 0);
		$gym_id = (int)Arr::getValByIndex($user_data, 'gym_id', 0);
		$gym_branch_id = (int)Arr::getValByIndex($user_data, 'gym_branch_id', 0);
		$type_code = (int)Arr::getValByIndex($user_data, 'type_code', 0);
		
		$first_name = trim(Arr::getValByIndex($user_data, 'first_name', ''));
		$last_name = trim(Arr::getValByIndex($user_data, 'last_name', ''));
		$id_number = trim(Arr::getValByIndex($user_data, 'id_number', ''));
		
		# <!--- Required variables check --!>
		if(!$user_id) return self::setError('מזהה משתמש אינו תקין.');
		elseif(!$type_code OR !isset($users_types_by_code[$type_code])) return self::setError('לא אותר סוג משתמש.');

		# <!--- Get user data from 'users' table --!>
		$user_to_update_data = $this->getUserById($user_id);
		if(!$user_to_update_data) return self::setError('לא אותר משתמש לעדכון.');

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') {
			if($auth_user['gym_id'] != $gym_id) {
				return self::setError('שגיאת הרשאה! לא ניתן לעדכן את המשתמש.');
			}
			elseif($auth_user['type_name'] != 'manager_user' AND $auth_user['gym_branch_id'] != $gym_branch_id){
				return self::setError('שגיאת הרשאה! לא ניתן לעדכן משתמש מסניף אחר.');
			}
		}

		# <!--- Main Data validation --!>
		$username = trim(Arr::getValByIndex($user_data, 'username', ''));
		$password = trim(Arr::getValByIndex($user_data, 'password', ''));
		$password_repeat = trim(Arr::getValByIndex($user_data, 'password_repeat', ''));
		$email = trim(Arr::getValByIndex($user_data, 'email', ''));

		$password_changed = ($password OR $password_repeat);

		if($username != $user_to_update_data['username'] OR $email != $user_to_update_data['email'] OR $password_changed) {
			$m_data_err_str = 
				'בוצע שינוי בשם משתמש או/ו כתובת דוא"ל.</br>'
				.'לכן, על מנת לעדכן את המשתמש יש להזין את כל הפרטים הדרושים להתחברות המשתמש:</br>'
				.'שם משתמש, כתובת דוא"ל וסיסמא.</br>';

			if(!$username) return self::setError($m_data_err_str.'לא אותר שם משתמש.');
			if(!$email) return self::setError($m_data_err_str.'לא אותרה כתובת דוא"ל.');
			if(!$password) return self::setError($m_data_err_str.'לא אותרה סיסמא.');
			if(!$password_repeat) return self::setError($m_data_err_str.'לא אותרה חזרה על הסיסמא.');

			$validation_helper = new ValidationHelper;

			# Username validation
			if(!$validation_helper->userNameValidate($username)) {
				return self::setError($m_data_err_str.$validation_helper->getError());
			}
			elseif($validation_helper->emailValidate($username)) {
				return self::setError($m_data_err_str.'השם משתמש לא יכול להיות במבנה של כתובת דוא"ל');
			}
			# Email validation
			elseif(!$validation_helper->emailValidate($email)) {
				return self::setError($m_data_err_str.'כתובת דוא"ל אינה תקינה');
			}
			# Password validation
			elseif(!$validation_helper->passwordValidate($password, $password_repeat)) {
				return self::setError($m_data_err_str.$validation_helper->getError());
			}

			# <!--- Check if the user does not exist in the system --!>
			$existing_user = $this->getUserByUsernameOrEmail($username, $email, ['except_user_id'=>$user_id]);
			if($existing_user) {
				if($existing_user['username'] == $username) {
					return self::setError($m_data_err_str."שם משתמש זהה קיים במערכת.");
				}
				elseif($existing_user['email'] == $email) {
					return self::setError($m_data_err_str.'כתובת דוא"ל זהה קיימת במערכת.');
				}
			}

			$update_conn_data_array = array(
				'username' => $username,
				'email' => $email,
				'password' => password_hash($password, PASSWORD_DEFAULT),
			);
		}

		$update_array = array(
			'gym_id' => $gym_id,
			'gym_branch_id' => $gym_branch_id,
			'type_code' => $type_code,			
			'first_name' => $first_name,
			'last_name' => $last_name,
			'id_number' => $id_number,
			'updated_by' => Auth::user()->id
		);

		$update_success = User::where('id', $user_id)
			->update(array_merge($update_array,$update_conn_data_array));
		
		if(!$update_success AND $update_success!=0) return self::setError('פעולת עדכון המשתמש נכשלה.');
		
		# <!--- Update user addresses & communications --!>
		$action_result = $common_service->addAddressesAndCommunications($user_id, 'user', $user_data);
		if(!$action_result) return self::setError($common_service->getError());
		
		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreUser($action_data=null){
		if(!$action_data) return self::setError('לא אותרו נתוני הפעולה.');

		$user_id = (int)Arr::getValByIndex($action_data, 'user_id', 0);
		if(!$user_id) return self::setError('מזהה משתמש אינו תקין.');

		$action = trim(Arr::getValByIndex($action_data, 'action', ''));
		if(!$action OR !in_array($action, ['delete_user','restore_user','delete_forever_user'])) {
			return self::setError('הפעולה אינה תקינה.');
		}

		$svars = MainCommonService::getSvars();
		$auth_user = $svars['user_data'];

		# <!--- Check permissions --!>
		if($auth_user['type_name'] == 'basic_user') return self::setError('שגיאת הרשאה! לא ניתן לשחזר את המשתמש.');
		else if(in_array($action, ['restore_user','delete_forever_user'])) {
			if($auth_user['type_name'] != 'super_user') {
				return self::setError('שגיאת הרשאה! לא ניתן לשחזר את המשתמש.');
			}
		}

		$where_array = ['id' =>  $user_id];
		if($auth_user['type_name'] != 'super_user') $where_array['gym_branch_id'] = Auth::user()->gym_branch_id;

		if($action == 'restore_user') {
			$query_success = User::where($where_array)
				->update(['deleted' => 0, 'updated_by' => Auth::user()->id]);
		}
		elseif($action=='delete_user') {
			$query_success = User::where($where_array)
				->update(['deleted' => 1, 'updated_by' => Auth::user()->id]);
		}
		elseif($action == 'delete_forever_user') {
			$query_success = User::where($where_array)->delete();
		}

		if(!$query_success) return self::setError('פעולת עדכון המשתמש נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function getUniversalUsersTypes($get_deleted=false){
		$this->users_types = array(
			'by_name' => [], 
			'by_code' => [] 
		);

		$this->users_types['by_name'] = $this->getUsersTypes('name_eng',$get_deleted);
		if(!$this->users_types OR !$this->users_types['by_name']) return $this->users_types;

		foreach ($this->users_types['by_name'] as $k => $type_data) {
			$this->users_types['by_code'][$type_data['code']] = $type_data;
		}

		return $this->users_types;
	}

	# ---------------------------------------------------------------------------
	public function getUsersTypes($key_by='name_eng', $get_deleted=false){
		if(!in_array($key_by, ['name_eng','code'])) $key_by='name_eng';

		$users_types_array = [];

		$UserType = UserType::select();
		
		if(!$get_deleted) $UserType = $UserType->where('deleted',0);		
		$users_types_array = $UserType->get()->keyBy($key_by)->toArray();
		
		return $users_types_array;
	}

	# ---------------------------------------------------------------------------
	public function getUserByUsernameOrEmail($username='', $email='', $properties=[]){
		if(!$username) return self::setError('לא אותרו פרטים לבדיקת משתמש.');
		if(!$email) $email = $username;

		$to_array = Arr::getValByIndex($properties, 'to_array', true);
		$except_user_id = (int)Arr::getValByIndex($properties, 'except_user_id', 0);

		$user = User::select(
				'users.*', 'users_types.name_eng AS user_type_eng', 'gyms.name AS gym_name',
				'users_types.name_heb AS user_type_heb', 'gyms_branches.name AS gym_branch_name'
			)
			->leftJoin('users_types', 'users.type_code', '=', 'users_types.code')
			->leftJoin('gyms', 'gyms.gym_id', '=', 'users.gym_id')
			->leftJoin('gyms_branches', 'gyms_branches.gym_branch_id', '=', 'users.gym_branch_id')
			->where(function($query) use ($username, $email) {					
				$query->orWhere("users.username","=",$username)
					->orWhere("users.email","=",$email);
			});

		if($except_user_id) $user = $user->where("users.id","!=",$except_user_id);

		$user = $user->get()->first();

		if($user AND $to_array) $user = $user->toArray();
		return $user;
	}

	# ---------------------------------------------------------------------------
	public function updateLastLogin($user_id=''){
		if(!$user_id) {
			$user = Auth::user()->toArray();

			if($user AND isset($user['id'])) $user_id = $user['id'];
			if(!$user_id) return self::setError('מזהה משתמש אינו תקין.');
		};

		$update_success = User::where("id","=",$user_id)->update(['last_login'=>DB::raw('NOW()')]);
		if(!$update_success AND $update_success!=0) return self::setError('פעולת עדכון המשתמש נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function checkLastLogin($last_login=''){
		if(!$last_login) {
			$user = Auth::user()->toArray();

			if($user AND isset($user['last_login'])) $last_login = $user['last_login'];
			if(!$last_login) return false;
		};

		$difference_in_seconds = time() - strtotime($last_login);
		$hours_passed = $difference_in_seconds/3600;

		if($hours_passed > 2) return false;
		return true;
	}

	# ---------------------------------------------------------------------------
	public function getUserById($user_id='', $properties=[]){
		if(!$user_id) return self::setError('מזהה משתמש אינו תקין.');

		$to_array = Arr::getValByIndex($properties, 'to_array', true);

		$user = User::select()
			->where("id","=",$user_id)
			->get()->first();

		if($user AND $to_array) $user=$user->toArray();

		return $user;
	}

}
