<?php namespace App\Services\Addresses;

use Auth;

# Services
use App\Services\MainService as MS;

# Models
use App\Models\Addresses\Address;
use App\Models\Addresses\‏‏AddressType;

# Helpers
use App\Helpers\ArrHelper;
use App\Helpers\StrHelper;

class AddressesService extends MS {
	public $max_addresses_count = 30;
	public $delete_old_addresses = true;
	public $addresses_types = [];

	private static $addresses_data = null;
	private static $owner_id = null;
	private static $owners_ids_array = null;
	private static $owner_type = null;

	function __construct($construct_data=null) {
		self::$addresses_data = ArrHelper::getValByIndex($construct_data, 'addresses_data', null);
		self::$owner_id = (int)ArrHelper::getValByIndex($construct_data, 'owner_id', null);
		self::$owners_ids_array = (array)ArrHelper::getValByIndex($construct_data, 'owners_ids_array', []);
		self::$owner_type = ArrHelper::getValByIndex($construct_data, 'owner_type', null);
	}

	# ---------------------------------------------------------------------------
	private function validateOwnerId(){
		if(!self::$owner_id) return self::setError('ההמזהה של בעל הרשומה אינו תקין');
		return true;
	}

	# ---------------------------------------------------------------------------
	private function validateOwnersIdsArray(){
		if(!self::$owners_ids_array OR !is_array(self::$owners_ids_array)) {
			return self::setError('המזהים של בעלי הרשומות אינם תקינים');
		}
		return true;
	}

	# ---------------------------------------------------------------------------
	private function validateOwnerType(){
		if(!self::$owner_type) return self::setError('ההמזהה של בעל הרשומה אינו תקין.');
		elseif(!StrHelper::isString(self::$owner_type)) return self::setError('סוג המשתנה "adr:owner_type" אינו תקין.'); 

		return true;
	}

	# ---------------------------------------------------------------------------
	private function validateAddressesData(){
		if(!self::$addresses_data) return self::setError('חסרים נתוני כתובות.');
		elseif(!ArrHelper::isArray(self::$addresses_data)) return self::setError('נתוני הכתובות "adr:addresses_data" אינם תקינים.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function getUniversalAddressesTypes($get_deleted=false){
		$this->addresses_types = array(
			'by_name' => [], 
			'by_code' => [] 
		);

		$this->addresses_types['by_name'] = $this->getAddressesTypes('name_eng',$get_deleted);
		if(!$this->addresses_types['by_name']) return $this->addresses_types;

		foreach ($this->addresses_types['by_name'] as $k => $type_data) {
			$this->addresses_types['by_code'][$type_data['code']] = $type_data;
		}

		return $this->addresses_types;
	}

	# ---------------------------------------------------------------------------
	public function getAddressesTypes($key_by='name_eng', $get_deleted=false){
		if(!in_array($key_by, ['name_eng','code'])) $key_by='name_eng';

		$AddressType = ‏‏AddressType::select();

		if(!$get_deleted) $AddressType = $AddressType->where('deleted',0);
		$AddressType = $AddressType->get()->keyBy($key_by)->toArray();
		
		return $AddressType;
	}

	# ---------------------------------------------------------------------------
	public function getAddressesList(){
		if(!self::validateOwnersIdsArray() OR !self::validateOwnerType()) return false;
		
		$addresses_by_owner = $counts_by_user = [];

		$addresses_list = Address::select()
			->whereIn('owner_id', self::$owners_ids_array)
			->where('owner_type', self::$owner_type)
			->orderBy('address_id')
			->get()->toArray();
		
		if($addresses_list) {
			# <!--- Group addresses by owner --!>
			foreach ($addresses_list as $k => $type_data) {
				$c_owner_id = $type_data['owner_id'];

				if(!isset($counts_by_user[$c_owner_id])) $counts_by_user[$c_owner_id] = 1;

				$addresses_by_owner[$c_owner_id]["adr_type".$counts_by_user[$c_owner_id]] = $type_data['type_code'];
				$addresses_by_owner[$c_owner_id]["adr".$counts_by_user[$c_owner_id]] = $type_data['address'];
				
				$counts_by_user[$c_owner_id]++;
			}
		}

		return $addresses_by_owner;
	}

	# ---------------------------------------------------------------------------
	public function addAddressesIfExists(){
		if(!self::validateOwnerId() OR !self::validateOwnerType()) return false;

		if(self::$addresses_data) {
			$addresses_insert_array = $this->setAddressesInsertArray();

			if($addresses_insert_array) $this->insertToDB($addresses_insert_array);
			else $this->error_msg = '';
		}

		return true;		
	}

	# ---------------------------------------------------------------------------
	public function addAddresses(){
		if(!self::validateOwnerId() OR !self::validateOwnerType() OR !self::validateAddressesData()) return false;

		$addresses_insert_array = $this->setAddressesInsertArray();

		if(!$addresses_insert_array OR !$this->insertToDB($addresses_insert_array)) return false;

		return true;
	}

	# ---------------------------------------------------------------------------
	public function setAddressesInsertArray(){
		if(!self::validateOwnerId() OR !self::validateOwnerType() OR !self::validateAddressesData()) return false;

		$addresses_insert_array = array();

		for ($i=1; $i <= $this->max_addresses_count ; $i++) {

			if(empty(self::$addresses_data["adr_type$i"]) OR !isset(self::$addresses_data["adr$i"])) continue;

			$curr_adr_type = (int)self::$addresses_data["adr_type$i"];
			$curr_address = trim(self::$addresses_data["adr$i"]);

			if(!$curr_adr_type) continue;

			$addresses_insert_array[] = array(
				'type_code' => $curr_adr_type, 
				'owner_id' => self::$owner_id, 
				'owner_type' => self::$owner_type, 
				'address' => $curr_address,
				'created_by' => Auth::user()->id,
				'updated_by' => Auth::user()->id
			);
		}

		if(!$addresses_insert_array OR !count($addresses_insert_array)) return self::setError('לא אותרו כתובות להכנסה');

		return $addresses_insert_array;
	}

	# ---------------------------------------------------------------------------
	public function insertToDB($addresses_insert_array=null){
		if(!$addresses_insert_array) return self::setError('לא אותרו כתובות להכנסה.');

		if($this->delete_old_addresses AND !$this->deleteOwnerAddresses()) return false;
		
		$insert_success = Address::insert($addresses_insert_array);
		if(!$insert_success) return self::setError('פעולת שמירת הכתובות נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteOwnerAddresses(){
		if(!self::validateOwnerId() OR !self::validateOwnerType()) return false;

		$delete_success = Address::where([
				'owner_id' => self::$owner_id, 
				'owner_type' => self::$owner_type
			])->delete();

		if(!$delete_success AND $delete_success!=0) return self::setError('פעולת מחיקה הכתובות נכשלה.');

		return true;
	}
}
