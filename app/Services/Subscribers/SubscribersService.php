<?php namespace App\Services\Subscribers;

# Models
use App\Models\Subscribers\Subscription;
use App\Models\Subscribers\‏‏SubscriptionTraning;

# Services
use App\Services\MainService as MS;
use App\Services\Addresses\AddressesService;
use App\Services\Communications\CommunicationsService;
use App\Services\Common\MainCommonService;

# Helpers
use App\Helpers\ArrHelper as Arr;
use App\Helpers\StrHelper as Str;
use App\Helpers\QueryHelper as QH;

use Illuminate\Support\Facades\Auth;
use DB;

class SubscribersService extends MS {
	public $users_types = [];

	# ---------------------------------------------------------------------------
	public function subscribersList($properties=[]){
		# <!--- Set variables default values --!>
		$addresses_list = $communications_list = $subscribers_ids = [];

		$q_params = QH::listQueryPrepareParams($properties);
		if(!$q_params) return self::setError('שגיאה בעיבוד נתוני מנוי.');
		
		if(!$q_params['sort_by']) $q_params['sort_by'] = 'subscription_full_name';
		$deleted = ($q_params['deleted'] == 'deleted'?1:0);
		
		$gym_id_filtering = (int)Arr::getValByIndex($properties, 'gym_id', 0);
		$gym_branch_id_filtering = (int)Arr::getValByIndex($properties, 'gym_branch_id', 0);

		$svars = MainCommonService::getSvars();
		$users_types_by_name = $svars['users_types']['by_name'];
		$auth_user = $svars['user_data'];

		$subscription_modal = new Subscription();

		# <!--- Create subscribers list query --!>
		$subscribers_list_query = $subscription_modal->select([DB::raw(
			"SQL_CALC_FOUND_ROWS "
			."subscribers.subscription_id, subscribers.gym_id, subscribers.gym_branch_id, "
			."subscribers.first_name, subscribers.last_name,  subscribers.user_id, "
			."CONCAT(subscribers.first_name, ' ', subscribers.last_name) AS subscription_full_name, "
			."subscribers.id_number, subscribers.created_at, subscribers.created_by, "
			."CONCAT(users.first_name, ' ', users.last_name) AS user_full_name, "
			."subscribers.updated_at, subscribers.updated_by, subscribers.expiry_date, gyms.name AS gym_name, "
			."gyms_branches.name AS gym_branch_name, subscribers_traning.weight, subscribers_traning.fat_percentage, "
			."subscribers_traning.created_at AS last_traning_date "
		)])
		->leftJoin('users', 'users.id', '=', 'subscribers.user_id')
		->leftJoin('gyms', 'gyms.gym_id', '=', 'subscribers.gym_id')
		->leftJoin('gyms_branches', 'gyms_branches.gym_branch_id', '=', 'subscribers.gym_branch_id')
		->leftJoin('subscribers_traning', function ($leftJoin) {
			$leftJoin->on('subscribers.subscription_id', '=', 'subscribers_traning.subscription_id')
				->on(
					'subscribers_traning.subscription_traning_id', '=', 
					DB::raw("("
						."SELECT MAX(st.subscription_traning_id) "
						."FROM subscribers_traning AS st "
						."WHERE st.subscription_id=subscribers.subscription_id AND st.deleted=0"
					.")")
				);
		})
		->where('subscribers.deleted','=',$deleted)
		->orderBy($q_params['sort_by'], $q_params['sort_direction'])
		->skip($q_params['rows_start'])->take($q_params['rows_limit']);

		if($q_params['deleted'] == 'expired') {
			$subscribers_list_query->where('subscribers.expiry_date', '<' ,date("Y-m-d"));
		}
		elseif($q_params['deleted'] == 'active') {
			$subscribers_list_query->where('subscribers.expiry_date', '>=' ,date("Y-m-d"));
		}
		
		# <!--- Permissions conditions --!>
		if($auth_user['type_name'] != 'super_user') {
			if($auth_user['type_name'] == 'basic_user') {
				$subscribers_list_query->where('subscribers.user_id', $auth_user['user_id']);
			}

			if($auth_user['type_name'] != 'manager_user') {
				$gym_branch_id_filtering = $auth_user['gym_branch_id'];
			}
			
			$gym_id_filtering = $auth_user['gym_id'];
		}

		# <!--- Filtering conditions --!>
		if($gym_id_filtering) {
			$subscribers_list_query->where('subscribers.gym_id', $gym_id_filtering);
		}
		if($gym_branch_id_filtering) {
			$subscribers_list_query->where('subscribers.gym_branch_id', $gym_branch_id_filtering);
		}

		# <!--- Search text conditions --!>
		QH::makeWhereSearch(
			$subscribers_list_query, 
			$q_params['search_text'], 
			['subscribers.first_name', 'subscribers.last_name', 'subscribers.id_number']
		);

		# <!--- Run query --!>
		$subscribers_list = $subscribers_list_query->get()->toArray();

		if($subscribers_list) {
			# <!--- Get total rows count --!>
			$q_params['total_rows_count'] = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;

			foreach ($subscribers_list as $k => $subscription) {
				# <!--- Set subscribers ids array --!>
				$subscribers_ids[$subscription['subscription_id']] = $subscription['subscription_id'];
			}

			if($subscribers_ids) {
				$subscribers_ids = array_keys($subscribers_ids);

				# <!--- Get subscribers addresses and communications --!>
				$adr_comm_params = array(
					'owners_ids_array' => $subscribers_ids, 
					'owner_type' => 'subscription'
				);

				$addresses_service = new AddressesService($adr_comm_params);
				$addresses_list = $addresses_service->getAddressesList();

				$communications_service = new CommunicationsService($adr_comm_params);
				$communications_list = $communications_service->getCommunicationsList();

				if(!$addresses_list AND $addresses_service->getError()) {
					return self::setError($addresses_service->getError());
				}

				if(!$communications_list AND $communications_service->getError()) {
					return self::setError($communications_service->getError());
				}
			}

			foreach ($subscribers_list as $sub_k => $subscription) {
				$new_expiry_date = Str::dateFromYMDtoDMY($subscription['expiry_date']);
				$new_last_traning_date = Str::dateFromYMDtoDMY($subscription['last_traning_date']);

				$subscribers_list[$sub_k]['expiry_date'] = $subscription['expiry_date'] = $new_expiry_date;
				$subscribers_list[$sub_k]['last_traning_date'] = $subscription['last_traning_date'] = $new_last_traning_date;

				if($addresses_list AND isset($addresses_list[$subscription['subscription_id']])) {					
					$subscribers_list[$sub_k] = array_merge(
						$subscribers_list[$sub_k], 
						$addresses_list[$subscription['subscription_id']]
					);
				}

				if($communications_list AND isset($communications_list[$subscription['subscription_id']])) {					
					$subscribers_list[$sub_k] = array_merge(
						$subscribers_list[$sub_k], 
						$communications_list[$subscription['subscription_id']]
					);
				}

				if(!$subscription['weight']) $subscribers_list[$sub_k]['weight'] = '';
				if(!$subscription['fat_percentage']) $subscribers_list[$sub_k]['fat_percentage'] = '';
				if(!$subscription['last_traning_date']) $subscribers_list[$sub_k]['last_traning_date'] = '';

				if(!$subscription['user_full_name']) $subscribers_list[$sub_k]['user_full_name'] = 'לא מוגדר';
				if(!$subscription['gym_name']) $subscribers_list[$sub_k]['gym_name'] = 'לא מוגדר';
				if(!$subscription['gym_branch_name']) $subscribers_list[$sub_k]['gym_branch_name'] = 'לא מוגדר';
				if(!$subscription['expiry_date']) $subscribers_list[$sub_k]['expiry_date'] = 'לא מוגדר';
			}
		}
		
		return QH::listQueryReturnData($q_params, $subscribers_list);
	}

	# ---------------------------------------------------------------------------
	public function addSubscription($subscription_data=[]){
		# <!--- Set variables default values --!>
		$first_name = trim(Arr::getValByIndex($subscription_data, 'first_name', ''));
		$last_name = trim(Arr::getValByIndex($subscription_data, 'last_name', ''));
		$id_number = trim(Arr::getValByIndex($subscription_data, 'id_number', ''));
		$expiry_date = Str::dateFromDMYtoYMD(Arr::getValByIndex($subscription_data, 'expiry_date', ''));

		$user_id = (int)Arr::getValByIndex($subscription_data, 'user_id', 0);
		$gym_id = (int)Arr::getValByIndex($subscription_data, 'gym_id', 0);
		$gym_branch_id = (int)Arr::getValByIndex($subscription_data, 'gym_branch_id', 0);

		# <!--- Required variables check --!>
		if(!$id_number) return self::setError('לא אותר מספר תעודת זהות.');
		elseif(!$last_name OR !$last_name) return self::setError('לא אותר שם המנוי.');
		elseif(!$user_id) return self::setError('מזהה משתמש אינו תקין.');
		elseif(!$expiry_date) return self::setError('לא אותר תאריך, תוקף המנוי');

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();
		
		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') {
			if(!$gym_id OR $gym_id != $auth_user['gym_id']) return self::setError('לא אותר מזהה מכון הכושר תקין.');

			if($auth_user['type_name'] != 'manager_user') {
				if(!$gym_branch_id OR $gym_branch_id != $auth_user['gym_branch_id']) {
					return self::setError('לא אותר מזהה סניף תקין.');
				}

				if($auth_user['type_name'] == 'basic_user' AND $user_id != $auth_user['user_id']) {
					return self::setError('שגיאת הרשאה.');
				}
			}
		}

		# <!--- Check if the subscription does not exist in the DB --!>
		$existing_subscription = $this->getSubscriptionByIdNumAndGym($id_number, $gym_id);
		if($existing_subscription) return self::setError('מנוי עם תעודת זהות זהה קיים במערכת.');

		$subscription_id = Subscription::insertGetId([
			'user_id' => $user_id,
			'gym_id' => $gym_id,
			'gym_branch_id' => $gym_branch_id,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'id_number' => $id_number,
			'expiry_date' => $expiry_date,
			'created_by' => $auth_user['id'],
			'updated_by' =>$auth_user['id']
		]);
		
		if(!$subscription_id) return self::setError('פעולת הוספת המנוי נכשלה.');

		# <!--- Insert subscription addresses & communications --!>
		$action_result = $common_service->addAddressesAndCommunications($subscription_id, 'subscription', $subscription_data);
		if(!$action_result) return self::setError($common_service->getError());
		
		return $subscription_id;
	}

	# ---------------------------------------------------------------------------
	public function editSubscription($subscription_data=[]){
		# <!--- Set variables default values --!>
		$first_name = trim(Arr::getValByIndex($subscription_data, 'first_name', ''));
		$last_name = trim(Arr::getValByIndex($subscription_data, 'last_name', ''));
		$id_number = trim(Arr::getValByIndex($subscription_data, 'id_number', ''));
		$expiry_date = Str::dateFromDMYtoYMD(Arr::getValByIndex($subscription_data, 'expiry_date', ''));

		$subscription_id = (int)Arr::getValByIndex($subscription_data, 'subscription_id', 0);
		$user_id = (int)Arr::getValByIndex($subscription_data, 'user_id', 0);
		$gym_id = (int)Arr::getValByIndex($subscription_data, 'gym_id', 0);
		$gym_branch_id = (int)Arr::getValByIndex($subscription_data, 'gym_branch_id', 0);

		# <!--- Required variables check --!>
		if(!$id_number) return self::setError('לא אותר מספר תעודת זהות.');
		elseif(!$last_name OR !$last_name) return self::setError('לא אותר שם המנוי.');
		elseif(!$user_id) return self::setError('מזהה משתמש אינו תקין.');
		elseif(!$subscription_id) return self::setError('מזהה מנוי אינו תקין.');
		elseif(!$expiry_date) return self::setError('לא אותר תאריך, תוקף המנוי');

		$common_service = new MainCommonService;		
		$auth_user = $common_service->getUserData();

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') {
			if(!$gym_id OR $gym_id != $auth_user['gym_id']) return self::setError('לא אותר מזהה מכון הכושר תקין.');

			if($auth_user['type_name'] != 'manager_user') {
				if(!$gym_branch_id OR $gym_branch_id != $auth_user['gym_branch_id']) {
					return self::setError('לא אותר מזהה סניף תקין.');
				}

				if($auth_user['type_name'] == 'basic_user' AND $user_id != $auth_user['user_id']) {
					return self::setError('שגיאת הרשאה.');
				}
			}
		}

		# <!--- Check if the subscription does not exist in the DB --!>
		$existing_subscription = $this->getSubscriptionByIdNumAndGym(
			$id_number, 
			$gym_id, 
			['except_subscription_id'=>$subscription_id]
		);
		if($existing_subscription) return self::setError('מנוי עם תעודת זהות זהה קיים במערכת.');

		$update_success = Subscription::where('subscription_id', $subscription_id)
			->update([
				'user_id' => $user_id,
				'gym_id' => $gym_id,
				'gym_branch_id' => $gym_branch_id,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'id_number' => $id_number,
				'expiry_date' => $expiry_date,
				'updated_by' =>$auth_user['id']
			]);

		if(!$update_success AND $update_success!=0) return self::setError('פעולת עדכון המנוי נכשלה.');

		# <!--- Update subscription addresses & communications --!>
		$action_result = $common_service->addAddressesAndCommunications($subscription_id, 'subscription', $subscription_data);
		if(!$action_result) return self::setError($common_service->getError());

		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreSubscription($action_data=null){
		if(!$action_data) return self::setError('לא אותרו נתוני הפעולה.');

		$subscription_id = (int)Arr::getValByIndex($action_data, 'subscription_id', 0);
		$action = trim(Arr::getValByIndex($action_data, 'action', ''));

		if(!$action OR !in_array($action, ['delete_subscription','restore_subscription'])) {
			return self::setError('הפעולה אינה תקינה.');
		}
		elseif(!$subscription_id) return self::setError('מזהה מנוי אינו תקין.');

		$where_array = ['subscription_id' =>  $subscription_id];

		if($action == 'restore_subscription') {
			$query_success = Subscription::where($where_array)
				->update([
					'user_id' => Auth::user()->id,
					'gym_id' => Auth::user()->gym_id,
					'gym_branch_id' => Auth::user()->gym_branch_id,
					'deleted' => 0,
					'updated_by' => Auth::user()->id
				]);
		}
		elseif($action=='delete_subscription') {
			$query_success = Subscription::where($where_array)
				->update([
					'user_id' => 0,
					'deleted' => 1, 
					'updated_by' => Auth::user()->id
				]);
		}

		if(!$query_success) return self::setError('פעולת עדכון המנוי נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function setSubscriptionUser($new_user_data=null){
		# <!--- Set variables default values --!>
		$subscription_id = (int)Arr::getValByIndex($new_user_data, 'subscription_id', 0);
		$user_id = (int)Arr::getValByIndex($new_user_data, 'user_id', 0);
		$gym_id = (int)Arr::getValByIndex($new_user_data, 'gym_id', 0);
		$gym_branch_id = (int)Arr::getValByIndex($new_user_data, 'gym_branch_id', 0);

		# <!--- Required variables check --!>
		if(!$user_id) return self::setError('מזהה משתמש אינו תקין.');
		elseif(!$subscription_id) return self::setError('מזהה מנוי אינו תקין.');

		$common_service = new MainCommonService;		
		$auth_user = $common_service->getUserData();

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') {
			if(!$gym_id OR $gym_id != $auth_user['gym_id']) return self::setError('לא אותר מזהה מכון הכושר תקין.');

			if($auth_user['type_name'] != 'manager_user') {
				if(!$gym_branch_id OR $gym_branch_id != $auth_user['gym_branch_id']) {
					return self::setError('לא אותר מזהה סניף תקין.');
				}

				if($auth_user['type_name'] == 'basic_user' AND $user_id != $auth_user['user_id']) {
					return self::setError('שגיאת הרשאה.');
				}
			}
		}

		$update_success = Subscription::where('subscription_id', $subscription_id)
			->update([
				'user_id' => $user_id,
				'gym_id' => $gym_id,
				'gym_branch_id' => $gym_branch_id,
				'updated_by' =>$auth_user['id']
			]);

		if(!$update_success AND $update_success!=0) return self::setError('פעולת עדכון המטפל נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function addSubscriptionTraining($subscription_train_data=null){
		if(!$subscription_train_data) return self::setError('לא אותרו נתונם.');

		# <!--- Set variables default values --!>
		$description = trim(Arr::getValByIndex($subscription_train_data, 'description', ''));

		$subscription_id = (int)Arr::getValByIndex($subscription_train_data, 'subscription_id', 0);
		$weight = (float)Arr::getValByIndex($subscription_train_data, 'weight', 0);
		$fat_percentage = (float)Arr::getValByIndex($subscription_train_data, 'fat_percentage', 0);

		# <!--- Required variables check --!>
		if(!$subscription_id) return self::setError('לא אותר מזהה מנוי.');
		
		$common_service = new MainCommonService;		
		$auth_user = $common_service->getUserData();

		$subscription_traning_id = ‏‏SubscriptionTraning::insertGetId([
			'subscription_id' => $subscription_id,
			'weight' => $weight,
			'fat_percentage' => $fat_percentage,
			'description' => $description,
			'created_by' => $auth_user['id'],
			'updated_by' =>$auth_user['id']
		]);
		
		if(!$subscription_traning_id) return self::setError('פעולת הוספת האימון/ביקור נכשלה.');
		
		return $subscription_traning_id;
	}

	# ---------------------------------------------------------------------------
	public function editSubscriptionTraining($subscription_train_data=null){
		if(!$subscription_train_data) return self::setError('לא אותרו נתונם.');

		# <!--- Set variables default values --!>
		$description = trim(Arr::getValByIndex($subscription_train_data, 'description', ''));

		$subscription_traning_id = (int)Arr::getValByIndex($subscription_train_data, 'subscription_traning_id', 0);
		$weight = (float)Arr::getValByIndex($subscription_train_data, 'weight', 0);
		$fat_percentage = (float)Arr::getValByIndex($subscription_train_data, 'fat_percentage', 0);

		# <!--- Required variables check --!>
		if(!$subscription_traning_id) return self::setError('לא אותר מזהה.');

		$common_service = new MainCommonService;		
		$auth_user = $common_service->getUserData();

		$update_success = ‏‏SubscriptionTraning::where('subscription_traning_id', $subscription_traning_id)
			->update([
				'weight' => $weight,
				'fat_percentage' => $fat_percentage,
				'description' => $description,
				'updated_by' =>$auth_user['id']
			]);

		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteSubscriptionTraining($subscription_train_data=null){
		if(!$subscription_train_data) return self::setError('לא אותרו נתונם.');

		$subscription_traning_id = (int)Arr::getValByIndex($subscription_train_data, 'subscription_traning_id', 0);
		if(!$subscription_traning_id) return self::setError('לא אותר מזהה.');

		$common_service = new MainCommonService;		
		$auth_user = $common_service->getUserData();

		$update_success = ‏‏SubscriptionTraning::where('subscription_traning_id', $subscription_traning_id)
			->update([
				'deleted' => 1,
				'updated_by' =>$auth_user['id']
			]);

		return true;
	}

	# ---------------------------------------------------------------------------
	public function subscribersTrainingList($properties=[]){
		# <!--- Set variables default values --!>
		$subscription_id = (int)Arr::getValByIndex($properties, 'subscription_id', 0);

		# <!--- Required variables check --!>
		if(!$subscription_id) return self::setError('לא אותר מזהה מנוי.');

		$q_params = QH::listQueryPrepareParams($properties);
		if(!$q_params) return self::setError('שגיאה בעיבוד נתונים.');
		
		if(!$q_params['sort_by']) $q_params['sort_by'] = 'created_at';
		$deleted = ($q_params['deleted'] == 'deleted'?1:0);
		
		$svars = MainCommonService::getSvars();
		$users_types_by_name = $svars['users_types']['by_name'];
		$auth_user = $svars['user_data'];

		$subscription_modal_train = new ‏‏SubscriptionTraning();
		
		# <!--- Create subscribers traning list query --!>
		$subscribers_train_list_query = $subscription_modal_train->select([DB::raw(
			"SQL_CALC_FOUND_ROWS "
			."subscribers_traning.*, "
			."CONCAT(users.first_name, ' ', users.last_name) AS user_full_name "
		)])
		->leftJoin('users', 'users.id', '=', 'subscribers_traning.created_by')
		->where('subscribers_traning.subscription_id','=',$subscription_id)
		->where('subscribers_traning.deleted','=',$deleted)
		->orderBy($q_params['sort_by'], $q_params['sort_direction'])
		->skip($q_params['rows_start'])->take($q_params['rows_limit']);

		# <!--- Run query --!>
		$subscribers_train_list = $subscribers_train_list_query->get()->toArray();

		if($subscribers_train_list) {
			# <!--- Get total rows count --!>
			$q_params['total_rows_count'] = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;

			foreach ($subscribers_train_list as $k => $train_data) {
				$new_created_at = Str::dateFromYMDtoDMY($train_data['created_at']);
				$subscribers_train_list[$k]['created_at'] = $train_data['created_at'] = $new_created_at;

				if(!$train_data['user_full_name']) $subscribers_train_list[$k]['user_full_name'] = 'לא מוגדר';
				if(!$train_data['created_at']) $subscribers_train_list[$k]['created_at'] = 'לא מוגדר';
			}
		}
		
		return QH::listQueryReturnData($q_params, $subscribers_train_list);
	}

	# ---------------------------------------------------------------------------
	public function getSubscriptionByIdNumAndGym($id_number=null, $gym_id=null, $properties=[]){
		if(!$id_number) return self::setError('לא אותר מספר תעודת זהות.[getSsubscriptionByIdNumAndGym]');
		if(!$gym_id) return self::setError('לא אותר מזהה חברה. [getSsubscriptionByIdNumAndGym]');

		$to_array = Arr::getValByIndex($properties, 'to_array', true);
		$except_subscription_id = (int)Arr::getValByIndex($properties, 'except_subscription_id', 0);

		$subscription = Subscription::select()
			->where([
				'gym_id' => $gym_id,
				'id_number' => $id_number
			]);
		
		if($except_subscription_id) {
			$subscription = $subscription->where("subscribers.subscription_id","!=",$except_subscription_id);
		}

		$subscription = $subscription->get()->first();

		if($subscription AND $to_array) $subscription = $subscription->toArray();
		return $subscription;
	}
}
