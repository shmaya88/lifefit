<?php namespace App\Services;

class MainService {
	public static $error_msg = '';

	# ---------------------------------------------------------------------------
	public static function setError($errorStr='', $return_value=false){
		self::$error_msg = $errorStr;
		return $return_value;
	}

	# ---------------------------------------------------------------------------
	public static function getError(){
		return self::$error_msg;
	}
}
