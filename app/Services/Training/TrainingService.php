<?php namespace App\Services\Training;

# Models
use App\Models\Training\Training as TrainingModal;

# Services
use App\Services\MainService as MS;
use App\Services\Common\MainCommonService;

# Helpers
use App\Helpers\ValidationHelper;
use App\Helpers\ArrHelper as Arr;
use App\Helpers\StrHelper as Str;
use App\Helpers\QueryHelper as QH;

use Illuminate\Support\Facades\Auth;
use DB;

class TrainingService extends MS {
	public $users_types = [];

	# ---------------------------------------------------------------------------
	public function trainingList($properties=[]){
		# <!--- Set variables default values --!>
		$q_params = QH::listQueryPrepareParams($properties);
		if(!$q_params) return self::setError('שגיאה בעיבוד נתוני מנוי.');

		$svars = MainCommonService::getSvars();
		$users_types_by_name = $svars['users_types']['by_name'];
		$auth_user = $svars['user_data'];

		if(!$q_params['sort_by']) $q_params['sort_by'] = 'name';
		$deleted = ($q_params['deleted'] == 'deleted'?1:0);		
		
		$training_modal = new TrainingModal();

		# <!--- Create subscribers list query --!>
		$training_list_query = $training_modal->select([DB::raw(
			"SQL_CALC_FOUND_ROWS "
			."training_templates.*, training_templates.name AS training_name, "
			."CONCAT(users.first_name, ' ', users.last_name) AS user_full_name, "
			."gyms.name AS gym_name , gyms_branches.name AS gym_branch_name"
		)])
		->leftJoin('users', 'users.id', '=', 'training_templates.user_id')
		->leftJoin('gyms', 'gyms.gym_id', '=', 'training_templates.gym_id')
		->leftJoin('gyms_branches', 'gyms_branches.gym_branch_id', '=', 'training_templates.gym_branch_id')
		->where('training_templates.deleted','=',$deleted)
		->orderBy($q_params['sort_by'], $q_params['sort_direction'])
		->skip($q_params['rows_start'])->take($q_params['rows_limit']);

		# <!--- Permissions conditions --!>
		if($auth_user['type_name'] != 'super_user') {
			$prm_str = " training_templates.gym_id = '$auth_user[gym_id]' ";
			
			if($auth_user['type_name'] == 'basic_user') {
				$prm_str .= "AND training_templates.user_id = '$auth_user[user_id]' ";
			}

			if($auth_user['type_name'] != 'manager_user') {
				$prm_str .= "AND training_templates.gym_branch_id = '$auth_user[gym_branch_id]' ";
			}

			$training_list_query->whereRaw(
				"("
					."($prm_str) OR "
					."("
						."training_templates.gym_id = 0 AND "
						."training_templates.gym_branch_id = 0 AND "
						."training_templates.user_id = 0"
					.")"
				.")"
			);
		}

		# <!--- Search text conditions --!>
		QH::makeWhereSearch(
			$training_list_query, 
			$q_params['search_text'], 
			['training_templates.name']
		);

		# <!--- Run query --!>
		$training_list = $training_list_query->get()->toArray();

		if($training_list) {
			# <!--- Get total rows count --!>
			$q_params['total_rows_count'] = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;

			foreach ($training_list as $t_k => $training_r) {
				if($training_r['user_id']) {
					$training_list[$t_k]['view_permissions'] = $training_list[$t_k]['user_full_name'];
					$training_list[$t_k]['view_permissions_type'] = 'משתמש';
				}
				elseif($training_r['gym_branch_id']) {
					$training_list[$t_k]['view_permissions'] = $training_list[$t_k]['gym_branch_name'];
					$training_list[$t_k]['view_permissions_type'] = 'סניף';
				}
				elseif($training_r['gym_id']) {
					$training_list[$t_k]['view_permissions'] = $training_list[$t_k]['gym_name'];
					$training_list[$t_k]['view_permissions_type'] = 'כל החברה';
				}
				else {
					$training_list[$t_k]['view_permissions'] = 'כולם';
					$training_list[$t_k]['view_permissions_type'] = 'כולם';
				}
			}
		}

		return QH::listQueryReturnData($q_params, $training_list);
	}

	# ---------------------------------------------------------------------------
	public function addTraining($training_data=null){
		# <!--- Required variables definition --!>
		if(!$training_data) return self::setError('לא אותרו נתוני האימון.');
		
		$description = trim(Arr::getValByIndex($training_data, 'description', ''));
		$name = trim(Arr::getValByIndex($training_data, 'name', ''));

		# <!--- Required variables check --!>
		if(!$description) return self::setError('לא הוגדר תיאור/פירוט.');
		elseif(!$name) return self::setError('לא הוגדר שם.');

		$svars = MainCommonService::getSvars();
		$auth_user = $svars['user_data'];

		# <!--- Set default permissions params --!>
		$permissions_params = array(
			'user_id' => 0,
			'gym_id' => 0,
			'gym_branch_id' => 0
		);

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') {
			if($auth_user['type_name'] == 'basic_user') $permissions_params['user_id'] = $auth_user['user_id'];
			if($auth_user['type_name'] != 'manager_user') $permissions_params['gym_branch_id'] = $auth_user['gym_branch_id'];

			$permissions_params['gym_id'] = $auth_user['gym_id'];
		}

		# <!--- Check if the training with the same name, does not exist in the DB --!>
		$existing_training = TrainingModal::select()
			->where(array_merge($permissions_params, ['name'=>$name]))
			->get()->toArray();
		
		if($existing_training) return self::setError('תבנית עם שם זהה קיימת ברמת ההרשאה שלך.');

		$training_id = TrainingModal::insertGetId(
			array_merge(
				$permissions_params, 
				[
					'name' => $name, 
					'description' => $description,
					'created_by' => $auth_user['id'],
					'updated_by' =>$auth_user['id']
				]
			)
		);

		if(!$training_id) return self::setError('פעולת הוספת התבנית נכשלה.');
		return $training_id;
	}

	# ---------------------------------------------------------------------------
	public function editTraining($training_data=null){
		# <!--- Required variables definition --!>
		if(!$training_data) return self::setError('לא אותרו נתוני האימון.');
		
		$training_id = (int)Arr::getValByIndex($training_data, 'training_id', 0);
		$description = trim(Arr::getValByIndex($training_data, 'description', ''));
		$name = trim(Arr::getValByIndex($training_data, 'name', ''));

		# <!--- Required variables check --!>
		if(!$training_id) return self::setError('מזהה אימון אינו תקין.');
		elseif(!$description) return self::setError('לא הוגדר תיאור/פירוט.');
		elseif(!$name) return self::setError('לא הוגדר שם.');

		$svars = MainCommonService::getSvars();
		$auth_user = $svars['user_data'];

		# <!--- Get training data from DB --!>
		$training_to_update_data = $this->getTrainingById($training_id);
		if(!$training_to_update_data) return self::setError('רשומת האימון לא אותרה.');

		# <!--- Set default permissions params --!>
		$permissions_params = array(
			'user_id' => 0,
			'gym_id' => 0,
			'gym_branch_id' => 0
		);

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') {
			if($auth_user['gym_id'] != $training_to_update_data['gym_id']) {
				return self::setError('שגיאת הרשאה. אינך מורשה לעדכן תבנית אימון זו.[gym_id]');
			}

			if($auth_user['type_name'] == 'basic_user') {
				if($auth_user['user_id'] != $training_to_update_data['user_id']) {
					return self::setError('שגיאת הרשאה. אינך מורשה לעדכן תבנית אימון זו.[user_id]');
				}
				$permissions_params['user_id'] = $auth_user['user_id'];
			}
			
			if($auth_user['type_name'] != 'manager_user') {
				if($auth_user['gym_branch_id'] != $training_to_update_data['gym_branch_id']) {
					return self::setError('שגיאת הרשאה. אינך מורשה לעדכן תבנית אימון זו.[gym_branch_id]');
				}
				$permissions_params['gym_branch_id'] = $auth_user['gym_branch_id'];
			}

			$permissions_params['gym_id'] = $auth_user['gym_id'];
		}

		# <!--- Check if the training with the same name, does not exist in the DB --!>
		if($training_to_update_data['name'] != $name) {
			$existing_training = TrainingModal::select()
				->where(array_merge($permissions_params, ['name'=>$name]))
				->get()->toArray();
			
			if($existing_training) return self::setError('תבנית עם שם זהה קיימת ברמת ההרשאה שלך.');
		}

		$update_success = TrainingModal::where('training_id', $training_id)
			->update([
				'name' => $name, 
				'description' => $description,
				'updated_by' => $auth_user['user_id']
			]);

		if(!$update_success AND $update_success!=0) return self::setError('פעולת עדכון תבנית האימון נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreTraining($action_data=null){
		if(!$action_data) return self::setError('לא אותרו נתוני הפעולה.');

		$training_id = (int)Arr::getValByIndex($action_data, 'training_id', 0);
		$action = trim(Arr::getValByIndex($action_data, 'action', ''));

		if(!$action OR !in_array($action, ['delete_training','restore_training'])) {
			return self::setError('הפעולה אינה תקינה.');
		}
		elseif(!$training_id) return self::setError('מזהה תבנית האימון אינו תקין.');

		$svars = MainCommonService::getSvars();
		$auth_user = $svars['user_data'];

		# <!--- Get training data from DB --!>
		$training_to_update_data = $this->getTrainingById($training_id);
		if(!$training_to_update_data) return self::setError('רשומת האימון לא אותרה.');

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') {
			if($auth_user['gym_id'] != $training_to_update_data['gym_id']) {
				return self::setError('שגיאת הרשאה. אינך מורשה לעדכן תבנית אימון זו.[gym_id]');
			}

			if($auth_user['type_name'] == 'basic_user') {
				if($auth_user['user_id'] != $training_to_update_data['user_id']) {
					return self::setError('שגיאת הרשאה. אינך מורשה לעדכן תבנית אימון זו.[user_id]');
				}
			}
			
			if($auth_user['type_name'] != 'manager_user') {
				if($auth_user['gym_branch_id'] != $training_to_update_data['gym_branch_id']) {
					return self::setError('שגיאת הרשאה. אינך מורשה לעדכן תבנית אימון זו.[gym_branch_id]');
				}
			}
		}

		$where_array = ['training_id' =>  $training_id];

		if($action == 'restore_training') {
			$query_success = TrainingModal::where($where_array)
				->update([
					'deleted' => 0,
					'updated_by' => $auth_user['user_id']
				]);
		}
		elseif($action == 'delete_training') {
			$query_success = TrainingModal::where($where_array)
				->update([
					'deleted' => 1, 
					'updated_by' => $auth_user['user_id']
				]);
		}

		if(!$query_success) return self::setError('פעולת עדכון תבנית האימון נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function getTrainingById($training_id='', $properties=[]){
		if(!$training_id) return self::setError('מזהה תבנית אימון אינו תקין.');

		$to_array = Arr::getValByIndex($properties, 'to_array', true);

		$training = TrainingModal::select()
			->where("training_id","=",$training_id)
			->get()->first();

		if($training AND $to_array) $training = $training->toArray();

		return $training;
	}
}
