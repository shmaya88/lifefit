<?php namespace App\Services\SharedMessages;

# Models
use App\Models\SharedMessages\SharedMessage as SharedMessageModal;

# Services
use App\Services\MainService as MS;
use App\Services\Common\MainCommonService;

# Helpers
use App\Helpers\ValidationHelper;
use App\Helpers\ArrHelper as Arr;
use App\Helpers\StrHelper as Str;
use App\Helpers\QueryHelper as QH;

use Illuminate\Support\Facades\Auth;
use DB;

class SharedMessagesService extends MS {
	# ---------------------------------------------------------------------------
	public function sharedMessageList($properties=null){
		$q_params = QH::listQueryPrepareParams($properties);
		if(!$q_params) return self::setError('שגיאה בעיבוד נתוני מנוי.');
		
		if(!$q_params['sort_by']) $q_params['sort_by'] = 'created_at';
		$deleted = ($q_params['deleted'] == 'deleted'?1:0);
		
		$gym_id_filtering = (int)Arr::getValByIndex($properties, 'gym_id', null);
		$get_general_messages = Arr::getValByIndex($properties, 'get_general_messages', false);
		
		$gym_id_filtering = ($gym_id_filtering ? [$gym_id_filtering] : null);

		$svars = MainCommonService::getSvars();
		$auth_user = $svars['user_data'];

		$shared_message_modal = new SharedMessageModal();

		# <!--- Create shared_messages list query --!>
		$shared_message_list_query = $shared_message_modal->select([DB::raw(
			"SQL_CALC_FOUND_ROWS "
			."shared_messages.message_id, shared_messages.gym_id, shared_messages.title, "
			."shared_messages.description, shared_messages.expiry_date, gyms.name AS gym_name, "
			."shared_messages.created_at, shared_messages.created_by, users.gym_branch_id, "
			."CONCAT(users.first_name, ' ', users.last_name) AS user_full_name "
		)])
		->leftJoin('users', 'users.id', '=', 'shared_messages.created_by')
		->leftJoin('gyms', 'gyms.gym_id', '=', 'shared_messages.gym_id')
		->where('shared_messages.deleted', '=' ,$deleted)
		->orderBy($q_params['sort_by'], $q_params['sort_direction'])
		->skip($q_params['rows_start'])->take($q_params['rows_limit']);

		if($q_params['deleted'] == 'expired') {
			$shared_message_list_query->where('shared_messages.expiry_date', '<' ,date("Y-m-d"));
		}
		elseif($q_params['deleted'] == 'active') {
			$shared_message_list_query->where(function($query) {
				$query->orWhere('shared_messages.expiry_date', '>=' ,date("Y-m-d"));				
				$query->orWhere('shared_messages.expiry_date', '=' ,'0000-00-00');
			});
		}

		# <!--- Permissions conditions --!>
		if($auth_user['type_name'] != 'super_user') {			
			if(!$gym_id_filtering) {
				$gym_id_filtering = [$auth_user['gym_id']];
				if($get_general_messages) $gym_id_filtering[] = 0;
			}
		}
		elseif($get_general_messages AND !$gym_id_filtering) $gym_id_filtering = [0];
		
		if($gym_id_filtering AND is_array($gym_id_filtering)) {
			$shared_message_list_query->whereIn('shared_messages.gym_id', $gym_id_filtering);
		}

		# <!--- Search text conditions --!>
		QH::makeWhereSearch(
			$shared_message_list_query, 
			$q_params['search_text'], 
			['shared_messages.title', 'gyms.name']
		);
		
		# <!--- Run query --!>
		$shared_messages_list = $shared_message_list_query->get()->toArray();
		
		if($shared_messages_list) {
			# <!--- Get total rows count --!>
			$q_params['total_rows_count'] = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;
			
			foreach ($shared_messages_list as $k => $row) {
				$new_expiry_date = Str::dateFromYMDtoDMY($row['expiry_date']);
				$new_created_at = Str::dateFromYMDtoDMY($row['created_at']);
				
				$shared_messages_list[$k]['expiry_date'] = $row['expiry_date'] = $new_expiry_date;
				$shared_messages_list[$k]['created_at'] = $row['created_at'] = $new_created_at;
				
				if(!$row['user_full_name']) $shared_messages_list[$k]['user_full_name'] = 'לא מוגדר';
				if(!$row['gym_name']) $shared_messages_list[$k]['gym_name'] = 'כל החברות';
				if(!$row['expiry_date']) $shared_messages_list[$k]['expiry_date'] = 'לא מוגדר';
			}
		}

		return QH::listQueryReturnData($q_params, $shared_messages_list);
	}

	# ---------------------------------------------------------------------------
	public function addSharedMessage($shared_messages_data=[]){
		# <!--- Required variables definition --!>
		if(!$shared_messages_data) return self::setError('לא אותרו נתוני ההודעה.');

		$description = trim(Arr::getValByIndex($shared_messages_data, 'description', ''));
		$title = trim(Arr::getValByIndex($shared_messages_data, 'title', ''));
		$expiry_date = Str::dateFromDMYtoYMD(Arr::getValByIndex($shared_messages_data, 'expiry_date', ''));
		$gym_id = (int)Arr::getValByIndex($shared_messages_data, 'gym_id', 0);

		$svars = MainCommonService::getSvars();
		$auth_user = $svars['user_data'];
		
		# <!--- Required variables check --!>
		if(!$description) return self::setError('לא הוגדר תוכן ההודעה.');
		elseif(!$title) return self::setError('לא הוגדרה כותרת.');		
		elseif($auth_user['type_name'] == 'basic_user') return self::setError('שגיאת הרשאה!');
		elseif(!$gym_id AND $auth_user['type_name'] != 'super_user') $gym_id = $auth_user['gym_id'];

		$message_id = SharedMessageModal::insertGetId([
			'gym_id' => $gym_id,
			'title' => $title,
			'expiry_date' => $expiry_date,
			'description' => $description,
			'created_by' => $auth_user['id'],
			'updated_by' =>$auth_user['id']
		]);

		if(!$message_id) return self::setError('פעולת הוספת ההודעה נכשלה.');
		return $message_id;
	}

	# ---------------------------------------------------------------------------
	public function editSharedMessage($shared_messages_data=[]){
		# <!--- Required variables definition --!>
		if(!$shared_messages_data) return self::setError('לא אותרו נתוני ההודעה.');

		$description = trim(Arr::getValByIndex($shared_messages_data, 'description', ''));
		$title = trim(Arr::getValByIndex($shared_messages_data, 'title', ''));
		$expiry_date = Str::dateFromDMYtoYMD(Arr::getValByIndex($shared_messages_data, 'expiry_date', ''));
		$message_id = (int)Arr::getValByIndex($shared_messages_data, 'message_id', 0);
		$gym_id = (int)Arr::getValByIndex($shared_messages_data, 'gym_id', 0);

		$svars = MainCommonService::getSvars();
		$auth_user = $svars['user_data'];

		# <!--- Required variables check --!>
		if(!$message_id) return self::setError('מזהה ההודעה אינו תקין.');
		elseif(!$description) return self::setError('לא הוגדר תוכן ההודעה.');
		elseif(!$title) return self::setError('לא הוגדרה כותרת.');		
		elseif($auth_user['type_name'] == 'basic_user') return self::setError('שגיאת הרשאה!');
		elseif(!$gym_id AND $auth_user['type_name'] != 'super_user') $gym_id = $auth_user['gym_id'];

		$update_success = SharedMessageModal::where('message_id', $message_id)
			->update([
				'gym_id' => $gym_id,
				'title' => $title,
				'expiry_date' => $expiry_date,
				'description' => $description,
				'updated_by' =>$auth_user['id']
			]);

		if(!$update_success AND $update_success!=0) return self::setError('פעולת עדכון ההודעה נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreSharedMessage($action_data=[]){
		if(!$action_data) return self::setError('לא אותרו נתוני הפעולה.');

		$message_id = (int)Arr::getValByIndex($action_data, 'message_id', 0);
		$action = trim(Arr::getValByIndex($action_data, 'action', ''));

		if(!$action OR !in_array($action, ['delete_shared_message','restore_shared_message'])) {
			return self::setError('הפעולה אינה תקינה.');
		}
		elseif(!$message_id) return self::setError('מזהה ההודעה אינו תקין.');

		if($action == 'restore_shared_message') $deleted = 0;
		elseif($action=='delete_shared_message') $deleted = 1;

		$query_success = SharedMessageModal::where(['message_id' =>  $message_id])
			->update([
				'deleted' => $deleted, 
				'updated_by' => Auth::user()->id
			]);

		if(!$query_success) return self::setError('פעולת עדכון המשתמש נכשלה.');

		return true;
	}
}
