<?php namespace App\Services\Notifications;

use Auth;
use DB;

# Services
use App\Services\MainService as MS;
use App\Services\Common\MainCommonService;

# Models
use App\Models\Notifications\ServiceNotifications AS ServiceNotificationsModal;

# Helpers
use App\Helpers\ArrHelper as Arr;
use App\Helpers\StrHelper as Str;
use App\Helpers\QueryHelper as QH;

class NotificationsService extends MS {
	# ---------------------------------------------------------------------------
	public function notificationsList($properties=[]){
		$q_params = QH::listQueryPrepareParams($properties);
		if(!$q_params) return self::setError('שגיאה בעיבוד נתוני מנוי.');

		if(!$q_params['sort_by']) $q_params['sort_by'] = 'created_at';
		$deleted = ($q_params['deleted'] == 'deleted'?1:0);

		$svars = MainCommonService::getSvars();
		$users_types_by_name = $svars['users_types']['by_name'];
		$auth_user = $svars['user_data'];

		$status_condition = trim(Arr::getValByIndex($properties, 'status', ''));

		# <!--- Create notifications list query --!>
		$notifications_list_query = ServiceNotificationsModal::select([DB::raw(
			"SQL_CALC_FOUND_ROWS "
			."service_notifications.notification_id, "
			."service_notifications.title, service_notifications.description, "
			."service_notifications.status, service_notifications.created_at, "
			."CONCAT(users.first_name, ' ', users.last_name) AS user_full_name, "
			."gyms.name AS gym_name, gyms_branches.name AS gym_branch_name "
		)])
		->leftJoin('users', 'users.id', '=', 'service_notifications.created_by')
		->leftJoin('gyms', 'gyms.gym_id', '=', 'users.gym_id')
		->leftJoin('gyms_branches', 'gyms_branches.gym_branch_id', '=', 'users.gym_branch_id')
		->where('service_notifications.deleted','=',$deleted)
		->orderBy($q_params['sort_by'], $q_params['sort_direction'])
		->skip($q_params['rows_start'])->take($q_params['rows_limit']);
		
		if($status_condition != 'all') {
			$notifications_list_query->where('service_notifications.status', $status_condition);
		}

		# <!--- Search text conditions --!>
		QH::makeWhereSearch(
			$notifications_list_query, 
			$q_params['search_text'], 
			['service_notifications.title', 'users.first_name', 'users.last_name']
		);

		# <!--- Run query --!>
		$notifications_list = $notifications_list_query->get()->toArray();

		if($notifications_list) {
			# <!--- Get total rows count --!>
			$q_params['total_rows_count'] = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;

			foreach ($notifications_list as $not_k => $notification) {
				$new_created_at = Str::dateFromYMDtoDMY($notification['created_at']);

				$notifications_list[$not_k]['created_at'] = $notification['created_at'] = $new_created_at;

				if((int)$notification['status']==1) $status_text = 'טופל';
				else $status_text = 'טרם טופל';
				$notifications_list[$not_k]['status_text'] = $notification['status_text'] = $status_text;

				if(!$notification['user_full_name']) $notifications_list[$not_k]['user_full_name'] = 'לא מוגדר';
				if(!$notification['gym_name']) $notifications_list[$not_k]['gym_name'] = 'לא מוגדר';
				if(!$notification['gym_branch_name']) $notifications_list[$not_k]['gym_branch_name'] = 'לא מוגדר';
			}
		}

		return QH::listQueryReturnData($q_params, $notifications_list);
	}

	# ---------------------------------------------------------------------------
	public function addNotification($notifications_data=null){
		# <!--- Required variables definition --!>
		if(!$notifications_data) return self::setError('לא אותרו נתוני האימון.');
		
		$description = trim(Arr::getValByIndex($notifications_data, 'description', ''));
		$title = trim(Arr::getValByIndex($notifications_data, 'title', ''));

		# <!--- Required variables check --!>
		if(!$description) return self::setError('לא הוגדר תיאור.');
		elseif(!$title) return self::setError('לא הוגדרה כותרת.');

		$description = str_replace(["\r\n", "\n", "\r"], "</br>", $description);

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();

		$notification_id = ServiceNotificationsModal::insertGetId([
			'title' => $title, 
			'description' => $description,
			'created_by' => $auth_user['id'],
			'updated_by' =>$auth_user['id']
		]);

		if(!$notification_id) return self::setError('פעולת הוספת ההודעה נכשלה.');
		return $notification_id;
	}

	# ---------------------------------------------------------------------------
	public function setNotificationStatus($notifications_data=null){
		# <!--- Required variables definition --!>
		if(!$notifications_data) return self::setError('לא אותרו נתוני האימון.');
		
		$notification_id = Arr::getValByIndex($notifications_data, 'notification_id', '');
		$new_status_code = Arr::getValByIndex($notifications_data, 'new_status_code', '');

		# <!--- Required variables check --!>
		if(!$notification_id) return self::setError('לא אותר מזהה.');

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();

		ServiceNotificationsModal::where('notification_id', $notification_id)
			->update([
				'status' => $new_status_code,
				'updated_by' =>$auth_user['id']
			]);
		
		return true;
	}
}
