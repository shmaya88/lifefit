<?php namespace App\Services\Gyms;

# Models
use App\Models\Gyms\Gym;
use App\Models\Gyms\‏‏GymBranch;

# Services
use App\Services\MainService as MS;
use App\Services\Addresses\AddressesService;
use App\Services\Communications\CommunicationsService;
use App\Services\Common\MainCommonService;

# Helpers
use App\Helpers\ArrHelper as Arr;
use App\Helpers\StrHelper as Str;
use App\Helpers\QueryHelper as QH;

use DB;

class GymsService extends MS {
	public $user_data = [];

	# ---------------------------------------------------------------------------
	public function getGymsList($properties=[]){
		# <!--- Set variables default values --!>
		$addresses_list = $communications_list = $gyms_ids = [];

		$q_params = QH::listQueryPrepareParams($properties);
		if(!$q_params) return self::setError('שגיאה בעיבוד נתוני מנוי.');

		$gym_id_filtering = (int)Arr::getValByIndex($properties, 'gym_id', 0);

		if(!$q_params['sort_by']) $q_params['sort_by'] = 'gym_name';
		$deleted = ($q_params['deleted'] == 'deleted'?1:0);

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();

		$gym_modal = new Gym;
		
		# <!--- Create gyms list query --!>
		$gyms_list_query = $gym_modal->select([DB::raw(
			"SQL_CALC_FOUND_ROWS gyms.*, gyms.name AS gym_name "
		)])
		->where('gyms.deleted','=',$deleted)
		->orderBy($q_params['sort_by'], $q_params['sort_direction'])
		->skip($q_params['rows_start'])->take($q_params['rows_limit']);
		
		if($auth_user['type_name'] != "super_user") $gym_id_filtering = $auth_user['gym_id'];

		# <!--- Filtering conditions --!>
		if($gym_id_filtering) $gyms_list_query->where('gyms.gym_id', $gym_id_filtering);

		# <!--- Search text conditions --!>
		QH::makeWhereSearch(
			$gyms_list_query, 
			$q_params['search_text'], 
			['gyms.name', 'gyms.id_number']
		);

		# <!--- Run query --!>
		$gyms_list = $gyms_list_query->get()->toArray();

		if($gyms_list) {
			# <!--- Get total rows count --!>
			$q_params['total_rows_count'] = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;

			foreach ($gyms_list as $k => $gym_d) {
				# <!--- Set gyms ids array --!>
				$gyms_ids[$gym_d['gym_id']] = $gym_d['gym_id'];
			}

			if($gyms_ids) {
				$gyms_ids = array_keys($gyms_ids);

				# <!--- Get gyms addresses and communications --!>
				$adr_comm_params = array(
					'owners_ids_array' => $gyms_ids, 
					'owner_type' => 'gym'
				);

				$addresses_service = new AddressesService($adr_comm_params);
				$addresses_list = $addresses_service->getAddressesList();

				$communications_service = new CommunicationsService($adr_comm_params);
				$communications_list = $communications_service->getCommunicationsList();

				if(!$addresses_list AND $addresses_service->getError()) {
					return self::setError($addresses_service->getError());
				}

				if(!$communications_list AND $communications_service->getError()) {
					return self::setError($communications_service->getError());
				}
			}

			foreach ($gyms_list as $k => $gym_d) {
				# <!--- Set gyms ids array --!>
				if($addresses_list AND isset($addresses_list[$gym_d['gym_id']])) {					
					$gyms_list[$k] = array_merge(
						$gyms_list[$k], 
						$addresses_list[$gym_d['gym_id']]
					);
				}

				if($communications_list AND isset($communications_list[$gym_d['gym_id']])) {					
					$gyms_list[$k] = array_merge(
						$gyms_list[$k], 
						$communications_list[$gym_d['gym_id']]
					);
				}
			}
		}

		return QH::listQueryReturnData($q_params, $gyms_list);
	}

	# ---------------------------------------------------------------------------
	public function getGymsBranchesList($properties=[]){
		# <!--- Set variables default values --!>
		$addresses_list = $communications_list = $gyms_branches_ids = [];

		$q_params = QH::listQueryPrepareParams($properties);
		if(!$q_params) return self::setError('שגיאה בעיבוד נתוני מנוי.');

		$gym_id_filtering = (int)Arr::getValByIndex($properties, 'gym_id', 0);

		if(!$q_params['sort_by']) $q_params['sort_by'] = 'gym_branch_name';
		$deleted = ($q_params['deleted'] == 'deleted'?1:0);

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();

		$gym_branch_modal = ‏‏GymBranch::select();

		# <!--- Create gyms branches list query --!>
		$gyms_branches_list_query = $gym_branch_modal->select([DB::raw(
				"SQL_CALC_FOUND_ROWS gyms_branches.*, gyms_branches.name AS gym_branch_name, gyms.name AS gym_name "
			)])
			->leftJoin('gyms', 'gyms.gym_id', '=', 'gyms_branches.gym_id')
			->where('gyms_branches.deleted','=',$deleted)
			->orderBy($q_params['sort_by'], $q_params['sort_direction'])
			->skip($q_params['rows_start'])->take($q_params['rows_limit']);
		
		# <!--- Permissions conditions --!>
		if($auth_user['type_name'] != "super_user") {
			$gym_id_filtering = $auth_user['gym_id'];
		}

		# <!--- Filtering conditions --!>
		if($gym_id_filtering) $gyms_branches_list_query->where('gyms.gym_id', $gym_id_filtering);

		# <!--- Search text conditions --!>
		QH::makeWhereSearch(
			$gyms_branches_list_query, 
			$q_params['search_text'], 
			['gyms_branches.name']
		);

		# <!--- Run query --!>
		$gyms_branches_list = $gyms_branches_list_query->get()->toArray();

		if($gyms_branches_list) {
			# <!--- Get total rows count --!>
			$q_params['total_rows_count'] = DB::select(DB::raw("SELECT FOUND_ROWS() AS 'total';"))[0]->total;

			foreach ($gyms_branches_list as $k => $gym_branch_d) {
				# <!--- Set gyms branches ids array --!>
				$gyms_branches_ids[$gym_branch_d['gym_branch_id']] = $gym_branch_d['gym_branch_id'];
			}

			if($gyms_branches_ids) {
				$gyms_branches_ids = array_keys($gyms_branches_ids);

				# <!--- Get gyms addresses and communications --!>
				$adr_comm_params = array(
					'owners_ids_array' => $gyms_branches_ids, 
					'owner_type' => 'gym_branch'
				);

				$addresses_service = new AddressesService($adr_comm_params);
				$addresses_list = $addresses_service->getAddressesList();

				$communications_service = new CommunicationsService($adr_comm_params);
				$communications_list = $communications_service->getCommunicationsList();

				if(!$addresses_list AND $addresses_service->getError()) {
					return self::setError($addresses_service->getError());
				}

				if(!$communications_list AND $communications_service->getError()) {
					return self::setError($communications_service->getError());
				}
			}

			foreach ($gyms_branches_list as $k => $gym_branch_d) {
				# <!--- Set gyms ids array --!>
				if($addresses_list AND isset($addresses_list[$gym_branch_d['gym_branch_id']])) {					
					$gyms_branches_list[$k] = array_merge(
						$gyms_branches_list[$k], 
						$addresses_list[$gym_branch_d['gym_branch_id']]
					);
				}

				if($communications_list AND isset($communications_list[$gym_branch_d['gym_branch_id']])) {					
					$gyms_branches_list[$k] = array_merge(
						$gyms_branches_list[$k], 
						$communications_list[$gym_branch_d['gym_branch_id']]
					);
				}
			}
		}

		return QH::listQueryReturnData($q_params, $gyms_branches_list);
	}

	# ---------------------------------------------------------------------------
	public function addGym($gym_data=null){
		# <!--- Required variables definition --!>
		if(!$gym_data) return self::setError('לא אותרו נתוני החברה/רשת.');

		$name = trim(Arr::getValByIndex($gym_data, 'name', ''));
		$id_number = trim(Arr::getValByIndex($gym_data, 'id_number', ''));

		# <!--- Required variables check --!>
		if(!$name) return self::setError('שם החברה/רשת אינו תקין.');
		elseif(!$id_number) return self::setError('ח.פ./ע.מ. החברה/רשת אינו תקין.');

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') return self::setError('שגיאת הרשאה! אין באפשרותך להוסיף חברה/רשת.');

		# <!--- Check if the gym does not exist in the DB --!>
		$existing_gym = $this->getGymByIdNumOrName($id_number, $name);
		if($existing_gym) {
			if($existing_gym['id_number'] == $id_number) return self::setError('קיימת חברה/רשת עם ח.פ./ע.מ. זהה.');
			elseif($existing_gym['name'] == $name) return self::setError('קיימת חברה/רשת עם שם זהה.');
		}
		elseif(self::getError()) return false;

		$gym_id = Gym::insertGetId([
			'name' => $name,
			'id_number' => $id_number,
			'created_by' => $auth_user['id'],
			'updated_by' =>$auth_user['id']
		]);

		if(!$gym_id) return self::setError('פעולת הוספת ח.פ./ע.מ. נכשלה.');

		# <!--- Insert gym addresses & communications --!>
		$action_result = $common_service->addAddressesAndCommunications($gym_id, 'gym', $gym_data);
		if(!$action_result) return self::setError($common_service->getError());
		
		return $gym_id;
	}

	# ---------------------------------------------------------------------------
	public function addGymBranch($gym_branch_data=null){
		# <!--- Required variables definition --!>
		if(!$gym_branch_data) return self::setError('לא אותרו נתוני החברה/רשת.');

		$gym_id = (int)Arr::getValByIndex($gym_branch_data, 'gym_id', 0);
		$gym_branch_name = trim(Arr::getValByIndex($gym_branch_data, 'gym_branch_name', ''));

		# <!--- Required variables check --!>
		if(!$gym_branch_name) return self::setError('שם הסניף אינו תקין.');
		elseif(!$gym_id) return self::setError('מזהה החברה/רשת אינו תקין.');

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();
		
		# <!--- Check permissions --!>		
		if(!in_array($auth_user['type_name'], ['super_user','manager_user'])) {
			return self::setError('שגיאת הרשאה! אין באפשרותך להוסיף סניף.');
		}
		elseif($auth_user['type_name'] != 'super_user' AND $auth_user['gym_id'] != $gym_id) {
			return self::setError('שגיאת הרשאה! אין באפשרותך להוסיף סניף - מזהה חברה אינו תואם.');
		}

		# <!--- Check if the gym branch does not exist in the DB --!>
		$existing_gym_branch = $this->getGymBranchByName($gym_branch_name, $gym_id);
		if($existing_gym_branch) return self::setError('קיים סניף עם שם זהה.');
		elseif(self::getError()) return false;

		$gym_branch_id = ‏‏GymBranch::insertGetId([
			'gym_id' => $gym_id,
			'name' => $gym_branch_name,
			'created_by' => $auth_user['id'],
			'updated_by' =>$auth_user['id']
		]);

		if(!$gym_branch_id) return self::setError('פעולת הוספת סניף נכשלה.');

		# <!--- Insert gym branch addresses & communications --!>
		$action_result = $common_service->addAddressesAndCommunications($gym_branch_id, 'gym_branch', $gym_branch_data);
		if(!$action_result) return self::setError($common_service->getError());
		
		return $gym_branch_id;
	}

	# ---------------------------------------------------------------------------
	public function editGym($gym_data=null){
		# <!--- Required variables definition --!>
		if(!$gym_data) return self::setError('לא אותרו נתוני החברה/רשת.');
		
		$gym_id = (int)Arr::getValByIndex($gym_data, 'gym_id', 0);
		$name = trim(Arr::getValByIndex($gym_data, 'name', ''));
		$id_number = trim(Arr::getValByIndex($gym_data, 'id_number', ''));

		# <!--- Required variables check --!>
		if(!$gym_id) return self::setError('מזהה החברה/רשת אינו תקין.');
		elseif(!$name) return self::setError('שם החברה/רשת אינו תקין.');
		elseif(!$id_number) return self::setError('ח.פ./ע.מ. החברה/רשת אינו תקין.');

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') return self::setError('שגיאת הרשאה! אין באפשרותך לערוך חברה/רשת.');

		# <!--- Check if the gym does not exist in the DB --!>
		$existing_gym = $this->getGymByIdNumOrName($id_number, $name, ['except_gym_id'=>$gym_id]);
		if($existing_gym) {
			if($existing_gym['id_number'] == $id_number) return self::setError('קיימת חברה/רשת עם ח.פ./ע.מ. זהה.');
			elseif($existing_gym['name'] == $name) return self::setError('קיימת חברה/רשת עם שם זהה.');
		}
		elseif(self::getError()) return false;

		$update_success = Gym::where('gym_id', $gym_id)
			->update([
				'name' => $name,
				'id_number' => $id_number,
				'updated_by' => $auth_user['user_id']
			]);

		if(!$update_success AND $update_success!=0) return self::setError('פעולת עדכון החברה/רשת נכשלה.');

		# <!--- Update subscription addresses & communications --!>
		$action_result = $common_service->addAddressesAndCommunications($gym_id, 'gym', $gym_data);
		if(!$action_result) return self::setError($common_service->getError());

		return true;
	}

	# ---------------------------------------------------------------------------
	public function editGymBranch($gym_branch_data=null){
		# <!--- Required variables definition --!>
		if(!$gym_branch_data) return self::setError('לא אותרו נתוני החברה/רשת.');

		$gym_id = (int)Arr::getValByIndex($gym_branch_data, 'gym_id', 0);
		$gym_branch_id = (int)Arr::getValByIndex($gym_branch_data, 'gym_branch_id', 0);
		$gym_branch_name = trim(Arr::getValByIndex($gym_branch_data, 'gym_branch_name', ''));

		# <!--- Required variables check --!>
		if(!$gym_branch_name) return self::setError('שם הסניף אינו תקין.');
		elseif(!$gym_id) return self::setError('מזהה החברה/רשת אינו תקין.');
		elseif(!$gym_branch_id) return self::setError('מזהה סניף אינו תקין.');

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();
		
		# <!--- Check permissions --!>		
		if(!in_array($auth_user['type_name'], ['super_user','manager_user'])) {
			return self::setError('שגיאת הרשאה! אין באפשרותך לעדכן סניף.');
		}
		elseif($auth_user['type_name'] != 'super_user' AND $auth_user['gym_id'] != $gym_id) {
			return self::setError('שגיאת הרשאה! אין באפשרותך לעדכן סניף - מזהה חברה אינו תואם.');
		}

		# <!--- Check if the gym branch does not exist in the DB --!>
		$existing_gym_branch = $this->getGymBranchByName(
			$gym_branch_name, 
			$gym_id, 
			['except_gym_branch_id'=>$gym_branch_id]
		);

		if($existing_gym_branch) return self::setError('קיים סניף עם שם זהה.');
		elseif(self::getError()) return false;

		$update_success = ‏‏GymBranch::where('gym_branch_id', $gym_branch_id)
			->update([
				'name' => $gym_branch_name,
				'updated_by' => $auth_user['user_id']
			]);

		if(!$update_success AND $update_success!=0) return self::setError('פעולת עדכון הסניף נכשלה.');

		# <!--- Update gym branch addresses & communications --!>
		$action_result = $common_service->addAddressesAndCommunications($gym_branch_id, 'gym_branch', $gym_branch_data);
		if(!$action_result) return self::setError($common_service->getError());

		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreGym($action_data=null){
		if(!$action_data) return self::setError('לא אותרו נתוני הפעולה.');

		$gym_id = (int)Arr::getValByIndex($action_data, 'gym_id', 0);
		$action = trim(Arr::getValByIndex($action_data, 'action', ''));

		if(!$action OR !in_array($action, ['delete_gym','restore_gym'])) {
			return self::setError('הפעולה אינה תקינה.');
		}
		elseif(!$gym_id) return self::setError('מזהה החברה/רשת אינו תקין.');

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();

		# <!--- Get gym data from DB --!>
		$gym_to_update_data = $this->getGymById($gym_id);
		if(!$gym_to_update_data) return self::setError('רשומת החברה/רשת לא אותרה.');

		# <!--- Check permissions --!>
		if($auth_user['type_name'] != 'super_user') return self::setError('שגיאת הרשאה! אין באפשרותך לשנות חברה/רשת.');

		$where_array = ['gym_id' =>  $gym_id];

		if($action == 'restore_gym') {
			$query_success = Gym::where($where_array)
				->update([
					'deleted' => 0,
					'updated_by' => $auth_user['user_id']
				]);
		}
		elseif($action == 'delete_gym') {
			$query_success = Gym::where($where_array)
				->update([
					'deleted' => 1, 
					'updated_by' => $auth_user['user_id']
				]);
		}

		if(!$query_success) return self::setError('פעולת עדכון חברה/רשת נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function deleteRestoreGymBranch($action_data=null){
		if(!$action_data) return self::setError('לא אותרו נתוני הפעולה.');

		$gym_branch_id = (int)Arr::getValByIndex($action_data, 'gym_branch_id', 0);
		$action = trim(Arr::getValByIndex($action_data, 'action', ''));

		if(!$action OR !in_array($action, ['delete_gym_branch','restore_gym_branch'])) {
			return self::setError('הפעולה אינה תקינה.');
		}
		elseif(!$gym_branch_id) return self::setError('מזהה סניף אינו תקין.');

		$common_service = new MainCommonService;
		$auth_user = $common_service->getUserData();

		# <!--- Get gym branch data from DB --!>
		$gym_branch_to_update_data = $this->getGymBranchById($gym_branch_id);
		if(!$gym_branch_to_update_data) return self::setError('רשומת הסניף לא אותרה.');

		# <!--- Check permissions --!>
		if(!in_array($auth_user['type_name'], ['super_user','manager_user'])) {
			return self::setError('שגיאת הרשאה! אין באפשרותך למחוק/לשחזר סניף.');
		}
		elseif($auth_user['type_name'] != 'super_user' AND $auth_user['gym_id'] != $gym_id) {
			return self::setError('שגיאת הרשאה! אין באפשרותך למחוק/לשחזר סניף - מזהה חברה אינו תואם.');
		}

		$where_array = ['gym_branch_id' =>  $gym_branch_id];

		if($action == 'restore_gym_branch') {
			$query_success = ‏‏GymBranch::where($where_array)
				->update([
					'deleted' => 0,
					'updated_by' => $auth_user['user_id']
				]);
		}
		elseif($action == 'delete_gym_branch') {
			$query_success = ‏‏GymBranch::where($where_array)
				->update([
					'deleted' => 1, 
					'updated_by' => $auth_user['user_id']
				]);
		}

		if(!$query_success) return self::setError('פעולת עדכון סניף נכשלה.');

		return true;
	}

	# ---------------------------------------------------------------------------
	public function getGymByIdNumOrName($id_number=null, $gym_name=null, $properties=[]){
		if(!$id_number) return self::setError('לא אותר מספר ח.פ./ע.מ.[getGymByIdNumOrName]');
		if(!$gym_name) return self::setError('לא אותר שם החברה/רשת. [getGymByIdNumOrName]');

		$to_array = Arr::getValByIndex($properties, 'to_array', true);
		$except_gym_id = (int)Arr::getValByIndex($properties, 'except_gym_id', 0);

		$gym = Gym::select()
			->where(function($query) use ($id_number, $gym_name) {
				$query->orWhere('id_number', $id_number)
					->orWhere('name', $gym_name);
			});

		if($except_gym_id) {
			$gym = $gym->where("gym_id","!=",$except_gym_id);
		}

		$gym = $gym->get()->first();

		if($gym AND $to_array) $gym = $gym->toArray();
		return $gym;
	}

	# ---------------------------------------------------------------------------
	public function getGymById($gym_id='', $properties=[]){
		if(!$gym_id) return self::setError('מזהה החברה/רשת אינו תקין.');

		$to_array = Arr::getValByIndex($properties, 'to_array', true);

		$gym = Gym::select()
			->where("gym_id","=",$gym_id)
			->get()->first();

		if($gym AND $to_array) $gym = $gym->toArray();

		return $gym;
	}

	# ---------------------------------------------------------------------------
	public function getGymBranchById($gym_branch_id='', $properties=[]){
		if(!$gym_branch_id) return self::setError('מזהה סניף אינו תקין.');

		$to_array = Arr::getValByIndex($properties, 'to_array', true);

		$gym_branch = ‏‏GymBranch::select()
			->where("gym_branch_id","=",$gym_branch_id)
			->get()->first();

		if($gym_branch AND $to_array) $gym_branch = $gym_branch->toArray();

		return $gym_branch;
	}

	# ---------------------------------------------------------------------------
	public function getGymBranchByName($gym_branch_name=null, $gym_id=null, $properties=[]){
		if(!$gym_branch_name) return self::setError('לא אותר שם הסניף. [getGymBranchByName]');

		if(!$gym_id) {
			$common_service = new MainCommonService;
			$auth_user = $common_service->getUserData();
			$gym_id = $auth_user['gym_id'];
		}

		$to_array = Arr::getValByIndex($properties, 'to_array', true);
		$except_gym_branch_id = (int)Arr::getValByIndex($properties, 'except_gym_branch_id', 0);

		$gym_branch = ‏‏GymBranch::select()
			->where([
				'gym_id' => $gym_id,
				'name' => $gym_branch_name
			]);

		if($except_gym_branch_id) {
			$gym_branch = $gym_branch->where("gym_branch_id","!=",$except_gym_branch_id);
		}

		$gym_branch = $gym_branch->get()->first();

		if($gym_branch AND $to_array) $gym_branch = $gym_branch->toArray();

		return $gym_branch;
	}
}
