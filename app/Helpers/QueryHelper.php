<?php namespace App\Helpers;

# Helpers
use App\Helpers\ArrHelper as Arr;


class QueryHelper {
	private static $error_msg = '';

	function __construct($construct_data=null) {
		self::$error_msg = '';
    }

	# ---------------------------------------------------------------------------
	public static function getError(){
		return self::$error_msg;
	}
	
	# ---------------------------------------------------------------------------
	public static function listQueryPrepareParams($params=null){
		if(!is_array($params)) {
			self::$error_msg = 'לא אותרו נתונים לעיבוד. ';
			return false;
		}

		$return_params = array();

		$return_params['search_text'] = trim(Arr::getValByIndex($params, 'search_text', ''));
		$return_params['data_index'] = trim(Arr::getValByIndex($params, 'data_index', 'data'));
		$return_params['count_index'] = trim(Arr::getValByIndex($params, 'rows_count_index', ''));
		$return_params['deleted'] = trim(Arr::getValByIndex($params, 'deleted_active_sb', ''));
		$return_params['sort_by'] = trim(Arr::getValByIndex($params, 'sort_by', ''));
		$return_params['sort_direction'] = trim(Arr::getValByIndex($params, 'sort_direction', ''));

		$return_params['rows_start'] = (int)Arr::getValByIndex($params, 'rows_start', 0);
		$return_params['rows_limit'] = (int)Arr::getValByIndex($params, 'rows_limit', 10);

		if(isset($params['rows_limit']) AND trim(strtolower($params['rows_limit']))=='unlimited') {
			$return_params['rows_limit'] = 3000000;
		}
		
		if(!$return_params['data_index']) $return_params['data_index'] = 'data';
		if(!$return_params['count_index']) $return_params['count_index'] = 'rows_count_index';
		if(!$return_params['deleted']) $return_params['deleted'] = 'active';
		if(!$return_params['sort_direction']) $return_params['sort_direction'] = 'ASC';

		return $return_params;
	}

	# ---------------------------------------------------------------------------
	public static function listQueryReturnData($params=null, $data=[]){
		if(!$params OR !is_array($params)) {
			self::$error_msg = 'לא אותרו נתונים.';
			return false;
		}

		if(!isset($params['total_rows_count'])) $params['total_rows_count'] = 0;
		if(!isset($params['data_index']) || !isset($params['count_index'])) {
			self::$error_msg = 'נתונים להחזרה אינם תקינים.';
			return false;
		}

		return array(
			$params['data_index'] => $data, 
			$params['count_index'] => $params['total_rows_count']
		);
	}
	
	# ---------------------------------------------------------------------------
	public static function makeWhereSearch(&$model, $search_string=null, $fields=null){
		$error_msg = '';
		
		if(!$model OR !$fields OR !$search_string) $error_msg = 'No Data Found.';
		elseif(!is_array($fields)) $error_msg = 'The "fields" variable must be a array type';
		elseif(!count($fields)) $error_msg = 'The "fields" variable is empty';
		elseif(!is_string($search_string)) $error_msg = 'The "search_string" variable must be a string type';

		if($error_msg) {
			self::$error_msg = $error_msg;
			return false;
		}

		$search_string = preg_replace("/[-+ ,\n\r\t]+/", " ", $search_string);
		$search_text_array = explode(" ", trim($search_string));

		if(!$search_text_array) return false;

		foreach ($search_text_array as $search_str) {
			$search_str = trim($search_str);
			if(strlen($search_str)<1) continue;		

			$model->where(function($query) use ($search_str, $fields) {
				foreach ($fields as $s_field) {
					$query->orWhere($s_field, 'like', "%$search_str%");
				}				
			});
		}

		return true;				
	}
}
