<?php namespace App\Helpers;


class ArrHelper {
	public $error_msg = '';

	# ---------------------------------------------------------------------------
	public static function getValByIndex($arr=null, $index=null, $default_value=null){
		if($arr AND $index!=null) {
			if(isset($arr[$index])) {
				return $arr[$index];
			}
		}

		return $default_value;
	}

	# ---------------------------------------------------------------------------
	public static function isArray($str){
		return (gettype($str) === "array");
	}
}
