<?php namespace App\Helpers;

# Helpers
use App\Helpers\ArrHelper as Arr;

class StrHelper {
	public $error_msg = '';

	# ---------------------------------------------------------------------------
	public static function isString($str){
		return (gettype($str) === "string");
	}

	# ---------------------------------------------------------------------------
	public static function generateFullName($arr=null){
		if(!$arr) return '';

		$first_name = Arr::getValByIndex($arr, 'first_name', '');
		$last_name = Arr::getValByIndex($arr, 'last_name', '');

		$full_name = $first_name;
		$full_name .= ($full_name ? ' ' : '' ).$last_name;

		return trim($full_name);
	}

	# ---------------------------------------------------------------------------
	public static function addCharsToLeft($str, $str_len=1, $add_char='0'){
		if(!$str) return '';
		if(!self::isString($str)) $str = (string)$str;

		return str_pad($str, $str_len, $add_char, STR_PAD_LEFT);
	}

	# ---------------------------------------------------------------------------
	public static function dateFromDMYtoYMD($date_str=null, $delimiter='/', $new_delimiter='-'){
		if(!$date_str OR !self::isString($date_str)) return '';

		$date_pieces = explode($delimiter, $date_str);

		$day = (int)Arr::getValByIndex($date_pieces, '0', 0);
		$month = (int)Arr::getValByIndex($date_pieces, '1', 0);
		$year = (int)Arr::getValByIndex($date_pieces, '2', 0);

		if($day AND $day<10) $day = self::addCharsToLeft($day, 2);
		if($month AND $month<10) $month = self::addCharsToLeft($month, 2);

		if(!$day OR !$month OR !$year OR $year<1000) return '';

		return "$year$new_delimiter$month$new_delimiter$day";
	}

	# ---------------------------------------------------------------------------
	public static function dateFromYMDtoDMY($date_str=null, $delimiter='-', $new_delimiter='/'){
		if(!$date_str OR !self::isString($date_str)) return '';

		$date_pieces = explode($delimiter, $date_str);

		$year = (int)Arr::getValByIndex($date_pieces, '0', 0);
		$month = (int)Arr::getValByIndex($date_pieces, '1', 0);
		$day = (int)Arr::getValByIndex($date_pieces, '2', 0);

		if($day AND $day<10) $day = self::addCharsToLeft($day, 2);
		if($month AND $month<10) $month = self::addCharsToLeft($month, 2);

		if(!$day OR !$month OR !$year OR $year<1000) return '';

		return "$day$new_delimiter$month$new_delimiter$year";
	}
}