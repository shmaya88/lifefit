<?php namespace App\Helpers;



class ValidationHelper {
	private static $error_msg = '';

	function __construct($construct_data=null) {
		self::$error_msg = '';
    }

	# ---------------------------------------------------------------------------
	public static function getError(){
		return self::$error_msg;
	}
	
	# ---------------------------------------------------------------------------
	public static function passwordValidate($password='', $password_repeat=''){

		if(!$password) self::$error_msg = 'לא הוזנה סיסמא. ';
		elseif(!$password_repeat) self::$error_msg = 'יש לחזור על הסיסמא, שנית. ';
		elseif($password != $password_repeat) self::$error_msg = 'הסיסמאות בשני השדות אינן זהות. ';

		if(self::$error_msg) return false;

		$chaining_error = '';

		if(!preg_match('/^[_@!\a-zA-Z0-9-]{8,20}$/', $password)) {
			$chaining_error .= 
				"בין 8 ל-20 תווים.</br>"
				.'רק תווים אלפאנומריים (A-Z,‏ a-z, 0-9) ותווים מיוחדים ( _ - ! ).</br>';
		}		
		if(!preg_match("#[0-9]+#",$password)) $chaining_error .= "לפחות ספרה אחת. ";
		if(!preg_match("#[a-z]+#",$password)) $chaining_error .= "לפחות אות קטנה אחת. ";
		if(!preg_match("#[A-Z]+#",$password)) $chaining_error .= "לפחות אות גדולה אחת. ";

		if($chaining_error) {
			self::$error_msg = "הסיסמא חייבת להכיל:</br>".$chaining_error;
			return false;
		}

		return true;
	}

	# ---------------------------------------------------------------------------
	public static function userNameValidate($username=''){
		# English chars + numbers only
		# Valid username, alphanumeric, special characters[_@!.-] & has minimum 5 character and maximum 20 chars
		if(!preg_match('/^[_@!\.a-zA-Z0-9-]{5,20}$/', $username)) {
			self::$error_msg = 
				'השם משתמש צריך להכיל בין 5 ל-20 תווים.</br>'
				.'השם משתמש יכול להכיל רק תווים אלפאנומריים (A-Z,‏ a-z, 0-9) ותווים מיוחדים ( . _ - @ ! ).';

				return false;
		}
		
		return true;
	}

	# ---------------------------------------------------------------------------
	public static function emailValidate($email=''){
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) return false;

		return true;
	}

}
