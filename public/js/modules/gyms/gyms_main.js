var GymsGridTable = GymsModal = {};

$( document ).ready(function() {
	if(currentModuleName=='gyms') setGymsGrid();
});

// ----------------------------------------------
function setGymsGrid() {
    showPreloader();

    GymsGridTable = new GridTable({
		parentElemId:"gyms_list_grid",
		columns:[
			{name:"gym_name", text:"שם החברה/רשת", sortable:true},
			{name:"id_number", text:'ח.פ./ע.מ.', sortable:true}
		],
		store:{
			url:'gyms/get_gyms_list',
			showPreloaderOnLoad:false,
			sortBy:'gym_name',
			autoLoad:true
		},
		events:{
			onSelectChange:function(){				
				refreshGymsGridTopToolBar(this);
			},
			onStoreLoad:function(){
				this.store.showPreloaderOnLoad = true;
			}
		},
		topToolBar:{
			search:{minChars:2, placeholder:'חיפוש'},
			fields:[
				{category:'button', name:'add_gym', 
					iconCls:'fas fa-plus', text:'חברה/רשת חדשה', btnCls:'success',
					events:{
						click:function(){
                            if(!isAdmin()) {
                                openMsgModal({
                                    msgText:'שגיאת הרשאה. אין באפשרותך לערוך את נתוני החברות/רשתות.', 
                                    msgType:'error'
                                });
                                return false;
                            }

							openGymModal({
								submitAction:"add_gym", 
								mainTitleText:'חברה/רשת חדשה', 
								submitBtnText:'הוסף'
							});
						}
					}					
				},
				{category:'dropDownMenu', name:'actions_menu', text:'פעולות', 
					btnCls:'info', iconCls:'fas fa-hand-point-down', disabled:true, 
					items:[
						{name:'edit_gym', text:'עריכה', iconCls:'fas fa-user-edit mrg-l-8 fo-co-red-warning',
							events:{
								click:function(){
									var selectedRowData = this.getSelectedRowData();
                                    var mainTitleText = 'עדכון חברה/רשת';
                                    var errorMsg = '';
									
									if(!selectedRowData) errorMsg = 'לא נבחרה שורה לעריכה';
									else if(!isAdmin()) errorMsg = 'שגיאת הרשאה. אין באפשרותך לערוך את נתוני החברות/רשתות.';

                                    if(errorMsg) {
                                        openMsgModal({msgText:errorMsg, msgType:'error'});
                                        return false;
                                    }

									if(selectedRowData && isDefined(selectedRowData.name)) {
										mainTitleText += ' - '+selectedRowData.name;
									}

									openGymModal({
										submitAction:"edit_gym", 
										mainTitleText:mainTitleText, 
										submitBtnText:'עדכן',
										fillData:selectedRowData
									});
								}
							}
						},
						{name:'delete_gym', text:'מחיקה', iconCls:'fas fa-user-minus mrg-l-8 fo-co-red-danger', 
							events:{
								click:function(){
									doDeleteRestoreGymAction('delete_gym', this.getSelectedRowData());
								}
							}
						},
						{name:'restore_gym', text:'שחזור', 
							iconCls:'fas fa-user-check mrg-l-8 fo-co-red-warning', hidden:true,
							events:{
								click:function(){
									doDeleteRestoreGymAction('restore_gym', this.getSelectedRowData());
								}
							}
						}
					]
				},
				{category:'selectBox', name:'deleted_active_sb', value:'active', attachToStoreLoad:true,
					store:[
						{text:'פעילים', value:'active'},
						{text:'מחוקים', value:'deleted'}
					],
					events:{
						change:function(el){
							refreshGymsGridTopToolBar(this);
							this.loadStore();
						}
					}
				}
			]
		}
	});
}

// ----------------------------------------------
function openGymModal(properties) {
    if(isUndefined(properties)) return false;

    // ----- Define default params values ----- //
	var modalId = 'dialog_gym_edit_modal';
    $('#'+modalId).remove();

    var submitAction = setDefaultWhenUndefined(properties.submitAction, "", 'checkIfEmpty');
	var mainTitleText = setDefaultWhenUndefined(properties.mainTitleText, "");
	var submitBtnText = setDefaultWhenUndefined(properties.submitBtnText, "");
    var fillData = setDefaultWhenUndefined(properties.fillData, "");

    // ----- Create form farams object ----- // 
	var formRowsObj = [
        {fields:[
			{name:'name', colMd:6, labelText:'שם החברה/רשת:', placeholder:'שם החברה/רשת', allowEmpty:false},
            {name:'id_number', colMd:6, labelText:'ח.פ./ע.מ.:', placeholder:'ח.פ./ע.מ.', 
				maxlength:12, onlyNumbers:true, allowEmpty:false
			}
        ]},
        {fields:[
			{name:'adr_type1', category:'selectBox', hidden:true, allowEmpty:false,
				store:getAddressesTypesListStore(),
				value:getAddressesTypesDefaultValue()
			},
			{name:'adr1', colMd:12, labelText:'כתובת מלאה:', placeholder:'כתובת מלאה', allowEmpty:false}
		]},
        {fields:[
			{name:'tel_type1', category:'selectBox', hidden:true, allowEmpty:false,
				store:getCommunicationsTypesListStore(),
				value:getCommunicationsTypesDefaultValue()
			},
			{name:'tel_num1', type:'tel', colMd:6, labelText:'מספר טלפון:', placeholder:'מספר טלפון', allowEmpty:false},
			{name:'tel_type2', category:'selectBox', hidden:true, allowEmpty:false,
				store:getCommunicationsTypesListStore(),
				value:getCommunicationsTypesDefaultValue()
			},
			{name:'tel_num2', colMd:6, labelText:'מספר טלפון נוסף:', placeholder:'מספר טלפון נוסף'}
		]},
        {type:'hidden', fields:[
			{name:'gym_id', type:'hidden'},
			{name:'submit_action', type:'hidden', value:submitAction}
		]}
    ];

    // ----- Fill form ----- // 
    if(fillData) formRowsObj = fillFormObject(formRowsObj, fillData);
    
    // ----- Create gym add/update modal ----- // 
	GymsModal = new ModalWindow({
        modalId:modalId,
		mainTitleParams:{
			text:mainTitleText
		},
        bodyParams:{formRowsObj:formRowsObj},
        buttonsParams:[
			{text:submitBtnText, name:'submit_btn',
				iconCls:(submitAction=='edit_gym' ? 'fas fa-edit' : 'fas fa-plus'), 
				styleClasses:'add-btn-s',
				events:{
					click:function(){
						var modal = this;
						var formBody = modal.modalBodyElem;

						var params = getFormParamsWithValidate(formBody);
						if(!params) return;

						if(isUndefined(params.submit_action) || !inArray(['add_gym','edit_gym'], params.submit_action)) {
							openMsgModal({msgText:'שגיאת מערכת! לא אותר סוג הפעולה', msgType:'error'});
							return;
						}
						
						showPreloader('#'+modalId, "1");

						if(!checkIfFormChanged(formBody)) {
							openMsgModal({msgText:'לא בוצעו שינויים בנתונים.'});
							hidePreloader();
							return;
						}

						$.ajax({
							type:"POST",
							url:"gyms/"+params.submit_action,
							data: params,
							success:function (record) {
								if(record.success) {
									openMsgModal({msgText:record.msg});
									GymsGridTable.loadStore();
									modal.closeModal();															
								}
								else {
									hidePreloader();
									openMsgModal({msgText:record.msg, msgType:'error'});
								}
							}
						});
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times',
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
    }).showModal();
}

// ----------------------------------------------
function openSelectGymModal(properties) {
	if(isUndefined(properties)) properties = {};

	properties.callbackFunc = setDefaultWhenUndefined(properties.callbackFunc, null);

	var selectedGymData = null;

	var modalId = 'dialog_select_gym_modal';
	$('#'+modalId).remove();

	selectGymModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:'בחירת חברה/רשת'
		},
		bodyParams:{
			html:'<div id="select_gym_modal_grid"></div>'
		},
		events:{
			afterLoad:function(){
				GymSelectGridTable = new GridTable({
					parentElemId:"select_gym_modal_grid",
					columns:[
                        {name:"gym_name", text:"שם החברה/רשת", sortable:true},
                        {name:"id_number", text:'ח.פ./ע.מ.', sortable:true}
                    ],
					events:{
						onSelectChange:function(){				
							selectedGymData = this.getSelectedRowData();

							$('#'+modalId).find('[name="select_btn"]')
								.prop("disabled", (selectedGymData ? false : true ));
						},
					},
					store:{
						url:'gyms/get_gyms_list',
                        sortBy:'gym_name',
						sortDirection:'ASC',
						rowsLimit:5
					},
					topToolBar:{
						search:{minChars:2, placeholder:'חיפוש'}
					}
				});
			}
		},		
		buttonsParams:[
			{text:'בחר', name:'select_btn', iconCls:'fas fa-check-circle', 
				styleClasses:'add-btn-s', disabled:true,
				events:{
					click:function(){
						if(!selectedGymData) {
							openMsgModal({msgText:'לא נבחרה חברה', msgType:'error'});
							return;
						}

						if(properties.callbackFunc) properties.callbackFunc(selectedGymData);

						this.closeModal();
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times', css:{'margin-right':'6px'},
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
	}).showModal();
};

// ----------------------------------------------
function refreshGymsGridTopToolBar(gymsGrid) {
	if(isUndefined(gymsGrid) || !objLength(gymsGrid)) {
		openMsgModal({msgText:'שגיאת מערכת! [refreshGymsGridTopToolBar-gymsGrid]', msgType:'error'});
		return false;
	}

	var showDeleteRestoreBtns = false;
	var showBasicBtns = true;

	var selectedRowElem = gymsGrid.getSelectedRowElem();

	var deletedActiveGymSB = gymsGrid.findTopToolBarFieldByName('deleted_active_sb');
	var actionsMenuBtn = gymsGrid.findTopToolBarFieldByName('actions_menu');
	var addGymBtn = gymsGrid.findTopToolBarFieldByName('add_gym');
	var editGymBtn = gymsGrid.findTopToolBarFieldByName('edit_gym');
	var deleteGymBtn = gymsGrid.findTopToolBarFieldByName('delete_gym');
    var restoreGymBtn = gymsGrid.findTopToolBarFieldByName('restore_gym');
    
    if(actionsMenuBtn) setFormFieldDisabled(actionsMenuBtn, (selectedRowElem ? false : true));

    if(deletedActiveGymSB) {
		if(deletedActiveGymSB.val()=='deleted') {
			showDeleteRestoreBtns = true;
			showBasicBtns = false;
		}
		else {
			showDeleteRestoreBtns = false;
			showBasicBtns = true;
		}
    }
    
    // -- Set basic buttons 'visible' attribute -- //
	if(addGymBtn) setFormFieldVisible(addGymBtn, showBasicBtns);
	if(editGymBtn) setFormFieldVisible(editGymBtn, showBasicBtns);
	if(deleteGymBtn) setFormFieldVisible(deleteGymBtn, showBasicBtns);
	// -- Set on 'deleted' status buttons 'visible' attribute -- //
	if(restoreGymBtn) setFormFieldVisible(restoreGymBtn, showDeleteRestoreBtns);

	return true;
};

// ----------------------------------------------
function doDeleteRestoreGymAction(action, selectedRowData){
	if(isUndefined(action) || !action || !inArray(['delete_gym','restore_gym'], action)) {
		openMsgModal({msgText:"שגיאת מערכת. לא אותרה פעולה.[doDeleteRestoreGymAction-action]", msgType:'error'});
		return;
	}

	var actionText = '';

	if(action=='delete_gym') {
		actionText = 'להסיר';
	}
	else if(action=='restore_gym') {
		actionText = 'לשחזר';
	}

	if(
		isUndefined(selectedRowData) || 
		!selectedRowData || 
		isUndefined(selectedRowData.gym_id) || 
		!parseInt(selectedRowData.gym_id)
	) {
		openMsgModal({msgText:"לא אותר מזהה חברה/רשת. לא ניתן "+actionText+" את החברה/רשת.", msgType:'error'});
		return;
	}
	
	if(!isAdmin()) {
        openMsgModal({msgText:'שגיאת הרשאה. אין באפשרותך לערוך את נתוני החברות/רשתות.', msgType:'error'});
        return false;
    }

	var modalMsgText = 
		'החברה/רשת שנבחרה היא:</br>'
		+selectedRowData.name
		+'</br>האם ברצונך '+actionText+' את החברה/רשת?';

	openYesNoQuestionModal({
		msgText:modalMsgText,
		yesBtnOnClick:function(){
			yesNoModal = this;

			showPreloader('#'+yesNoModal.modalUniqueId, "0.9");

			$.ajax({
				type:"POST",
				url:"gyms/do_delete_restore_gym_action",
				data: {
					action:action,
					gym_id:selectedRowData.gym_id
				},
				success:function (record) {
					yesNoModal.closeModal();

					if(record.success) {
						if(record.msg) openMsgModal({msgText:record.msg});
						GymsGridTable.loadStore();
					}
					else {
						openMsgModal({msgText:record.msg, msgType:'error'});
					}

					hidePreloader();
				}
			});
		}
	});
}

// ----------------------------------------------
function getSelectGymButton(properties) {
    if(isUndefined(properties)) properties = {};

    hidden = setDefaultWhenUndefined(properties.hidden, false);
    allowEmpty = setDefaultWhenUndefined(properties.allowEmpty, false);
    value = setDefaultWhenUndefined(properties.value, '');
    labelText = setDefaultWhenUndefined(properties.labelText, '');
    colMd = setDefaultWhenUndefined(properties.colMd, 0);
    defaulGymBranchName = setDefaultWhenUndefined(properties.defaulGymBranchName, '');

    return {
        category:'input', name:'gym_name', value:value, labelText:labelText, 
        colMd:colMd, hidden:hidden, cls:'clickable-input', allowEmpty:allowEmpty,
        events:{
            click:function(el){
                openSelectGymModal({
                    callbackFunc: $.proxy(function(selectedRowData){
                        if(isUndefined(selectedRowData) || !selectedRowData) {
                            openMsgModal({msgText:"לא אותרו נתוני חברה", msgType:'error'});
                            return false;
                        }

                        var gymIdElem = gymNameElem = null;
						var elementClassType = setDefaultWhenUndefined(this.elementClassType, null);
						var newGymId = setDefaultWhenUndefined(selectedRowData.gym_id, 0);
                        var newGymName = setDefaultWhenUndefined(selectedRowData.gym_name, '');

                        if(elementClassType == 'grid') {
                            gymIdElem = this.findTopToolBarFieldByName('gym_id');
                            gymNameElem = this.findTopToolBarFieldByName('gym_name');
                            gymBranchIdElem = this.findTopToolBarFieldByName('gym_branch_id');
                            gymBranchNameElem = this.findTopToolBarFieldByName('gym_branch_name');
                        }
                        else if(elementClassType == 'form') {
                            gymIdElem = this.getFormFieldElemByName('gym_id');
                            gymNameElem = this.getFormFieldElemByName('gym_name');
                            gymBranchIdElem = this.getFormFieldElemByName('gym_branch_id');
                            gymBranchNameElem = this.getFormFieldElemByName('gym_branch_name');
                        }
                        else {
							openMsgModal({msgText:"סוג האובייקט אינו תקין", msgType:'error'});
							return false;
                        }
                        
                        if(!gymIdElem || !gymNameElem) {
                            openMsgModal({msgText:"לא אותרו שדות לעריכה [gym_id|gym_name]", msgType:'error'});
                            return false;
						}

						currId = gymIdElem.val();
						
						var setValuesObj = [
							{elem:gymIdElem, val:newGymId},
							{elem:gymNameElem, val:newGymName}
						];

						if(currId != newGymId) {
							gymNameElem.attr("changed",true);

							setValuesObj.push({elem:gymBranchIdElem, val:''});
							setValuesObj.push({elem:gymBranchNameElem, val:defaulGymBranchName});
						}

						setElementsValue(setValuesObj);

						if(elementClassType == 'grid') {
							this.loadStore();
						}
						
						return setValuesObj;
                    },this)
                });
            }
        }
    };
}