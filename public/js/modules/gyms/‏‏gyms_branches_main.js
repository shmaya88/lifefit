var GymsBranchesGridTable = GymsBranchesModal = {};

$( document ).ready(function() {
	if(currentModuleName=='gyms_branches') setGymsBranchesGrid();
});

// ----------------------------------------------
function setGymsBranchesGrid() {
    var userData = getUserData();
	var gymsList = getGymsList();
	var gymsBranchesList = getGymsBranchesList();

	if(!userData) {
		openMsgModal({msgText:"שגיאה! [setUsersGrid-userData]", msgType:'error'});
		return;
    }
    
    showPreloader();

    GymsBranchesGridTable = new GridTable({
		parentElemId:"gyms_branches_list_grid",
		columns:[
			{name:"gym_branch_name", text:"שם הסניף", sortable:true},
			{name:"adr1", text:'כתובת'},
			{name:"gym_name", text:'חברה/רשת', hidden:(!isAdmin()), sortable:true}
		],
		store:{
			url:'gyms_branches/get_gyms_branches_list',
			showPreloaderOnLoad:false,
			sortBy:'gym_branch_name',
			autoLoad:true
		},
		events:{
			onSelectChange:function(){				
				refreshGymsBranchesGridTopToolBar(this);
			},
			onStoreLoad:function(){
				this.store.showPreloaderOnLoad = true;
			}
		},
		topToolBar:{
			search:{minChars:2, placeholder:'חיפוש'},
			fields:[
				{category:'button', name:'add_gym_branch', 
					iconCls:'fas fa-plus', text:'סניף חדש', btnCls:'success',
					events:{
						click:function(){
							openGymBranchModal({
								submitAction:"add_gym_branch", 
								mainTitleText:'סניף חדש', 
								submitBtnText:'הוסף'
							});
						}
					}					
				},
				{category:'dropDownMenu', name:'actions_menu', text:'פעולות', 
					btnCls:'info', iconCls:'fas fa-hand-point-down', disabled:true, 
					items:[
						{name:'edit_gym_branch', text:'עריכה', iconCls:'fas fa-user-edit mrg-l-8 fo-co-red-warning',
							events:{
								click:function(){
									var selectedRowData = this.getSelectedRowData();
                                    var mainTitleText = 'עדכון סניף';
                                    var errorMsg = '';
									
									if(!selectedRowData) errorMsg = 'לא נבחרה שורה לעריכה';
									else if(!isAdmin() && !isManagerUser()) {
                                        errorMsg = 'שגיאת הרשאה. אין באפשרותך לערוך את נתוני הסניף.';
                                    }

                                    if(errorMsg) {
                                        openMsgModal({msgText:errorMsg, msgType:'error'});
                                        return false;
                                    }

									if(selectedRowData && isDefined(selectedRowData.name)) {
										mainTitleText += ' - '+selectedRowData.name;
									}

									openGymBranchModal({
										submitAction:"edit_gym_branch", 
										mainTitleText:mainTitleText, 
										submitBtnText:'עדכן',
										fillData:selectedRowData
									});
								}
							}
						},
						{name:'delete_gym_branch', text:'מחיקה', iconCls:'fas fa-user-minus mrg-l-8 fo-co-red-danger', 
							events:{
								click:function(){
									doDeleteRestoreGymBranchAction('delete_gym_branch', this.getSelectedRowData());
								}
							}
						},
						{name:'restore_gym_branch', text:'שחזור', 
							iconCls:'fas fa-user-check mrg-l-8 fo-co-red-warning', hidden:true,
							events:{
								click:function(){
									doDeleteRestoreGymBranchAction('restore_gym_branch', this.getSelectedRowData());
								}
							}
						}
					]
				},
				{category:'selectBox', name:'deleted_active_sb', value:'active', attachToStoreLoad:true,
					store:[
						{text:'פעילים', value:'active'},
						{text:'מחוקים', value:'deleted'}
					],
					events:{
						change:function(el){
							refreshGymsBranchesGridTopToolBar(this);
							this.loadStore();
						}
					}
                },
                getSelectGymButton({hidden:!isAdmin(), value:'כל החברות'}),
				{category:'input', name:'gym_id', value:getGymsDefaultValue(), attachToStoreLoad:true, hidden:true},
				{category:'button', name:'clear_filters', iconCls:'fas fa-filter', 
					text:'איפוס סינונים', btnCls:'warning', hidden:!isAdmin(),
					events:{
						click:function(){
							gymIdElem = this.findTopToolBarFieldByName('gym_id');
                            gymNameElem = this.findTopToolBarFieldByName('gym_name');
							
							if(gymIdElem) gymIdElem.val(0);
							if(gymNameElem) gymNameElem.val('כל החברות');

							this.loadStore();
						}
					}					
				}
			]
		}
	});
}

// ----------------------------------------------
function openGymBranchModal(properties) {
    if(isUndefined(properties)) return false;

    // ----- Define default params values ----- //
	var modalId = 'dialog_gym_branch_edit_modal';
    $('#'+modalId).remove();

    var userData = getUserData();
	if(!userData) {
		openMsgModal({msgText:"שגיאה! [openGymBranchModal-userData]", msgType:'error'});
		return;
	}
    
    var branchNameCss = null;

    var submitAction = setDefaultWhenUndefined(properties.submitAction, "", 'checkIfEmpty');
	var mainTitleText = setDefaultWhenUndefined(properties.mainTitleText, "");
	var submitBtnText = setDefaultWhenUndefined(properties.submitBtnText, "");
    var fillData = setDefaultWhenUndefined(properties.fillData, "");

    if(isAdmin()) {
		branchNameCss = {
			'cursor':'pointer',
			'background-color':'#1490ac',
			'color':'#fff'
		};
	}

    // ----- Create form farams object ----- // 
	var formRowsObj = [
        {fields:[
            getSelectGymButton({colMd:12, hidden:!isAdmin(), labelText:'חברה/רשת:', allowEmpty:!isAdmin()})
		]},
        {fields:[
			{name:'gym_branch_name', colMd:12, labelText:'שם הסניף:', placeholder:'שם הסניף', allowEmpty:false}
        ]},
        {fields:[
			{name:'adr_type1', category:'selectBox', hidden:true, allowEmpty:false,
				store:getAddressesTypesListStore(),
				value:getAddressesTypesDefaultValue()
			},
			{name:'adr1', colMd:12, labelText:'כתובת מלאה:', placeholder:'כתובת מלאה', allowEmpty:false}
		]},
        {fields:[
			{name:'tel_type1', category:'selectBox', hidden:true, allowEmpty:false,
				store:getCommunicationsTypesListStore(),
				value:getCommunicationsTypesDefaultValue()
			},
			{name:'tel_num1', type:'tel', colMd:6, labelText:'מספר טלפון:', placeholder:'מספר טלפון', allowEmpty:false},
			{name:'tel_type2', category:'selectBox', hidden:true, allowEmpty:false,
				store:getCommunicationsTypesListStore(),
				value:getCommunicationsTypesDefaultValue()
			},
			{name:'tel_num2', colMd:6, labelText:'מספר טלפון נוסף:', placeholder:'מספר טלפון נוסף'}
		]},
        {type:'hidden', fields:[
            {name:'gym_id', type:'hidden', value:getGymsDefaultValue()},
			{name:'gym_branch_id', type:'hidden'},
			{name:'submit_action', type:'hidden', value:submitAction}
		]}
    ];

    // ----- Fill form ----- // 
    if(fillData) formRowsObj = fillFormObject(formRowsObj, fillData);

    // ----- Create gym branch add/update modal ----- // 
	GymsBranchesModal = new ModalWindow({
        modalId:modalId,
		mainTitleParams:{
			text:mainTitleText
		},
        bodyParams:{formRowsObj:formRowsObj},
        buttonsParams:[
			{text:submitBtnText, name:'submit_btn',
				iconCls:(submitAction=='edit_gym_branch' ? 'fas fa-edit' : 'fas fa-plus'), 
				styleClasses:'add-btn-s',
				events:{
					click:function(){
						var modal = this;
						var formBody = modal.modalBodyElem;

						var params = getFormParamsWithValidate(formBody);
						if(!params) return;

						if(
                            isUndefined(params.submit_action) || 
                            !inArray(['add_gym_branch','edit_gym_branch'], params.submit_action)
                        ) {
							openMsgModal({msgText:'שגיאת מערכת! לא אותר סוג הפעולה', msgType:'error'});
							return;
						}
						
						showPreloader('#'+modalId, "1");

						if(!checkIfFormChanged(formBody)) {
							openMsgModal({msgText:'לא בוצעו שינויים בנתונים.'});
							hidePreloader();
							return;
						}

						$.ajax({
							type:"POST",
							url:"gyms_branches/"+params.submit_action,
							data: params,
							success:function (record) {
								if(record.success) {
									openMsgModal({msgText:record.msg});
									GymsBranchesGridTable.loadStore();
									modal.closeModal();															
								}
								else {
									hidePreloader();
									openMsgModal({msgText:record.msg, msgType:'error'});
								}
							}
						});
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times',
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
    }).showModal();
}

// ----------------------------------------------
function openSelectGymBranchModal(properties) {
	if(isUndefined(properties)) properties = {};

	properties.callbackFunc = setDefaultWhenUndefined(properties.callbackFunc, null);
	properties.gymData = setDefaultWhenUndefined(properties.gymData, null);

    if(properties.gymData && isDefined(properties.gymData.gym_id)) gymIdVal = properties.gymData.gym_id;
    else gymIdVal = getGymsDefaultValue();

	var selectedGymBranchData = null;

	var modalId = 'dialog_select_gym_branch_modal';
	$('#'+modalId).remove();

	selectGymModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:'בחירת סניף'
		},
		bodyParams:{
			html:'<div id="select_gym_branch_modal_grid"></div>'
		},
		events:{
			afterLoad:function(){
				GymSelectGridTable = new GridTable({
					parentElemId:"select_gym_branch_modal_grid",
					columns:[
                        {name:"gym_branch_name", text:"שם הסניף", sortable:true},
                        {name:"adr1", text:'כתובת'},
                        {name:"gym_name", text:'חברה/רשת', hidden:(!isAdmin()), sortable:true}
                    ],
					events:{
						onSelectChange:function(){				
							selectedGymBranchData = this.getSelectedRowData();

							$('#'+modalId).find('[name="select_btn"]')
								.prop("disabled", (selectedGymBranchData ? false : true ));
						},
					},
					store:{
						url:'gyms_branches/get_gyms_branches_list',
                        sortBy:'gym_branch_name',
						sortDirection:'ASC',
						rowsLimit:5
					},
					topToolBar:{
                        search:{minChars:2, placeholder:'חיפוש'},
                        fields:[
                            {category:'input', name:'gym_id', value:gymIdVal, attachToStoreLoad:true, hidden:true}
                        ]
					}
				});
			}
		},		
		buttonsParams:[
			{text:'בחר', name:'select_btn', iconCls:'fas fa-check-circle', 
				styleClasses:'add-btn-s', disabled:true,
				events:{
					click:function(){
						if(!selectedGymBranchData) {
							openMsgModal({msgText:'לא נבחר סניף', msgType:'error'});
							return;
						}

						if(properties.callbackFunc) properties.callbackFunc(selectedGymBranchData);
						this.closeModal();
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times', css:{'margin-right':'6px'},
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
	}).showModal();
};

// ----------------------------------------------
function refreshGymsBranchesGridTopToolBar(gymsBranchesGrid) {
    if(!objLength(gymsBranchesGrid)) {
		openMsgModal({msgText:'שגיאת מערכת! [refreshGymsBranchesGridTopToolBar-gymsGrid]', msgType:'error'});
		return false;
    }

    var showDeleteRestoreBtns = false;
    var showBasicBtns = true;

    var selectedRowElem = gymsBranchesGrid.getSelectedRowElem();
    
    var deletedActiveGymBranchSB = gymsBranchesGrid.findTopToolBarFieldByName('deleted_active_sb');
	var actionsMenuBtn = gymsBranchesGrid.findTopToolBarFieldByName('actions_menu');
	var addGymBranchBtn = gymsBranchesGrid.findTopToolBarFieldByName('add_gym_branch');
	var editGymBranchBtn = gymsBranchesGrid.findTopToolBarFieldByName('edit_gym_branch');
	var deleteGymBranchBtn = gymsBranchesGrid.findTopToolBarFieldByName('delete_gym_branch');
    var restoreGymBranchBtn = gymsBranchesGrid.findTopToolBarFieldByName('restore_gym_branch');

    if(actionsMenuBtn) setFormFieldDisabled(actionsMenuBtn, (selectedRowElem ? false : true));
    
    if(deletedActiveGymBranchSB) {
		if(deletedActiveGymBranchSB.val()=='deleted') {
			showDeleteRestoreBtns = true;
			showBasicBtns = false;
		}
		else {
			showDeleteRestoreBtns = false;
			showBasicBtns = true;
		}
    }

    // -- Set basic buttons 'visible' attribute -- //
	if(addGymBranchBtn) setFormFieldVisible(addGymBranchBtn, showBasicBtns);
	if(editGymBranchBtn) setFormFieldVisible(editGymBranchBtn, showBasicBtns);
	if(deleteGymBranchBtn) setFormFieldVisible(deleteGymBranchBtn, showBasicBtns);
	// -- Set on 'deleted' status buttons 'visible' attribute -- //
	if(restoreGymBranchBtn) setFormFieldVisible(restoreGymBranchBtn, showDeleteRestoreBtns);

	return true;
}

// ----------------------------------------------
function doDeleteRestoreGymBranchAction(action, selectedRowData){
	if(isUndefined(action) || !action || !inArray(['delete_gym_branch','restore_gym_branch'], action)) {
		openMsgModal({msgText:"שגיאת מערכת. לא אותרה פעולה.[doDeleteRestoreGymBranchAction-action]", msgType:'error'});
		return;
	}

	var actionText = '';

	if(action=='delete_gym_branch') {
		actionText = 'להסיר';
	}
	else if(action=='restore_gym_branch') {
		actionText = 'לשחזר';
	}

	if(
		isUndefined(selectedRowData) || 
		!selectedRowData || 
		isUndefined(selectedRowData.gym_branch_id) || 
		!parseInt(selectedRowData.gym_branch_id)
	) {
		openMsgModal({msgText:"לא אותר מזהה הסניף. לא ניתן "+actionText+" את הסניף.", msgType:'error'});
		return;
	}
	
	if(!isAdmin() && !isManagerUser()) {
        openMsgModal({msgText:'שגיאת הרשאה. אין באפשרותך לערוך את נתוני הסניף.', msgType:'error'});
        return false;
    }

	var modalMsgText = 
		'הסניף שנבחר הוא:</br>'
		+selectedRowData.name
		+'</br>האם ברצונך '+actionText+' את הסניף?';

	openYesNoQuestionModal({
		msgText:modalMsgText,
		yesBtnOnClick:function(){
			yesNoModal = this;

			showPreloader('#'+yesNoModal.modalUniqueId, "0.9");

			$.ajax({
				type:"POST",
				url:"gyms_branches/do_delete_restore_gym_branch_action",
				data: {
					action:action,
					gym_branch_id:selectedRowData.gym_branch_id
				},
				success:function (record) {
					yesNoModal.closeModal();

					if(record.success) {
						if(record.msg) openMsgModal({msgText:record.msg});
						GymsBranchesGridTable.loadStore();
					}
					else {
						openMsgModal({msgText:record.msg, msgType:'error'});
					}
					
					hidePreloader();
				}
			});
		}
	});
}

// ----------------------------------------------
function getSelectGymBranchButton(properties) {
    if(isUndefined(properties)) properties = {};

    hidden = setDefaultWhenUndefined(properties.hidden, false);
    allowEmpty = setDefaultWhenUndefined(properties.allowEmpty, false);
    value = setDefaultWhenUndefined(properties.value, '');
    labelText = setDefaultWhenUndefined(properties.labelText, '');
    colMd = setDefaultWhenUndefined(properties.colMd, 0);

    return {
        category:'input', name:'gym_branch_name', value:value, labelText:labelText, 
        colMd:colMd, hidden:hidden, cls:'clickable-input', allowEmpty:allowEmpty,
        events:{
            click:function(el){
                var elementClassType = setDefaultWhenUndefined(this.elementClassType, null);

                gymData = null;

                if(elementClassType == 'grid') gymIdElem = this.findTopToolBarFieldByName('gym_id');
                else gymIdElem = this.getFormFieldElemByName('gym_id');

                if(gymIdElem) gymData = {
                    gym_id:gymIdElem.val()
                };

                openSelectGymBranchModal({
                    gymData:gymData,
                    callbackFunc: $.proxy(function(selectedRowData){
                        if(isUndefined(selectedRowData) || !selectedRowData) {
                            openMsgModal({msgText:"לא אותרו נתוני סניף", msgType:'error'});
                            return;
                        }

                        var gymBranchIdElem = gymBranchNameElem = null;
                        var elementClassType = setDefaultWhenUndefined(this.elementClassType, null);
                        var newGymId = setDefaultWhenUndefined(selectedRowData.gym_id, 0);
                        var newGymBranchId = setDefaultWhenUndefined(selectedRowData.gym_branch_id, 0);
                        var newGymName = setDefaultWhenUndefined(selectedRowData.gym_name, '');
                        var newGymBranchName = setDefaultWhenUndefined(selectedRowData.gym_branch_name, '');

                        if(elementClassType == 'grid') {
                            gymIdElem = this.findTopToolBarFieldByName('gym_id');
                            gymNameElem = this.findTopToolBarFieldByName('gym_name');
                            gymBranchIdElem = this.findTopToolBarFieldByName('gym_branch_id');
                            gymBranchNameElem = this.findTopToolBarFieldByName('gym_branch_name');
                        }
                        else if(elementClassType == 'form') {
                            gymIdElem = this.getFormFieldElemByName('gym_id');
                            gymNameElem = this.getFormFieldElemByName('gym_name');
                            gymBranchIdElem = this.getFormFieldElemByName('gym_branch_id');
                            gymBranchNameElem = this.getFormFieldElemByName('gym_branch_name');
                        }
                        else {
                            openMsgModal({msgText:"סוג האובייקט אינו תקין", msgType:'error'});
                            return;   
                        }                        
                        
                        if(!gymBranchIdElem || !gymBranchNameElem) {
                            openMsgModal({msgText:"לא אותרו שדות לעריכה [gym_branch_id|gym_branch_name]", msgType:'error'});
                            return;
                        }
                        
                        currId = gymBranchIdElem.val();

                        var setValuesObj = [
                            {elem:gymBranchIdElem, val:newGymBranchId},
                            {elem:gymIdElem, val:newGymId},
                            {elem:gymNameElem, val:newGymName},
                            {elem:gymBranchNameElem, val:newGymBranchName}
                        ];

                        setElementsValue(setValuesObj);
                        
                        if(elementClassType == 'grid') {
                            this.loadStore();
                        }
                        else if(elementClassType == 'form') {
                            if(currId != newGymBranchId) gymBranchNameElem.attr("changed",true);	
                        }

                        return setValuesObj;								
                    },this)
                });
            }
        }
    };
}