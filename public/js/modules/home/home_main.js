$( document ).ready(function() {
	var userData = getUserData();
	var gymsList = getGymsList();

	var currDate = new Date();
	var currHours = currDate.getHours();

	var welcomeUserStr = '';

	if(currHours >= 23 || currHours <= 4) welcomeUserStr = "לילה טוב";
	else if(currHours >= 5 && currHours <= 10) welcomeUserStr = "בוקר טוב";
	else if(currHours >= 11 && currHours <= 13) welcomeUserStr = "צהריים טובים";
	else if(currHours >= 14 && currHours <= 16) welcomeUserStr = "אחר צהריים טובים";
	else if(currHours >= 17 && currHours <= 22) welcomeUserStr = "ערב טוב";
	
	gymName = 'ברוכים הבאים למערכת LifeFit';
	if(!isAdmin() && gymsList && gymsList.length==1) gymName = gymsList[0].gym_name;

	gymNameHtml = '<div id="home_page_gym_name">'+gymName+'</div>';

	$('#home_page_welcome_block').append(
		userData.full_name+', '+welcomeUserStr+'!'
		+'</br></br>'
		+gymNameHtml
	);

	createHomePageSlider();
});

// ----------------------------------------------
function createHomePageSlider() {
	console.log(SharedMessagesList);

	// if(!SharedMessagesList.length) return;

	var MAX_WIDTH = 1250;
	var MAX_HEIGHT = 250;

	$('#home_page_slide_bar').css({
		"position":"relative",
		"margin":"0 auto",
		// "margin-top":"170px",
		"top":"0px",
		"left":"0px",
		"width":MAX_WIDTH+"px",
		"height":MAX_HEIGHT+"px",
		"overflow":"hidden",
		"visibility":"hidden"
	});

	$('#home_page_slide_bar div[data-u="slides"]').css({
		"cursor":"default",
		"position":"relative",
		"top":"0px",
		"left":"0px",
		"width":MAX_WIDTH+'px',
		'height':MAX_HEIGHT+'px',
		'overflow':'hidden'
	});

	if(isUndefined(SharedMessagesList) || !isArray(SharedMessagesList) || !SharedMessagesList.length) {
		SharedMessagesList = [];
		SharedMessagesList.push({title:'לא מוגדר',description:'לא מוגדר'});
	}

	$.each(SharedMessagesList, function( k, messageData ) {								
		$('#home_page_slide_bar div[data-u="slides"]').append(
			'<div>'
				+'<div class="slide-block">'
					+'<div class="slide-block-title">'
						+messageData.title
					+'</div>'
					+messageData.description
				+'</div>'
			+'</div>'
		);
	});

	var jssor_1_SlideoTransitions = [
		[{b:0,d:600,y:-290,e:{y:27}}],
		[{b:0,d:1000,y:185},{b:1000,d:500,o:-1},{b:1500,d:500,o:1},{b:2000,d:1500,r:360},{b:3500,d:1000,rX:30},{b:4500,d:500,rX:-30},{b:5000,d:1000,rY:30},{b:6000,d:500,rY:-30},{b:6500,d:500,sX:1},{b:7000,d:500,sX:-1},{b:7500,d:500,sY:1},{b:8000,d:500,sY:-1},{b:8500,d:500,kX:30},{b:9000,d:500,kX:-30},{b:9500,d:500,kY:30},{b:10000,d:500,kY:-30},{b:10500,d:500,c:{x:125.00,t:-125.00}},{b:11000,d:500,c:{x:-125.00,t:125.00}}],
		[{b:0,d:600,x:535,e:{x:27}}],
		[{b:-1,d:1,o:-1},{b:0,d:600,o:1,e:{o:5}}],
		[{b:-1,d:1,c:{x:250.0,t:-250.0}},{b:0,d:800,c:{x:-250.0,t:250.0},e:{c:{x:7,t:7}}}],
		[{b:-1,d:1,o:-1},{b:0,d:600,x:-570,o:1,e:{x:6}}],
		[{b:-1,d:1,o:-1,r:-180},{b:0,d:800,o:1,r:180,e:{r:7}}],
		[{b:0,d:1000,y:80,e:{y:24}},{b:1000,d:1100,x:570,y:170,o:-1,r:30,sX:9,sY:9,e:{x:2,y:6,r:1,sX:5,sY:5}}],
		[{b:2000,d:600,rY:30}],
		[{b:0,d:500,x:-105},{b:500,d:500,x:230},{b:1000,d:500,y:-120},{b:1500,d:500,x:-70,y:120},{b:2600,d:500,y:-80},{b:3100,d:900,y:160,e:{y:24}}],
		[{b:0,d:1000,o:-0.4,rX:2,rY:1},{b:1000,d:1000,rY:1},{b:2000,d:1000,rX:-1},{b:3000,d:1000,rY:-1},{b:4000,d:1000,o:0.4,rX:-1,rY:-1}]
	];

	var jssor_1_options = {
		$AutoPlay: 1,
		$Idle: 2000,
		$CaptionSliderOptions: {
			$Class: $JssorCaptionSlideo$,
			$Transitions: jssor_1_SlideoTransitions,
			$Breaks: [
				[{d:2000,b:1000}]
			]
		},
		$ArrowNavigatorOptions: {
			$Class: $JssorArrowNavigator$
		},
		$BulletNavigatorOptions: {
			$Class: $JssorBulletNavigator$
		}
	};

	var jssor_1_slider = new $JssorSlider$("home_page_slide_bar", jssor_1_options);

	/*#region responsive code begin*/

	function ScaleSlider() {
		var containerElement = jssor_1_slider.$Elmt.parentNode;
		var containerWidth = containerElement.clientWidth;

		if (containerWidth) {
			var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
			jssor_1_slider.$ScaleWidth(expectedWidth);
		}
		else {
			window.setTimeout(ScaleSlider, 30);
		}
	}

	ScaleSlider();
	
	$Jssor$.$AddEvent(window, "load", ScaleSlider);
	$Jssor$.$AddEvent(window, "resize", ScaleSlider);
	$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
	/*#endregion responsive code end*/
};