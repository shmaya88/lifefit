var TrainingGridTable = TrainingModal = {};

$( document ).ready(function() {
	if(currentModuleName=='training') setTrainingGrid();
});

// ----------------------------------------------
function doDeleteRestoreTrainingAction(action, selectedRowData){
	if(isUndefined(action) || !action || !inArray(['delete_training','restore_training'], action)) {
		openMsgModal({msgText:"שגיאת מערכת. לא אותרה פעולה.[doDeleteRestoreTrainingAction-action]", msgType:'error'});
		return;
	}

	var actionText = '';

	if(action=='delete_training') {
		actionText = 'להסיר';
	}
	else if(action=='restore_training') {
		actionText = 'לשחזר';
	}

	if(
		isUndefined(selectedRowData) || 
		!selectedRowData || 
		isUndefined(selectedRowData.training_id) || 
		!parseInt(selectedRowData.training_id)
	) {
		openMsgModal({msgText:"לא אותר מזהה תבנית. לא ניתן "+actionText+" את התבנית אימון.", msgType:'error'});
		return;
	}
	
	if(!checkTrainingUpdatePermissions(selectedRowData)) return;

	var modalMsgText = 
		'התבנית שנבחרה היא:</br>'
		+selectedRowData.name
		+'</br>האם ברצונך '+actionText+' את תבנית האימון?';

	openYesNoQuestionModal({
		msgText:modalMsgText,
		yesBtnOnClick:function(){
			yesNoModal = this;

			showPreloader('#'+yesNoModal.modalUniqueId, "0.9");

			$.ajax({
				type:"POST",
				url:"training/do_delete_restore_training_action",
				data: {
					action:action,
					training_id:selectedRowData.training_id
				},
				success:function (record) {
					yesNoModal.closeModal();

					if(record.success) {
						if(record.msg) openMsgModal({msgText:record.msg});
						TrainingGridTable.loadStore();
					}
					else {
						openMsgModal({msgText:record.msg, msgType:'error'});
					}

					hidePreloader();
				}
			});
		}
	});
}

// ----------------------------------------------
function getSelectTrainingButton(properties) {
	if(isUndefined(properties)) properties = {};

    hidden = setDefaultWhenUndefined(properties.hidden, false);
    allowEmpty = setDefaultWhenUndefined(properties.allowEmpty, false);
    value = setDefaultWhenUndefined(properties.value, '');
    labelText = setDefaultWhenUndefined(properties.labelText, '');
	colMd = setDefaultWhenUndefined(properties.colMd, 0);
	
	return {
        category:'input', name:'training_name', value:value, labelText:labelText, 
        colMd:colMd, hidden:hidden, cls:'clickable-input', allowEmpty:allowEmpty,
        events:{
            click:function(el){
                openSelectTrainingModal({
                    callbackFunc: $.proxy(function(selectedRowData){
                        if(isUndefined(selectedRowData) || !selectedRowData) {
                            openMsgModal({msgText:"לא אותרו נתוני תבנית", msgType:'error'});
                            return;
                        }

                        var trainingIdElem = trainingNameElem = null;
                        var elementClassType = setDefaultWhenUndefined(this.elementClassType, null);
                        var trainingId = setDefaultWhenUndefined(selectedRowData.training_id, 0);
                        var trainingName = setDefaultWhenUndefined(selectedRowData.training_name, '');
						var trainingNameRes = setDefaultWhenUndefined(selectedRowData.name, '');
						var trainingDescription = setDefaultWhenUndefined(selectedRowData.description, '');
						
						if(!trainingName) trainingName = trainingNameRes;

                        if(elementClassType == 'grid') {
                            trainingIdElem = this.findTopToolBarFieldByName('training_id');
                            trainingNameElem = this.findTopToolBarFieldByName('training_name');
                            trainingDescriptionElem = this.findTopToolBarFieldByName('description');
                        }
                        else if(elementClassType == 'form') {
                            trainingIdElem = this.getFormFieldElemByName('training_id');
                            trainingNameElem = this.getFormFieldElemByName('training_name');
                            trainingDescriptionElem = this.getFormFieldElemByName('description');
                        }
                        else {
                            openMsgModal({msgText:"סוג האובייקט אינו תקין", msgType:'error'});
                            return;   
                        }
                        
                        if(!trainingIdElem || !trainingNameElem) {
                            openMsgModal({msgText:"לא אותרו שדות לעריכה [training_id|training_name]", msgType:'error'});
                            return;
                        }
                        
                        currId = trainingIdElem.val();

                        var setValuesObj = [
                            {elem:trainingIdElem, val:trainingId},
                            {elem:trainingNameElem, val:trainingName},
                            {elem:trainingDescriptionElem, val:trainingDescription}
                        ];

                        setElementsValue(setValuesObj);
                        
                        if(elementClassType == 'grid') {
                            this.loadStore();
                        }
                        else if(elementClassType == 'form') {
                            if(currId != trainingId) {
								if(trainingNameElem) trainingNameElem.attr("changed",true);
								if(trainingIdElem) trainingIdElem.attr("changed",true);
							}
                        }

                        return setValuesObj;								
					},this),
					continueBtnHidden:true
                });
            }
        }
    };
}

// ----------------------------------------------
function checkTrainingUpdatePermissions(data){
	if(isUndefined(data) || !objLength(data)) {
		openMsgModal({msgText:"שגיאת מערכת. לא אותרו נתונים.[checkTrainingUpdatePermissions-data]", msgType:'error'});
		return false;
	}

	data.user_id = setDefaultWhenUndefined(data.user_id, null);
	data.gym_id = setDefaultWhenUndefined(data.gym_id, null);
	data.gym_branch_id = setDefaultWhenUndefined(data.gym_branch_id, null);

	var errorMsg = '';
	var userData = getUserData();

	if(isBasicUser() && userData.user_id != data.user_id) {
		errorMsg = 'שגיאת הרשאה. באפשרותך לעדכן/לשנות רק את תבניות האימונים, המשוייכות למשתמש שלך.';
	}
	else if (isBranchUser() && userData.gym_branch_id != data.gym_branch_id) {
		errorMsg = 'שגיאת הרשאה. באפשרותך לעדכן/לשנות רק את תבניות האימונים, המשוייכות לסניף שלך.';
	}
	else if (isManagerUser() && userData.gym_id != data.gym_id) {
		errorMsg = 'שגיאת הרשאה. באפשרותך לעדכן/לשנות רק את תבניות האימונים, המשוייכות לחברה שלך.';
	}

	if(errorMsg) {
		openMsgModal({msgText:errorMsg, msgType:'error'});
		return false;
	}

	return true;
}