// ----------------------------------------------
function openTrainingModal(properties) {
    if(isUndefined(properties)) return false;

    // ----- Define default params values ----- //
	var modalId = 'dialog_training_edit_modal';
    $('#'+modalId).remove();
    
    var submitAction = setDefaultWhenUndefined(properties.submitAction, "add_training", 'checkIfEmpty');
	var mainTitleText = setDefaultWhenUndefined(properties.mainTitleText, "");
	var submitBtnText = setDefaultWhenUndefined(properties.submitBtnText, "");
    var fillData = setDefaultWhenUndefined(properties.fillData, "");
    
    // ----- Create form farams object ----- // 
	var formRowsObj = [
        {fields:[
			{name:'name', colMd:12, labelText:'שם התבנית:', placeholder:'שם התבנית', allowEmpty:false}
        ]},
        {fields:[
            {name:'description', category:'textEditor', colMd:12, labelText:'תוכן התבנית:'}
        ]},
        {type:'hidden', fields:[
			{name:'training_id', colMd:1, type:'hidden'},
			{name:'submit_action', colMd:1, type:'hidden', value:submitAction}
		]}
    ];

    // ----- Fill form ----- // 
    if(fillData) formRowsObj = fillFormObject(formRowsObj, fillData);
    
    // ----- Create user add/update modal ----- // 
	TrainingModal = new ModalWindow({
        modalId:modalId,
		mainTitleParams:{
			text:mainTitleText
		},
        bodyParams:{formRowsObj:formRowsObj},
		buttonsParams:[
			{text:submitBtnText, name:'submit_btn',
				iconCls:(submitAction=='edit_training' ? 'fas fa-edit' : 'fas fa-plus'), 
				styleClasses:'add-btn-s',
				events:{
					click:function(){
						var modal = this;
						var formBody = modal.modalBodyElem;

						var params = getFormParamsWithValidate(formBody);
						if(!params) return;

						if(
							isUndefined(params.submit_action) || 
							!inArray(['add_training','edit_training'], params.submit_action)
						) {
							openMsgModal({msgText:'שגיאת מערכת! לא אותר סוג הפעולה', msgType:'error'});
							return;
						}
						
						showPreloader('#'+modalId, "1");

						if(!checkIfFormChanged(formBody)) {
							openMsgModal({msgText:'לא בוצעו שינויים בנתונים.'});
							hidePreloader();
							return;
						}

						$.ajax({
							type:"POST",
							url:"training/"+params.submit_action,
							data: params,
							success:function (record) {
								if(record.success) {
									openMsgModal({msgText:record.msg});
									TrainingGridTable.loadStore();
									modal.closeModal();															
								}
								else {
									hidePreloader();
									openMsgModal({msgText:record.msg, msgType:'error'});
								}
							}
						});
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times',
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
    }).showModal();

};

// ----------------------------------------------
function openSelectTrainingModal(properties) {
	if(isUndefined(properties)) properties = {};

	properties.callbackFunc = setDefaultWhenUndefined(properties.callbackFunc, null);
	properties.continueBtnHidden = setDefaultWhenUndefined(properties.continueBtnHidden, false);

	var selectedTrainingData = null;

	var modalId = 'dialog_select_training_modal';
	$('#'+modalId).remove();

	selectTrainingModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:'בחירת תבנית אימון'
		},
		bodyParams:{
			html:'<div id="select_training_modal_grid"></div>'
		},
		events:{
			afterLoad:function(){
				TrainingSelectGridTable = new GridTable({
					parentElemId:"select_training_modal_grid",
					columns:[
						{name:"training_name", text:"שם התבנית", sortable:true},
						{name:"view_permissions_type", text:"סוג ההרשאה"},
						{name:"view_permissions", text:"הרשאות תצוגה"}
					],
					events:{
						onSelectChange:function(){				
							selectedTrainingData = this.getSelectedRowData();

							$('#'+modalId).find('[name="select_btn"]')
								.prop("disabled", (selectedTrainingData ? false : true ));
						},
					},
					store:{
						url:'training/get_training_list',
						sortBy:'training_name',
						sortDirection:'ASC',
						rowsLimit:5
					},
					topToolBar:{
						search:{minChars:2, placeholder:'חיפוש'}
					}
				});
			}
		},		
		buttonsParams:[
			{text:'בחר', name:'select_btn', iconCls:'fas fa-check-circle', 
				styleClasses:'add-btn-s', disabled:true,
				events:{
					click:function(){
						if(!selectedTrainingData) {
							openMsgModal({msgText:'לא נבחרה תבנית', msgType:'error'});
							return;
						}

						if(properties.callbackFunc) properties.callbackFunc(selectedTrainingData);
						this.closeModal();
					}
				}
			},
			{text:"המשך ללא תבנית", name:'continue', css:{'margin-right':'6px'}, 
				styleClasses:'add-btn-s', hidden:properties.continueBtnHidden,
				events:{
					click:function(){
						if(properties.callbackFunc) properties.callbackFunc({});
						this.closeModal();
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times', css:{'margin-right':'6px'},
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
	}).showModal();
};