// ----------------------------------------------
function setTrainingGrid() {
	showPreloader();

    TrainingGridTable = new GridTable({
		parentElemId:"training_list_grid",
		columns:[
			{name:"training_name", text:"שם התבנית", sortable:true},
			{name:"view_permissions_type", text:"סוג ההרשאה"},
			{name:"view_permissions", text:"הרשאות תצוגה"}
		],
		store:{
			url:'training/get_training_list',
			showPreloaderOnLoad:false,
			sortBy:'training_name',
			autoLoad:true
		},
		events:{
			onSelectChange:function(){				
				refreshTrainingGridTopToolBar(this);
			},
			onStoreLoad:function(){
				this.store.showPreloaderOnLoad = true;
			}
		},
		topToolBar:{
			search:{minChars:2, placeholder:'חיפוש'},
			fields:[
				{category:'button', name:'add_training', 
					iconCls:'fas fa-plus', text:'אימון חדש', btnCls:'success',
					events:{
						click:function(){
							openTrainingModal({
								submitAction:"add_training", 
								mainTitleText:'אימון חדש', 
								submitBtnText:'הוסף'
							});
						}
					}					
				},
				{category:'dropDownMenu', name:'actions_menu', text:'פעולות', 
					btnCls:'info', iconCls:'fas fa-hand-point-down', disabled:true, 
					items:[
						{name:'edit_training', text:'עריכה', iconCls:'fas fa-user-edit mrg-l-8 fo-co-red-warning',
							events:{
								click:function(){
									var selectedRowData = this.getSelectedRowData();
									var mainTitleText = 'עדכון תבנית אימון';
									
									if(!selectedRowData) {
										openMsgModal({msgText:"לא נבחרה שורה לעריכה", msgType:'error'});
										return;
									}
									else if(!checkTrainingUpdatePermissions(selectedRowData)) return;

									if(selectedRowData && isDefined(selectedRowData.name)) {
										mainTitleText += ' - '+selectedRowData.name;
									}

									openTrainingModal({
										submitAction:"edit_training", 
										mainTitleText:mainTitleText, 
										submitBtnText:'עדכן',
										fillData:selectedRowData
									});
								}
							}
						},
						{name:'delete_training', text:'מחיקה', iconCls:'fas fa-user-minus mrg-l-8 fo-co-red-danger', 
							events:{
								click:function(){
									doDeleteRestoreTrainingAction('delete_training', this.getSelectedRowData());
								}
							}
						},
						{name:'restore_training', text:'שחזור', 
							iconCls:'fas fa-user-check mrg-l-8 fo-co-red-warning', hidden:true,
							events:{
								click:function(){
									doDeleteRestoreTrainingAction('restore_training', this.getSelectedRowData());
								}
							}
						}
					]
				},
				{category:'selectBox', name:'deleted_active_sb', value:'active', attachToStoreLoad:true,
					store:[
						{text:'פעילים', value:'active'},
						{text:'מחוקים', value:'deleted'}
					],
					events:{
						change:function(el){
							refreshTrainingGridTopToolBar(this);
							this.loadStore();
						}
					}
				}
			]
		}
	});
};

// ----------------------------------------------
function refreshTrainingGridTopToolBar(trainingGrid) {
	if(isUndefined(trainingGrid) || !objLength(trainingGrid)) {
		openMsgModal({msgText:'שגיאת מערכת! [refreshTrainingGridTopToolBar-trainingGrid]', msgType:'error'});
		return false;
	}

	var showDeleteRestoreBtns = false;
	var showBasicBtns = true;

	var selectedRowElem = trainingGrid.getSelectedRowElem();

	var deletedActiveTrainingSB = trainingGrid.findTopToolBarFieldByName('deleted_active_sb');
	var actionsMenuBtn = trainingGrid.findTopToolBarFieldByName('actions_menu');
	var addTrainingBtn = trainingGrid.findTopToolBarFieldByName('add_training');
	var editTrainingBtn = trainingGrid.findTopToolBarFieldByName('edit_training');
	var deleteTrainingBtn = trainingGrid.findTopToolBarFieldByName('delete_training');
    var restoreTrainingBtn = trainingGrid.findTopToolBarFieldByName('restore_training');
    
    if(actionsMenuBtn) setFormFieldDisabled(actionsMenuBtn, (selectedRowElem ? false : true));

    if(deletedActiveTrainingSB) {
		if(deletedActiveTrainingSB.val()=='deleted') {
			showDeleteRestoreBtns = true;
			showBasicBtns = false;
		}
		else {
			showDeleteRestoreBtns = false;
			showBasicBtns = true;
		}
    }
    
    // -- Set basic buttons 'visible' attribute -- //
	if(addTrainingBtn) setFormFieldVisible(addTrainingBtn, showBasicBtns);
	if(editTrainingBtn) setFormFieldVisible(editTrainingBtn, showBasicBtns);
	if(deleteTrainingBtn) setFormFieldVisible(deleteTrainingBtn, showBasicBtns);
	// -- Set on 'deleted' status buttons 'visible' attribute -- //
	if(restoreTrainingBtn) setFormFieldVisible(restoreTrainingBtn, showDeleteRestoreBtns);

	return true;
};