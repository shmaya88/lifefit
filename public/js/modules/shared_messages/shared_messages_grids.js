// ----------------------------------------------
function setSharedMessagesGrid() {
	showPreloader();

    new GridTable({
		parentElemId:"shared_messages_list_grid",
		columns:[
			{name:"expiry_date", text:"תוקף ההודעה", sortable:true},
			{name:"title", text:"כותרת", sortable:true},
			{name:"user_full_name", text:"נוצר על ידי", sortable:true},
			{name:"gym_name", text:"מוצג בחברה", sortable:true, hidden:!isAdmin()},
			{name:"created_at", text:"תאריך יצירה", sortable:true}
		],
		store:{
			url:'shared_messages/get_shared_messages_list',
			showPreloaderOnLoad:false,
			sortBy:'created_at',
			sortDirection:'DESC',
			autoLoad:true
		},
		events:{
			onSelectChange:function(){			
				var selectedRowElem = this.getSelectedRowElem();				
				var actionsMenuBtn = this.findTopToolBarFieldByName('actions_menu');
				
				if(actionsMenuBtn) setFormFieldDisabled(actionsMenuBtn, (selectedRowElem ? false : true));
			},
			onStoreLoad:function(){
				this.store.showPreloaderOnLoad = true;
			}
		},
		topToolBar:{
			search:{minChars:2, placeholder:'חיפוש'},
			fields:[
				{category:'button', name:'add_shared_message',
					iconCls:'fas fa-plus', text:'הודעה חדשה', btnCls:'success',
					events:{
						click:function(){
							openSharedMessageModal({
								submitAction:"add_shared_message", 
								mainTitleText:'הודעה חדשה', 
								submitBtnText:'הוסף',
								reloadGrid:this
							});
						}
					}					
				},
				{category:'dropDownMenu', name:'actions_menu', text:'פעולות', 
					btnCls:'info', iconCls:'fas fa-hand-point-down', disabled:true, 
					items:[
						{name:'show_shared_message_description', disabled:true,
							iconCls:'fas fa-eye mrg-l-8 fo-co-red-success', text:'תוכן ההודעה', 
							events:{
								click:function(){
									var selectedRowData = this.getSelectedRowData();

									if(!selectedRowData) {
										openMsgModal({msgText:"לא נבחרה שורה", msgType:'error'});
										return;
									}
									
									var showDescriptionModalId = 'show_shared_message_description_modal';
									$('#'+showDescriptionModalId).remove();

									new ModalWindow({
										modalId:showDescriptionModalId,
										mainTitleParams:{
											text:'תוכן ההודעה', 
											css:{"background-color":'#00B8C0'}
										},
										bodyParams:{
											html:
												'<div class="modal-text" '
													+'style="'
														+'min-height:210px;'
														+'max-height:210px;'
														+'overflow-x:auto;'
													+'"'
												+'>'
													+selectedRowData.description
												+'</div>'
										},
										buttonsParams:[
											{text:'סגור', iconCls:'fas fa-times',
												events:{
													click:function(){
														this.closeModal();
													}
												}
											}
										]
									}).showModal();
								}
							}					
						},
						{name:'edit_shared_message', text:'עריכה', 
							iconCls:'fas fa-user-edit mrg-l-8 fo-co-red-warning',
							events:{
								click:function(){
									var selectedRowData = this.getSelectedRowData();
									var mainTitleText = 'עדכון הודעה';
									
									if(!selectedRowData) {
										openMsgModal({msgText:"לא נבחרה שורה לעריכה", msgType:'error'});
										return;
									}
									else if(!checkSharedMessageUpdatePermissions(selectedRowData)) return;

									if(selectedRowData && isDefined(selectedRowData.title)) {
										mainTitleText += ' - '+selectedRowData.title;
									}
									
									openSharedMessageModal({
										submitAction:"edit_shared_message", 
										mainTitleText:mainTitleText, 
										submitBtnText:'עדכן',
										fillData:selectedRowData,
										reloadGrid:this
									});
								}
							}
						},
						{name:'delete_shared_message', text:'מחיקה', iconCls:'fas fa-user-minus mrg-l-8 fo-co-red-danger', 
							events:{
								click:function(){
									doDeleteRestoreSharedMessageAction({
										action:'delete_shared_message',
										selectedRowData:this.getSelectedRowData(),
										reloadGrid:this
									});
								}
							}
						},
						{name:'restore_shared_message', text:'שחזור', 
							iconCls:'fas fa-user-check mrg-l-8 fo-co-red-warning', hidden:true,
							events:{
								click:function(){
									doDeleteRestoreSharedMessageAction({
										action:'restore_shared_message',
										selectedRowData:this.getSelectedRowData(),
										reloadGrid:this
									});
								}
							}
						}
					]
				},
				getSelectGymButton({hidden:!isAdmin(), value:'כל החברות', defaulGymBranchName:'כל הסניפים'}),
				{category:'input', name:'gym_id', value:getGymsDefaultValue(), attachToStoreLoad:true, hidden:true},
				{category:'button', name:'clear_filters', iconCls:'fas fa-filter', 
					text:'איפוס סינונים', btnCls:'warning', hidden:!isAdmin(),
					events:{
						click:function(){
							gymIdElem = this.findTopToolBarFieldByName('gym_id');
                            gymNameElem = this.findTopToolBarFieldByName('gym_name');
							
							if(gymIdElem) gymIdElem.val(0);
							if(gymNameElem) gymNameElem.val('כל החברות');

							this.loadStore();
						}
					}					
				}
			]
		}
	});
};