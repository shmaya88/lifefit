// ----------------------------------------------
function openSharedMessageModal(properties) {
    if(isUndefined(properties)) return false;

    // ----- Define default params values ----- //
	var modalId = 'dialog_shared_messages_edit_modal';
    $('#'+modalId).remove();
    
    var submitAction = setDefaultWhenUndefined(properties.submitAction, "add_shared_message", 'checkIfEmpty');
	var mainTitleText = setDefaultWhenUndefined(properties.mainTitleText, "");
	var submitBtnText = setDefaultWhenUndefined(properties.submitBtnText, "");
	var fillData = setDefaultWhenUndefined(properties.fillData, "");
	var reloadGrid = setDefaultWhenUndefined(properties.reloadGrid, null);
	
	var expiryDateColMd = (isAdmin() ? 6 : 12);
    
    // ----- Create form farams object ----- // 
	var formRowsObj = [
		{fields:[
			getSelectGymButton({
				colMd:6, hidden:!isAdmin(), labelText:'מוצג בחברה:', value:'כל החברות', allowEmpty:true
			}),
			{name:'expiry_date', category:'date', colMd:expiryDateColMd, labelText:'תוקף ההודעה:'}
        ]},
        {fields:[
			{name:'title', colMd:12, labelText:'כותרת:', placeholder:'כותרת', allowEmpty:false}
        ]},
        {fields:[
            {name:'description', category:'textEditor', colMd:12, labelText:'תוכן ההודעה:'}
        ]},
        {type:'hidden', fields:[
			{name:'gym_id', type:'hidden', value:getGymsDefaultValue()},
			{name:'message_id', type:'hidden'},
			{name:'submit_action', type:'hidden', value:submitAction}
		]}
    ];

    // ----- Fill form ----- // 
    if(fillData) formRowsObj = fillFormObject(formRowsObj, fillData);
    
    // ----- Create add/update modal ----- // 
	new ModalWindow({
        modalId:modalId,
		mainTitleParams:{
			text:mainTitleText
		},
        bodyParams:{formRowsObj:formRowsObj},
		buttonsParams:[
			{text:submitBtnText, name:'submit_btn',
				iconCls:(submitAction=='edit_shared_message' ? 'fas fa-edit' : 'fas fa-plus'), 
				styleClasses:'add-btn-s',
				events:{
					click:function(){
						var modal = this;
						var formBody = modal.modalBodyElem;

						var params = getFormParamsWithValidate(formBody);
						if(!params) return;

						if(
							isUndefined(params.submit_action) || 
							!inArray(['add_shared_message','edit_shared_message'], params.submit_action)
						) {
							openMsgModal({msgText:'שגיאת מערכת! לא אותר סוג הפעולה', msgType:'error'});
							return;
						}
						
						showPreloader('#'+modalId, "1");

						if(!checkIfFormChanged(formBody)) {
							openMsgModal({msgText:'לא בוצעו שינויים בנתונים.'});
							hidePreloader();
							return;
						}

						$.ajax({
							type:"POST",
							url:"shared_messages/"+params.submit_action,
							data: params,
							success:function (record) {
								if(record.success) {
									openMsgModal({msgText:record.msg});
									if(reloadGrid) reloadGrid.loadStore();
									modal.closeModal();														
								}
								else {
									hidePreloader();
									openMsgModal({msgText:record.msg, msgType:'error'});
								}
							}
						});
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times',
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
    }).showModal();

};