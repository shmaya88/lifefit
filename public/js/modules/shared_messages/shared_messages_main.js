$( document ).ready(function() {
	if(currentModuleName=='shared_messages') setSharedMessagesGrid();
});

// ----------------------------------------------
function doDeleteRestoreSharedMessageAction(properties){
	if(isUndefined(properties)) return false;

	var selectedRowData = setDefaultWhenUndefined(properties.selectedRowData, null);
	var reloadGrid = setDefaultWhenUndefined(properties.reloadGrid, null);
	var action = setDefaultWhenUndefined(properties.action, "");

	if(isUndefined(action) || !action || !inArray(['delete_shared_message','restore_shared_message'], action)) {
		openMsgModal({
			msgText:"שגיאת מערכת. לא אותרה פעולה.[doDeleteRestoreSharedMessageAction-action]", 
			msgType:'error'
		});
		return;
	}

	var actionText = '';

	if(action=='delete_shared_message') {
		actionText = 'להסיר';
	}
	else if(action=='restore_shared_message') {
		actionText = 'לשחזר';
	}

	if(
		isUndefined(selectedRowData) || 
		!selectedRowData || 
		isUndefined(selectedRowData.message_id) || 
		!parseInt(selectedRowData.message_id)
	) {
		openMsgModal({msgText:"לא אותר מזהה הודעה. לא ניתן "+actionText+" את ההודעה.", msgType:'error'});
		return;
	}
	
	if(!checkSharedMessageUpdatePermissions(selectedRowData)) return;

	var modalMsgText = 
		'ההודעה שנבחרה היא:</br>'
		+selectedRowData.title
		+'</br>האם ברצונך '+actionText+' את תבנית האימון?';

	openYesNoQuestionModal({
		msgText:modalMsgText,
		yesBtnOnClick:function(){
			yesNoModal = this;

			showPreloader('#'+yesNoModal.modalUniqueId, "0.9");

			$.ajax({
				type:"POST",
				url:"shared_messages/do_delete_restore_shared_message_action",
				data: {
					action:action,
					message_id:selectedRowData.message_id
				},
				success:function (record) {
					yesNoModal.closeModal();

					if(record.success) {
						if(record.msg) openMsgModal({msgText:record.msg});
						if(reloadGrid) reloadGrid.loadStore();
					}
					else {
						openMsgModal({msgText:record.msg, msgType:'error'});
					}

					hidePreloader();
				}
			});
		}
	});
}

// ----------------------------------------------
function checkSharedMessageUpdatePermissions(data){
	if(isUndefined(data) || !objLength(data)) {
		openMsgModal({
			msgText:"שגיאת מערכת. לא אותרו נתונים.[checkSharedMessageUpdatePermissions-data]", 
			msgType:'error'
		});
		return false;
	}

	data.user_id = setDefaultWhenUndefined(data.user_id, null);
	data.gym_id = setDefaultWhenUndefined(data.gym_id, null);
	data.gym_branch_id = setDefaultWhenUndefined(data.gym_branch_id, null);

	var errorMsg = '';
	var userData = getUserData();

	if (isBranchUser() && userData.gym_branch_id != data.gym_branch_id) {
		errorMsg = 'שגיאת הרשאה. באפשרותך לעדכן/לשנות רק את ההודעות, המשוייכות לסניף שלך.';
	}
	else if (isManagerUser() && userData.gym_id != data.gym_id) {
		errorMsg = 'שגיאת הרשאה. באפשרותך לעדכן/לשנות רק את ההודעות, המשוייכות לחברה שלך.';
	}

	if(errorMsg) {
		openMsgModal({msgText:errorMsg, msgType:'error'});
		return false;
	}

	return true;
}