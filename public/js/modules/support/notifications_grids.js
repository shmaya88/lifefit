// ----------------------------------------------
function setNotificationsServicesGrid() {
	showPreloader();

	new GridTable({
		parentElemId:"notifications_services_list_grid",
		columns:[
			{name:"status_text", text:"סטטוס", sortable:true},
			{name:"created_at", text:"תאריך", sortable:true},
			{name:"title", text:"כותרת", sortable:true},
			{name:"user_full_name", text:"נוצר על ידי", sortable:true},
			{name:"gym_name", text:"חברה", sortable:true},
			{name:"gym_branch_name", text:"סניף", sortable:true}
		],
		store:{
			url:'/support/get_notifications_list',
			showPreloaderOnLoad:false,
			sortBy:'created_at',
			sortDirection:'DESC',
			autoLoad:true
		},
		events:{
			onSelectChange:function(){				
				var selectedRowElem = this.getSelectedRowElem();
				var showBtn = this.findTopToolBarFieldByName('show_notification_description');
				var setStatusBtn = this.findTopToolBarFieldByName('set_notification_status');

				if(showBtn) setFormFieldDisabled(showBtn, (selectedRowElem ? false : true));
				if(setStatusBtn) {
					setFormFieldDisabled(setStatusBtn, (selectedRowElem ? false : true));

					if(selectedRowElem && (selectedRowData = this.getSelectedRowData())) {
						if(isDefined(selectedRowData.status)){							
							var newStatusText = '<i class="fas fa-exchange-alt btn-fa-icon-cls"></i>';

							if(selectedRowData.status==1) newStatusText += 'הגדר כטרם טופל';
							else newStatusText += 'הגדר כטופל';
							
							setStatusBtn.html(newStatusText);
						}
					}
				}
			},
			onStoreLoad:function(){
				this.store.showPreloaderOnLoad = true;
			}
		},
		topToolBar:{
			search:{minChars:2, placeholder:'חיפוש'},
			fields:[
				{category:'button', name:'show_notification_description', disabled:true,
					iconCls:'fas fa-eye', text:'הצג תיאור', btnCls:'success',
					events:{
						click:function(){
							var selectedRowData = this.getSelectedRowData();

							if(!selectedRowData) {
								openMsgModal({msgText:"לא נבחרה שורה", msgType:'error'});
								return;
							}
							
							var showNotificationModalId = 'show_notification_description_modal';
							$('#'+showNotificationModalId).remove();

							new ModalWindow({
								modalId:showNotificationModalId,
								mainTitleParams:{
									text:'תיאור פניית שירות', 
									css:{"background-color":'#00B8C0'}
								},
								bodyParams:{
									html:
										'<div class="modal-text" '
											+'style="'
												+'min-height:100px;'
												+'max-height:350px;'
												+'overflow-x:auto;'
												+'margin-bottom:10px;'
											+'"'
										+'>'
											+selectedRowData.description
										+'</div>'
								},
								buttonsParams:[
									{text:'סגור', iconCls:'fas fa-times',
										events:{
											click:function(){
												this.closeModal();
											}
										}
									}
								]
							}).showModal();
						}
					}					
				},
				{category:'button', name:'set_notification_status', text:'הגדר כטופל', 
					iconCls:'fas fa-exchange-alt', btnCls:'warning', disabled:true,
					events:{
						click:function(){
							var selectedRowData = this.getSelectedRowData();
							
							if(!selectedRowData) {
								openMsgModal({msgText:"לא נבחרה שורה לעריכה", msgType:'error'});
								return;
							}

							$.ajax({
								type:"POST",
								url:"/support/set_notification_status",
								data: {
									notification_id:selectedRowData.notification_id,
									new_status_code:(selectedRowData.status==1 ? 0 : 1)
								},
								success:$.proxy(function (record) {
									hidePreloader();

									if(record.success) {
										openMsgModal({msgText:record.msg});
										this.loadStore();													
									}
									else {
										openMsgModal({msgText:record.msg, msgType:'error'});
									}
								},this)
							});
						}
					}
				},
				{category:'selectBox', name:'status', value:0, attachToStoreLoad:true,
					store:[
						{text:'כל הסטטוסים', value:'all'},						
						{text:'טרם טופלו', value:0},						
						{text:'טופלו', value:1}
					],
					events:{
						change:function(el){
							// refreshSubscriptionGridTopToolBar(this);
							this.loadStore();
						}
					}
				}
			]
		}
	});
};
