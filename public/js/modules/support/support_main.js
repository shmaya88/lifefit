$( document ).ready(function() {
	if(currentModuleName=='support/notifications_services') {
		setNotificationsServicesGrid();
	}

	if(currentModuleName=='support/support_page') {
		createHomePageFooterSupportMsgForm();
	}
});

// ----------------------------------------------
function createHomePageFooterSupportMsgForm() {
	// ----- Create form farams object ----- //		
	createFormElement({
		parentElemId:'support_page_footer_form',
		formRows:[
			{fields:[
				{name:'title', colMd:12, placeholder:'נושא', maxlength:25, cls:'send-support-form-input-field'}
			], cls:'send-support-form-row'},
			{fields:[
				{name:'description', category:'textarea', colMd:12, css:{height:'185px'}}				
			], cls:'send-support-form-row'},
			{fields:[
				{name:'submit_btn', category:'button', text:"שליחה", 
					cls:'send-support-form-submit-btn', colMd:2,
					events:{
						click:function(){
							var params = getFormParamsWithValidate(this.parentElem);
							if(!params) return;

							var errorMsg = '';
							var title = setDefaultWhenUndefined(params.title, "");
							var description = setDefaultWhenUndefined(params.description, "");
							
							if(!title) errorMsg = 'נא להזין את נושא הפנייה';
							else if(!description) errorMsg = 'נא להזין את תוכן הפנייה';

							if(errorMsg) {
								openMsgModal({msgText:errorMsg, msgType:'error'});
								return;
							}

							showPreloader();
							
							$.ajax({
								type:"POST",
								url:"/support/add_notification",
								data: {
									title:title,
									description:description
								},
								success:$.proxy(function (record) {
									if(record.success) {
										openMsgModal({msgText:record.msg});
				
										setElementsValue([
											{elem:this.getFormFieldElemByName('title'), val:''},
											{elem:this.getFormFieldElemByName('description'), val:''}
										]);											
									}
									else {
										openMsgModal({msgText:record.msg, msgType:'error'});
									}

									hidePreloader();
								},this)
							});
						}
					}
				}
			], cls:'send-support-form-row'},
		]
	});
}