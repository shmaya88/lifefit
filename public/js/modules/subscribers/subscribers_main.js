$( document ).ready(function() {
	if(currentModuleName=='subscribers') setSubscribersMainGrid();    
});

// ----------------------------------------------
function doDeleteRestoreSubscriptionAction(properties) {
	if(isUndefined(properties)) return false;

	var selectedRowData = setDefaultWhenUndefined(properties.selectedRowData, null);
	var reloadGrid = setDefaultWhenUndefined(properties.reloadGrid, null);
	var action = setDefaultWhenUndefined(properties.action, "");

	if(isUndefined(action) || !action || !inArray(['delete_subscription','restore_subscription'], action)) {
		openMsgModal({
			msgText:"שגיאת מערכת. לא אותרה פעולה.[doDeleteRestoreSubscriptionAction-action]", 
			msgType:'error'
		});
		return;
	}
	
	var actionText = warningMsg = '';

	if(action=='delete_subscription') {
		actionText = 'להסיר';
	}
	else if(action=='restore_subscription') {
		actionText = 'לשחזר';
		warningMsg = '</br><span style="font-size:12px;">'
				+'לתשומת ליבך, לאחר השחזור, את/ה תוגדר/י כמטפל במנוי.'
			+'</span>';
	}

	if(
		isUndefined(selectedRowData) || 
		!selectedRowData || 
		isUndefined(selectedRowData.subscription_id) || 
		!parseInt(selectedRowData.subscription_id)
	) {
		openMsgModal({msgText:"לא אותר מזהה מנוי. לא ניתן "+actionText+" את המנוי.", msgType:'error'});
		return;
	}

	var modalMsgText = 
		'המנוי שנבחר הוא:</br>'
		+selectedRowData.subscription_full_name
		+(selectedRowData.id_number ? '('+selectedRowData.id_number+')' : '')
		+'</br>האם ברצונך '+actionText+' את המנוי?</br>'
		+warningMsg;

	openYesNoQuestionModal({
		msgText:modalMsgText,
		yesBtnOnClick:function(){
			yesNoModal = this;

			showPreloader('#'+yesNoModal.modalUniqueId, "0.9");

			$.ajax({
				type:"POST",
				url:"subscribers/do_delete_restore_subscription_action",
				data: {
					action:action,
					subscription_id:selectedRowData.subscription_id
				},
				success:function (record) {
					yesNoModal.closeModal();
					hidePreloader();
					
					if(record.success) {
						if(record.msg) openMsgModal({msgText:record.msg});
						if(reloadGrid) reloadGrid.loadStore();
					}
					else {
						openMsgModal({msgText:record.msg, msgType:'error'});
					}
				}
			});
		}
	});
};