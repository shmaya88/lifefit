// ----------------------------------------------
function openSubscriptionModal(properties) {
	if(isUndefined(properties)) return false;

	// ----- Define default params values ----- //
	var modalId = 'dialog_subscription_edit_modal';
	$('#'+modalId).remove();

	var userData = getUserData();
	if(!userData) {
		openMsgModal({msgText:"שגיאה! [openSubscriptionModal-userData]", msgType:'error'});
		return;
	}

	var submitAction = setDefaultWhenUndefined(properties.submitAction, "add_subscription", 'checkIfEmpty');
	var mainTitleText = setDefaultWhenUndefined(properties.mainTitleText, "");
	var submitBtnText = setDefaultWhenUndefined(properties.submitBtnText, "");
	var fillData = setDefaultWhenUndefined(properties.fillData, "");
	var reloadGrid = setDefaultWhenUndefined(properties.reloadGrid, null);
	
	var expiryDateColMd = (isBasicUser() ? 12 : 6);

	var nextYearDate = new Date();
	nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
	
	expiryDate = nextYearDate.getDate()+'/'+(nextYearDate.getMonth()+1)+'/'+nextYearDate.getFullYear();

	// ----- Create form farams object ----- // 
	var formRowsObj = [
		{fields:[
			getSelectUserButton({colMd:6, labelText:'בטיפול של:', value:userData.full_name, hidden:isBasicUser()}),
			{name:'expiry_date', category:'date', colMd:expiryDateColMd, labelText:'תוקף המנוי:', 
				value:expiryDate, allowEmpty:false
			}
		]},
		{fields:[
			{name:'first_name', colMd:3, labelText:'שם פרטי:', placeholder:'שם פרטי', allowEmpty:false},
			{name:'last_name', colMd:3, labelText:'שם משפחה:', placeholder:'שם משפחה', allowEmpty:false},
			{name:'id_number', colMd:3, labelText:'תעודת זהות:', placeholder:'תעודת זהות', 
				maxlength:9, onlyNumbers:true, allowEmpty:false, isIdNumber:true
			},
			{name:'birthdate', colMd:3, category:'date', labelText:'תאריך לידה:'}
		]},
		{fields:[
			{name:'tel_type1', category:'selectBox', colMd:2, labelText:'סוג התקשרות:', 
				store:getCommunicationsTypesListStore(),
				value:getCommunicationsTypesDefaultValue(),
				allowEmpty:false
			},
			{name:'tel_num1', type:'tel', colMd:4, labelText:'מספר טלפון:', placeholder:'מספר טלפון', allowEmpty:false},
			{name:'tel_type2', category:'selectBox', colMd:2, labelText:'סוג התקשרות:', 
				store:getCommunicationsTypesListStore(),
				value:getCommunicationsTypesDefaultValue()
			},
			{name:'tel_num2', colMd:4, labelText:'מספר טלפון נוסף:', placeholder:'מספר טלפון נוסף'},
		]},
		{fields:[
			{name:'adr_type1', category:'selectBox', colMd:2, labelText:'סוג הכתובת:', 
				store:getAddressesTypesListStore(),
				value:getAddressesTypesDefaultValue(),
				allowEmpty:false
			},
			{name:'adr1', colMd:4, labelText:'כתובת:', placeholder:'כתובת', allowEmpty:false},
			{name:'adr_type2', category:'selectBox', colMd:2, labelText:'סוג הכתובת:', 
				store:getAddressesTypesListStore(),
				value:getAddressesTypesDefaultValue()
			},
			{name:'adr2', colMd:4, labelText:'כתובת נוספת:', placeholder:'כתובת נוספת'},
		]},
		{type:'hidden', fields:[
			{name:'user_id', colMd:1, type:'hidden', value:userData.user_id},
			{name:'gym_id', colMd:1, type:'hidden', value:userData.gym_id},
			{name:'gym_branch_id', colMd:1, type:'hidden', value:userData.gym_branch_id},
			{name:'subscription_id', colMd:1, type:'hidden'},
			{name:'submit_action', colMd:1, type:'hidden', value:submitAction}
		]}
	];

	// ----- Fill form ----- // 
	if(fillData) formRowsObj = fillFormObject(formRowsObj, fillData);

	// ----- Create user add/update modal ----- // 
	SubscribersModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:mainTitleText
		},
		bodyParams:{formRowsObj:formRowsObj},		
		buttonsParams:[
			{text:submitBtnText, name:'submit_btn',
				iconCls:(submitAction=='edit_subscription' ? 'fas fa-user-edit' : 'fas fa-user-plus'), 
				styleClasses:'add-btn-s',
				events:{
					click:function(){
						var modal = this;
						var formBody = modal.modalBodyElem;

						var params = getFormParamsWithValidate(formBody);
						if(!params) return;

						if(	isUndefined(params.submit_action) || 
							!inArray(['add_subscription','edit_subscription'], params.submit_action)
						) {
							openMsgModal({msgText:'שגיאת מערכת! לא אותר סוג הפעולה', msgType:'error'});
							return;
						}

						if(!checkIfFormChanged(formBody)) {
							openMsgModal({msgText:'לא בוצעו שינויים בנתונים.'});
							return;
						}
						
						showPreloader('#'+modalId, "1");

						$.ajax({
							type:"POST",
							url:"subscribers/"+params.submit_action,
							data: params,
							success:function (record) {
								if(record.success) {
									openMsgModal({msgText:record.msg});
									if(reloadGrid) reloadGrid.loadStore();
									modal.closeModal();															
								}
								else {
									hidePreloader();
									openMsgModal({msgText:record.msg, msgType:'error'});
								}
							}
						});
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times',
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
	}).showModal();
};

// ----------------------------------------------
function setSubscriptionUserModal(properties) {
	if(isUndefined(properties)) properties = {};

	var selectedSubscription = setDefaultWhenUndefined(properties.selectedSubscription, null);
	var subscriptionUserModalCallback = setDefaultWhenUndefined(properties.callback, null);

	if(!selectedSubscription) {
		openMsgModal({msgText:"לא נבחר מנוי לעריכה", msgType:'error'});
		return;
	}

	openSelectUserModal({
		callbackFunc: $.proxy(function(selectedRow){
			if(isUndefined(selectedRow) || !selectedRow) {
				openMsgModal({msgText:"לא אותרו נתוני משתמש", msgType:'error'});
				return;
			}

			showPreloader();

			$.ajax({
				type:"POST",
				url:"subscribers/set_subscription_user",
				data: {
					subscription_id:setDefaultWhenUndefined(selectedSubscription.subscription_id, 0),
					user_id:setDefaultWhenUndefined(selectedRow.user_id, 0),
					gym_id:setDefaultWhenUndefined(selectedRow.gym_id, 0),
					gym_branch_id:setDefaultWhenUndefined(selectedRow.gym_branch_id, 0)
				},
				success:function (record) {
					hidePreloader();
					
					if(record.success) {
						openMsgModal({msgText:record.msg});
						if(subscriptionUserModalCallback) subscriptionUserModalCallback();
					}
					else {
						openMsgModal({msgText:record.msg, msgType:'error'});
					}
				}
			});
		},this)
	});
};

// ----------------------------------------------
function openSetSubscriptionTrainingModal(properties) {
    if(isUndefined(properties)) return false;

    // ----- Define default params values ----- //
	var modalId = 'dialog_set_subscription_training_modal';
    $('#'+modalId).remove();
    
    var submitAction = setDefaultWhenUndefined(properties.submitAction, "add_sub_training", 'checkIfEmpty');
	var mainTitleText = setDefaultWhenUndefined(properties.mainTitleText, "");
	var submitBtnText = setDefaultWhenUndefined(properties.submitBtnText, "");
	var fillData = setDefaultWhenUndefined(properties.fillData, "");
	var setSubscriptionTrainingModal = setDefaultWhenUndefined(properties.callback, null);
    
    // ----- Create form farams object ----- // 
	var formRowsObj = [
		{fields:[
			{name:'weight', colMd:6, labelText:'משקל(ק"ג):', placeholder:'משקל', maxlength:6},
			{name:'fat_percentage', colMd:6, labelText:'אחוזי שומן(%):', placeholder:'אחוזי שומן', maxlength:5}
        ]},
        {fields:[
			getSelectTrainingButton({colMd:12, labelText:'שם תבנית האימון:', labelText:'שם התבנית:', allowEmpty:true}),
        ]},
        {fields:[
            {name:'description', category:'textEditor', colMd:12, labelText:'תיאור האימון:'}
        ]},
        {type:'hidden', fields:[
			{name:'subscription_id', type:'hidden'},
			{name:'subscription_traning_id', type:'hidden'},
			{name:'training_id', type:'hidden'},
			{name:'submit_action', type:'hidden', value:submitAction}
		]}
    ];

    // ----- Fill form ----- // 
    if(fillData) formRowsObj = fillFormObject(formRowsObj, fillData);
    
    // ----- Create add/update modal ----- // 
	SubTrainingModal = new ModalWindow({
        modalId:modalId,
		mainTitleParams:{
			text:mainTitleText
		},
        bodyParams:{formRowsObj:formRowsObj},
		buttonsParams:[
			{text:submitBtnText, name:'submit_btn',
				iconCls:(submitAction=='edit_sub_training' ? 'fas fa-edit' : 'fas fa-plus'), 
				styleClasses:'add-btn-s',
				events:{
					click:function(){
						var modal = this;
						var formBody = modal.modalBodyElem;

						var params = getFormParamsWithValidate(formBody);
						if(!params) return;

						if(
							isUndefined(params.submit_action) || 
							!inArray(['add_sub_training','edit_sub_training'], params.submit_action)
						) {
							openMsgModal({msgText:'שגיאת מערכת! לא אותר סוג הפעולה', msgType:'error'});
							return;
						}

						if(params.submit_action=='edit_sub_training' && !checkIfFormChanged(formBody)) {
							openMsgModal({msgText:'לא בוצעו שינויים בנתונים.'});
							return;
						}

						showPreloader('#'+modalId, "1");

						$.ajax({
							type:"POST",
							url:"subscribers/"+params.submit_action,
							data: params,
							success:function (record) {
								if(record.success) {
									openMsgModal({msgText:record.msg});
									if(setSubscriptionTrainingModal) setSubscriptionTrainingModal();
									modal.closeModal();													
								}
								else {
									openMsgModal({msgText:record.msg, msgType:'error'});
								}

								hidePreloader();
							}
						});
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times',
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
    }).showModal();

};

// ----------------------------------------------
function openSubscriptionTrainingListModal(properties) {
	if(isUndefined(properties)) properties = {};

	properties.callbackFunc = setDefaultWhenUndefined(properties.callbackFunc, null);
	selectedSubscription = setDefaultWhenUndefined(properties.selectedSubscription, {});
	subscriptionId = setDefaultWhenUndefined(selectedSubscription.subscription_id, 0);

	var modalId = 'dialog_subscription_training_list_modal';
	var gridId = 'select_subscription_training_list_modal_grid';
	$('#'+modalId).remove();

	selectSubscriptionTrainingListModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:'רשימת אימוני/ביקורי מנוי'
		},
		bodyParams:{
			html:'<div id="'+gridId+'"></div>'
		},
		events:{
			afterLoad:function(){
				TrainingSelectGridTable = new GridTable({
					parentElemId:gridId,
					columns:[
						{name:"created_at", text:"תאריך", sortable:true},
						{name:"weight", text:"משקל", sortable:true},
						{name:"fat_percentage", text:"אחוזי שומן", sortable:true},
						{name:"user_full_name", text:"נוצר על ידי", sortable:true}
					],
					store:{
						url:'subscribers/get_subscription_training_list',
						sortBy:'created_at',
						sortDirection:'DESC',
						rowsLimit:5
					},
					events:{
						onSelectChange:function(){				
							var selectedRowElem = this.getSelectedRowElem();
							var showBtn = this.findTopToolBarFieldByName('show_train_description');
							var editBtn = this.findTopToolBarFieldByName('edit_sub_training');
							var deleteBtn = this.findTopToolBarFieldByName('delete_sub_training');

							if(showBtn) setFormFieldDisabled(showBtn, (selectedRowElem ? false : true));
							if(editBtn) setFormFieldDisabled(editBtn, (selectedRowElem ? false : true));
							if(deleteBtn) setFormFieldDisabled(deleteBtn, (selectedRowElem ? false : true));
						}
					},
					topToolBar:{
						fields:[
							{category:'input', name:'subscription_id', hidden:true, value:subscriptionId, attachToStoreLoad:true},
							{category:'button', name:'show_train_description', disabled:true,
								iconCls:'fas fa-eye', text:'הצג תיאור', btnCls:'success',
								events:{
									click:function(){
										var selectedRowData = this.getSelectedRowData();

										if(!selectedRowData) {
											openMsgModal({msgText:"לא נבחרה שורה", msgType:'error'});
											return;
										}
										
										var showSubscriptionModalId = 'show_train_description_modal';
										$('#'+showSubscriptionModalId).remove();

										new ModalWindow({
											modalId:showSubscriptionModalId,
											mainTitleParams:{
												text:'תיאור האימון/ביקור', 
												css:{"background-color":'#00B8C0'}
											},
											bodyParams:{
												html:
													'<div class="modal-text" '
														+'style="'
															+'min-height:100px;'
															+'max-height:350px;'
															+'overflow-x:auto;'
														+'"'
													+'>'
														+selectedRowData.description
													+'</div>'
											},
											buttonsParams:[
												{text:'סגור', iconCls:'fas fa-times',
													events:{
														click:function(){
															this.closeModal();
														}
													}
												}
											]
										}).showModal();
									}
								}					
							},
							{category:'button', name:'edit_sub_training', text:'עריכה', 
								iconCls:'fas fa-user-edit', btnCls:'warning', disabled:true,
								events:{
									click:function(){
										var selectedRowData = this.getSelectedRowData();
										
										if(!selectedRowData) {
											openMsgModal({msgText:"לא נבחרה שורה לעריכה", msgType:'error'});
											return;
										}
										
										var userData = getUserData();
										
										if(!isAdmin() && selectedRowData.updated_by != userData.user_id) {
											openMsgModal({
												msgText:
													'אינך מורשה לערוך את הרשומה המסומנת.</br>'
													+'רק מקים הרשומה רשאי לבצע בה שינויים.', 
												msgType:'error'
											});
											return;
										}

										openSetSubscriptionTrainingModal({
											submitAction:"edit_sub_training", 
											mainTitleText:'עדכון אימון/ביקור', 
											submitBtnText:'עדכן',
											fillData:selectedRowData,
											callback: $.proxy(function(){
												this.loadStore();
											},this)
										});
									}
								}
							},
							{category:'button', name:'delete_sub_training', text:'הסרה', 
								iconCls:'fas fa-user-edit', btnCls:'danger', disabled:true,
								events:{
									click:function(){
										var selectedRowData = this.getSelectedRowData();
										
										if(!selectedRowData) {
											openMsgModal({msgText:"לא נבחרה שורה לעריכה", msgType:'error'});
											return;
										}
										
										var userData = getUserData();
										
										if(!isAdmin() && selectedRowData.updated_by != userData.user_id) {
											openMsgModal({
												msgText:
													'אינך מורשה לערוך את הרשומה המסומנת.</br>'
													+'רק מקים הרשומה רשאי לבצע בה שינויים.', 
												msgType:'error'
											});
											return;
										}

										openYesNoQuestionModal({
											msgText:'האם ברונצך להסיר את האימון/ביקור המסומן?',
											yesBtnOnClick:$.proxy(function(){
												closeOpenYesNoQuestionModal();
												showPreloader();

												$.ajax({
													type:"POST",
													url:"subscribers/delete_sub_training",
													data: {
														subscription_traning_id:selectedRowData.subscription_traning_id
													},
													success:$.proxy(function (record) {
														hidePreloader();

														if(record.success) {
															openMsgModal({msgText:record.msg});
															this.loadStore();													
														}
														else {
															openMsgModal({msgText:record.msg, msgType:'error'});
														}
													},this)
												});
											},this)
										});
									}
								}
							}
						]
					}
				});
			}
		},		
		buttonsParams:[
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times', css:{'margin-right':'6px'},
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
	}).showModal();
};