// ----------------------------------------------
function setSubscribersMainGrid() {
	showPreloader();

	SubscribersGridTable = new GridTable({
		parentElemId:"subscribers_list_grid",
		columns:[
			{name:"subscription_full_name", text:"שם מלא", sortable:true},
			{name:"id_number", text:"ת.ז.", sortable:true},
			{name:"expiry_date", text:"תוקף המנוי", sortable:true},
			{name:"last_traning_date", text:"תאריך אימון אחרון", sortable:true},
			{name:"user_full_name", text:"מטפל", sortable:true, hidden:isBasicUser()},
			{name:"gym_name", text:"חברה", sortable:true, hidden:!isAdmin()},
			{name:"gym_branch_name", text:"סניף", sortable:true, hidden:(isBranchUser() || isBasicUser())}
		],
		store:{
			url:'subscribers/get_subscribers_list',
			showPreloaderOnLoad:false,
			sortBy:'last_traning_date',
			sortDirection:'DESC',
			autoLoad:true
		},
		events:{
			onSelectChange:function(){				
				refreshSubscriptionGridTopToolBar(this);
			},
			onStoreLoad:function(){
				this.store.showPreloaderOnLoad = true;
			}
		},
		topToolBar:{
			search:{minChars:2, placeholder:'חיפוש'},
			fields:[
				{category:'button', name:'add_subscription', 
					iconCls:'fas fa-plus', text:'מנוי חדש', btnCls:'success',
					events:{
						click:function(){
							openSubscriptionModal({
								submitAction:"add_subscription", 
								mainTitleText:'מנוי חדש', 
								submitBtnText:'הוסף',
								reloadGrid:this
							});
						}
					}
				},
				{category:'button', name:'set_training', text:'רישום אימון/ביקור', hidden:!isBasicUser(),
					iconCls:'fas fa-dumbbell', btnCls:'primary',
					events:{
						click:function(){
							openSelectTrainingModal({
								callbackFunc: $.proxy(function(selectedTraining){
									var selectedSubscription = this.getSelectedRowData();
									if(isUndefined(selectedTraining) || !selectedTraining) {
										selectedTraining = {};
									}

									if(!selectedSubscription) {
										openMsgModal({msgText:"לא נבחר מנוי לעריכה", msgType:'error'});
										return;
									}

									fillData = selectedSubscription;
									fillData.description = setDefaultWhenUndefined(selectedTraining.description, "");
									fillData.training_name = setDefaultWhenUndefined(selectedTraining.training_name, "");

									openSetSubscriptionTrainingModal({
										submitAction:"add_sub_training", 
										mainTitleText:'הוספת אימון/ביקור', 
										submitBtnText:'הוסף',
										fillData:fillData,
										callback: $.proxy(function(){
											this.loadStore();
										},this)
									});
								},this)
							});
						}
					}
				},
				{category:'button', name:'training_history', hidden:!isBasicUser(),
					text:'היסטוריית אימונים/ביקורים', btnCls:'info',
					events:{
						click:function(){
							openSubscriptionTrainingListModal({
								selectedSubscription:this.getSelectedRowData()
							});
						}
					}
				},
				{category:'dropDownMenu', name:'actions_menu', text:'פעולות', hidden:isBasicUser(),
					btnCls:'info', iconCls:'fas fa-hand-point-down', disabled:true, 
					items:[
						{name:'set_training', text:'רישום אימון/ביקור', iconCls:'fas fa-dumbbell mrg-l-8', 
							events:{
								click:function(){
									openSelectTrainingModal({
										callbackFunc: $.proxy(function(selectedTraining){
											var selectedSubscription = this.getSelectedRowData();
											if(isUndefined(selectedTraining) || !selectedTraining) {
												selectedTraining = {};
											}

											if(!selectedSubscription) {
												openMsgModal({msgText:"לא נבחר מנוי לעריכה", msgType:'error'});
												return;
											}

											fillData = selectedSubscription;
											fillData.description = setDefaultWhenUndefined(selectedTraining.description, "");
											fillData.training_name = setDefaultWhenUndefined(selectedTraining.training_name, "");

											openSetSubscriptionTrainingModal({
												submitAction:"add_sub_training", 
												mainTitleText:'הוספת אימון/ביקור', 
												submitBtnText:'הוסף',
												fillData:fillData,
												callback: $.proxy(function(){
													this.loadStore();
												},this)
											});
										},this)
									});
								}
							}
						},
						{name:'edit_subscription', text:'עריכה', 
							iconCls:'fas fa-user-edit mrg-l-8 fo-co-red-warning',
							events:{
								click:function(){
									var selectedRowData = this.getSelectedRowData();
									var mainTitleText = 'עדכון מנוי';

									if(!selectedRowData) {
										openMsgModal({msgText:"לא נבחרה שורה לעריכה", msgType:'error'});
										return;
									}

									if(selectedRowData && isDefined(selectedRowData.subscription_full_name)) {
										mainTitleText += ' '+selectedRowData.subscription_full_name;
									}

									openSubscriptionModal({
										submitAction:"edit_subscription", 
										mainTitleText:mainTitleText, 
										submitBtnText:'עדכן',
										fillData:selectedRowData,
										reloadGrid:this
									});
								}
							}
						},
						{name:'delete_subscription', text:'מחיקה', 
							iconCls:'fas fa-user-minus mrg-l-8 fo-co-red-danger', 
							events:{
								click:function(){
									doDeleteRestoreSubscriptionAction({
										action:'delete_subscription',
										selectedRowData:this.getSelectedRowData(),
										reloadGrid:this
									});
								}
							}
						},
						{name:'restore_subscription', text:'שחזור', 
							iconCls:'fas fa-user-check mrg-l-8 fo-co-red-warning', hidden:true,
							events:{
								click:function(){
									doDeleteRestoreSubscriptionAction({
										action:'restore_subscription',
										selectedRowData:this.getSelectedRowData(),
										reloadGrid:this
									});
								}
							}
						},
						{type:'divider'},
						{name:'change_user', text:'עדכון/שינוי מטפל', hidden:isBasicUser(),
							events:{
								click:function(){
									setSubscriptionUserModal({
										selectedSubscription:this.getSelectedRowData(),
										callback: $.proxy(function(selectedRow){
											this.loadStore();
										},this)
									});
								}
							}
						},				
						{name:'training_history', text:'היסטוריית אימונים/ביקורים',
							events:{
								click:function(){
									openSubscriptionTrainingListModal({
										selectedSubscription:this.getSelectedRowData()
									});
								}
							}
						}
					]
				},
				{category:'selectBox', name:'deleted_active_sb', hidden:!isAdmin(), 
					value:'active', attachToStoreLoad:true,
					store:[
						{text:'פעילים', value:'active'},						
						{text:'פגי תוקף', value:'expired'},
						{text:'מחוקים', value:'deleted'}
					],
					events:{
						change:function(el){
							this.loadStore();
						}
					}
				},
				getSelectGymButton({hidden:!isAdmin(), value:'כל החברות', defaulGymBranchName:'כל הסניפים'}),
				getSelectGymBranchButton({hidden:(!isManagerUser() && !isAdmin()), value:'כל הסניפים'}),
				{category:'input', name:'gym_id', value:getGymsDefaultValue(), attachToStoreLoad:true, hidden:true},
				{category:'input', name:'gym_branch_id', 
					value:getGymsBranchesDefaultValue(), 
					attachToStoreLoad:true, hidden:true
				},
				{category:'button', name:'clear_filters', iconCls:'fas fa-filter', 
					text:'איפוס סינונים', btnCls:'warning', hidden:(!isManagerUser() && !isAdmin()),
					events:{
						click:function(){
							gymIdElem = this.findTopToolBarFieldByName('gym_id');
                            gymNameElem = this.findTopToolBarFieldByName('gym_name');
                            gymBranchIdElem = this.findTopToolBarFieldByName('gym_branch_id');
							gymBranchNameElem = this.findTopToolBarFieldByName('gym_branch_name');
							
							if(gymIdElem) gymIdElem.val(0);
							if(gymBranchIdElem) gymBranchIdElem.val(0);
							if(gymNameElem) gymNameElem.val('כל החברות');
							if(gymBranchNameElem) gymBranchNameElem.val('כל הסניפים');

							this.loadStore();
						}
					}					
				}
			]
		}
	});
};

// ----------------------------------------------
function refreshSubscriptionGridTopToolBar(subscribersGrid) {
	if(isUndefined(subscribersGrid) || !objLength(subscribersGrid)) {
		openMsgModal({msgText:'שגיאת מערכת! [refreshSubscriptionGridTopToolBar-subscribersGrid]', msgType:'error'});
		return false;
	}

	// training_history set_training

	var showDeleteRestoreBtns = false;
	var showBasicBtns = true;

	var selectedRowElem = subscribersGrid.getSelectedRowElem();
	var deletedActiveSubscriptionSB = subscribersGrid.findTopToolBarFieldByName('deleted_active_sb');
	var actionsMenuBtn = subscribersGrid.findTopToolBarFieldByName('actions_menu');
	var addSubscriptionBtn = subscribersGrid.findTopToolBarFieldByName('add_subscription');
	var editSubscriptionBtn = subscribersGrid.findTopToolBarFieldByName('edit_subscription');
	var setTrainingBtn = subscribersGrid.findTopToolBarFieldByName('set_training');
	var trainingHistoryBtn = subscribersGrid.findTopToolBarFieldByName('training_history');
	var deleteSubscriptionBtn = subscribersGrid.findTopToolBarFieldByName('delete_subscription');
	var deleteForeverSubscriptionBtn = subscribersGrid.findTopToolBarFieldByName('delete_forever_subscription');
	var restoreSubscriptionBtn = subscribersGrid.findTopToolBarFieldByName('restore_subscription');

	if(actionsMenuBtn) setFormFieldDisabled(actionsMenuBtn, (selectedRowElem ? false : true));
	if(setTrainingBtn) setFormFieldDisabled(setTrainingBtn, (selectedRowElem ? false : true));
	if(trainingHistoryBtn) setFormFieldDisabled(trainingHistoryBtn, (selectedRowElem ? false : true));

	if(deletedActiveSubscriptionSB) {
		if(deletedActiveSubscriptionSB.val()=='deleted') {
			showDeleteRestoreBtns = true;
			showBasicBtns = false;
		}
		else if(deletedActiveSubscriptionSB.val()=='expired') {
			showDeleteRestoreBtns = false;
			showBasicBtns = true;
		}
		else {
			deletedActiveSubscriptionSB.val('active');
			showDeleteRestoreBtns = false;
			showBasicBtns = true;
		}
	}

	// -- Set basic buttons 'visible' attribute -- //
	if(addSubscriptionBtn) setFormFieldVisible(addSubscriptionBtn, showBasicBtns);
	if(editSubscriptionBtn) setFormFieldVisible(editSubscriptionBtn, showBasicBtns);
	if(deleteSubscriptionBtn) setFormFieldVisible(deleteSubscriptionBtn, showBasicBtns);
	// -- Set special buttons 'visible' attribute -- //
	if(deleteForeverSubscriptionBtn) setFormFieldVisible(deleteForeverSubscriptionBtn, showDeleteRestoreBtns);
	if(restoreSubscriptionBtn) setFormFieldVisible(restoreSubscriptionBtn, showDeleteRestoreBtns);
};