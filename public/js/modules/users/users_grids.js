// ----------------------------------------------
function setUsersGrid() {	
	var userData = getUserData();
	var gymsList = getGymsList();
	var gymsBranchesList = getGymsBranchesList();

	if(!userData) {
		openMsgModal({msgText:"שגיאה! [setUsersGrid-userData]", msgType:'error'});
		return;
	}

	showPreloader();
	
	UsersGridTable = new GridTable({
		parentElemId:"users_list_grid",
		columns:[
			{name:"full_name", text:"שם מלא", sortable:true},
			{name:"email", text:'דוא"ל', sortable:true},
			{name:"username", text:'שם משתמש', sortable:true},
			{name:"id_number", text:"ת.ז.", sortable:true},
			{name:"type_name_heb", text:"סוג", sortable:true},
			{name:"gym_name", text:"חברה", hidden:(!isAdmin()), sortable:true},
			{name:"gym_branch_name", text:"סניף", hidden:(isBranchUser()), sortable:true}
		],
		store:{
			url:'users/get_users_list',
			showPreloaderOnLoad:false,
			autoLoad:true,
			sortBy:'full_name',
			sortDirection:'ASC'
		},
		events:{
			onSelectChange:function(){				
				refreshUserGridTopToolBar(this);
			},
			onStoreLoad:function(){
				this.store.showPreloaderOnLoad = true;
			}
		},
		topToolBar:{
			search:{minChars:2, placeholder:'חיפוש'},
			fields:[
				{category:'button', name:'add_user', iconCls:'fas fa-user-plus', 
					text:'משתמש חדש', btnCls:'success',
					events:{
						click:function(){
							openUserModal({submitAction:"add_user", mainTitleText:'משתמש חדש', submitBtnText:'הוסף'});
						}
					}					
				},		
				{category:'dropDownMenu', name:'actions_menu', text:'פעולות', 
					btnCls:'info', iconCls:'fas fa-hand-point-down', disabled:true, 
					items:[
						{name:'edit_user', 	iconCls:'fas fa-user-edit mrg-l-8 fo-co-red-warning', text:'עריכה',
							events:{
								click:function(){
									var selectedRowData = this.getSelectedRowData();
									var mainTitleText = 'עדכון משתמש';

									if(!selectedRowData) {
										openMsgModal({msgText:"לא נבחרה שורה לעריכה", msgType:'error'});
										return;
									}

									if(isDefined(selectedRowData.full_name)) mainTitleText += ' '+selectedRowData.full_name;

									openUserModal({
										submitAction:"edit_user", 
										mainTitleText:mainTitleText, 
										submitBtnText:'עדכן',
										fillData:selectedRowData
									});
								}
							}
						},
						{name:'delete_user',
							iconCls:'fas fa-user-minus mrg-l-8 fo-co-red-danger', text:'מחיקה',
							events:{
								click:function(){
									doDeleteRestoreUserAction('delete_user', this.getSelectedRowData());
								}
							}
						},
						{name:'restore_user', iconCls:'fas fa-user-check mrg-l-8 fo-co-red-warning', 
							text:'שחזור', hidden:true,
							events:{
								click:function(){
									doDeleteRestoreUserAction('restore_user', this.getSelectedRowData());
								}
							}
						},
						{name:'delete_forever_user', iconCls:'fas fa-user-minus mrg-l-8 fo-co-red-danger', 
							text:'מחיקה לצמיתות', hidden:true,
							events:{
								click:function(){
									doDeleteRestoreUserAction('delete_forever_user', this.getSelectedRowData());
								}
							}
						}
						// {type:'divider'}
					]
				},
				{category:'selectBox', name:'deleted_active_sb', hidden:!isAdmin(), value:'active', attachToStoreLoad:true,
					store:[
						{text:'פעילים', value:'active'},
						{text:'מחוקים', value:'deleted'}
					],
					events:{
						change:function(el){
							refreshUserGridTopToolBar(this);
							this.loadStore();
						}
					}
				},
				getSelectGymButton({hidden:!isAdmin(), value:'כל החברות', defaulGymBranchName:'כל הסניפים'}),
				getSelectGymBranchButton({hidden:(!isManagerUser() && !isAdmin()), value:'כל הסניפים'}),
				{category:'input', name:'gym_id', value:getGymsDefaultValue(), attachToStoreLoad:true, hidden:true},
				{category:'input', name:'gym_branch_id', 
					value:getGymsBranchesDefaultValue(), 
					attachToStoreLoad:true, hidden:true
				},
				{category:'button', name:'clear_filters', iconCls:'fas fa-filter', 
					text:'איפוס סינונים', btnCls:'warning', hidden:(!isManagerUser() && !isAdmin()),
					events:{
						click:function(){
							gymIdElem = this.findTopToolBarFieldByName('gym_id');
                            gymNameElem = this.findTopToolBarFieldByName('gym_name');
                            gymBranchIdElem = this.findTopToolBarFieldByName('gym_branch_id');
							gymBranchNameElem = this.findTopToolBarFieldByName('gym_branch_name');
							
							if(gymIdElem) gymIdElem.val(0);
							if(gymBranchIdElem) gymBranchIdElem.val(0);
							if(gymNameElem) gymNameElem.val('כל החברות');
							if(gymBranchNameElem) gymBranchNameElem.val('כל הסניפים');

							this.loadStore();
						}
					}					
				}
			]
		}
	});
};

// ----------------------------------------------
function refreshUserGridTopToolBar(usersGrid) {
	if(isUndefined(usersGrid) || !objLength(usersGrid)) {
		openMsgModal({msgText:'שגיאת מערכת! [refreshUserGridTopToolBar-usersGrid]', msgType:'error'});
		return false;
	}

	var showDeleteRestoreBtns = false;
	var showBasicBtns = true;

	var selectedRowElem = usersGrid.getSelectedRowElem();

	var deletedActiveUserSB = usersGrid.findTopToolBarFieldByName('deleted_active_sb');
	var actionsMenuBtn = usersGrid.findTopToolBarFieldByName('actions_menu');
	var addUserBtn = usersGrid.findTopToolBarFieldByName('add_user');
	var editUserBtn = usersGrid.findTopToolBarFieldByName('edit_user');
	var deleteUserBtn = usersGrid.findTopToolBarFieldByName('delete_user');
	var deleteForeverUserBtn = usersGrid.findTopToolBarFieldByName('delete_forever_user');
	var restoreUserBtn = usersGrid.findTopToolBarFieldByName('restore_user');

	if(actionsMenuBtn) setFormFieldDisabled(actionsMenuBtn, (selectedRowElem ? false : true));
	// if(editUserBtn) editUserBtn.prop("disabled", (selectedRowElem ? false : true ));			
	// if(deleteUserBtn) deleteUserBtn.prop("disabled", (selectedRowElem ? false : true ));			
	// if(restoreUserBtn) restoreUserBtn.prop("disabled", (selectedRowElem ? false : true ));			
	// if(deleteForeverUserBtn) deleteForeverUserBtn.prop("disabled", (selectedRowElem ? false : true ));	

	if(deletedActiveUserSB) {
		if(deletedActiveUserSB.val()=='deleted' && isAdmin()) {
			showDeleteRestoreBtns = true;
			showBasicBtns = false;
		}
		else {
			deletedActiveUserSB.val('active');
			showDeleteRestoreBtns = false;
			showBasicBtns = true;
		}
	}

	// -- Set basic buttons 'visible' attribute -- //
	if(addUserBtn) setFormFieldVisible(addUserBtn, showBasicBtns);
	if(editUserBtn) setFormFieldVisible(editUserBtn, showBasicBtns);
	if(deleteUserBtn) setFormFieldVisible(deleteUserBtn, showBasicBtns);
	// -- Set on 'deleted' status buttons 'visible' attribute -- //
	if(deleteForeverUserBtn) setFormFieldVisible(deleteForeverUserBtn, showDeleteRestoreBtns);
	if(restoreUserBtn) setFormFieldVisible(restoreUserBtn, showDeleteRestoreBtns);

	return true;
};