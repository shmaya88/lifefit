var UsersGridTable = UserModal = {};

$( document ).ready(function() {
	if(currentModuleName=='users') setUsersGrid();    
});

// ----------------------------------------------
function setGymFieldsByUserType(formBody, selectedUserTypeInfo) {
	if(isUndefined(formBody) || isUndefined(selectedUserTypeInfo) || !formBody || !selectedUserTypeInfo) return false;

	var setValuesObj = null;

	var gymIdElem =  formBody.find('[name="gym_id"]');
	var gymNameElem =  formBody.find('[name="gym_name"]');
	var gymBranchIdElem =  formBody.find('[name="gym_branch_id"]');
	var gymBranchNameElem =  formBody.find('[name="gym_branch_name"]');
	
	if(gymNameElem.is(":hidden")) gymIdElem = gymNameElem = null;
	if(gymBranchNameElem.is(":hidden")) gymBranchIdElem = gymBranchNameElem = null;

	var disableGym = disableBranch = true;
	
	if(selectedUserTypeInfo.name_eng == 'manager_user') {
		setValuesObj = [
			{elem:gymBranchIdElem, val:''},
			{elem:gymBranchNameElem, val:''}
		];

		disableGym = false;
	}
	else if(selectedUserTypeInfo.name_eng == 'super_user') {
		setValuesObj = [
			{elem:gymIdElem, val:''},
			{elem:gymBranchIdElem, val:''},
			{elem:gymNameElem, val:''},
			{elem:gymBranchNameElem, val:''}
		];
	}
	else {
		disableGym = disableBranch = false;
	}
	
	setFormFieldAllowEmpty(gymNameElem, disableGym);
	setFormFieldAllowEmpty(gymBranchNameElem, disableBranch);

	setFormFieldDisabled(gymNameElem, disableGym);
	setFormFieldDisabled(gymBranchNameElem, disableBranch);

	setElementsValue(setValuesObj);
};

// ----------------------------------------------
function doDeleteRestoreUserAction(action, selectedRowData) {
	if(isUndefined(action) || !action || !inArray(['delete_user','restore_user','delete_forever_user'], action)) {
		openMsgModal({msgText:"שגיאת מערכת. לא אותרה פעולה.[doDeleteRestoreUserAction-action]", msgType:'error'});
		return;
	}

	var actionText = warningMsg = '';

	if(action=='delete_user') {
		actionText = 'להסיר';
		warningMsg = '</br><span style="font-size:12px;">'
				+'לתשומת ליבך, שחזור המשתמש יתבצע רק על ידי מנהל מערכת.'
			+'</span>';
	}
	else if(action=='delete_forever_user') {
		actionText = 'להסיר לצמיתות';
		warningMsg = '</br><span style="font-size:12px;">'
				+'לתשומת ליבך, לאחר ביצוע הסרה לצמיתות, לא ניתן לשחזר את המשתמש.'
			+'</span>';
	}
	else if(action=='restore_user') {
		actionText = 'לשחזר';
	}

	if(
		isUndefined(selectedRowData) || 
		!selectedRowData || 
		isUndefined(selectedRowData.user_id) || 
		!parseInt(selectedRowData.user_id)
	) {
		openMsgModal({msgText:"לא אותר מזהה משתמש. לא ניתן "+actionText+" את המשתמש.", msgType:'error'});
		return;
	}

	var modalMsgText = 
		'המשתמש שנבחר הוא:</br>'
		+selectedRowData.full_name
		+(selectedRowData.id_number ? '('+selectedRowData.id_number+')' : '')
		+'</br>האם ברצונך '+actionText+' את המשתמש?</br>'
		+warningMsg;

	openYesNoQuestionModal({
		msgText:modalMsgText,
		yesBtnOnClick:function(){
			yesNoModal = this;

			showPreloader('#'+yesNoModal.modalUniqueId, "0.9");

			$.ajax({
				type:"POST",
				url:"users/do_delete_restore_user_action",
				data: {
					action:action,
					user_id:selectedRowData.user_id
				},
				success:function (record) {
					yesNoModal.closeModal();

					if(record.success) {
						if(record.msg) openMsgModal({msgText:record.msg});
						UsersGridTable.loadStore();
					}
					else {
						openMsgModal({msgText:record.msg, msgType:'error'});
					}

					hidePreloader();
				}
			});
		}
	});
};

// ----------------------------------------------
function getSelectUserButton(properties) {
	if(isUndefined(properties)) properties = {};

    hidden = setDefaultWhenUndefined(properties.hidden, false);
    allowEmpty = setDefaultWhenUndefined(properties.allowEmpty, false);
    value = setDefaultWhenUndefined(properties.value, '');
    labelText = setDefaultWhenUndefined(properties.labelText, '');
	colMd = setDefaultWhenUndefined(properties.colMd, 0);
	
	return {
        category:'input', name:'user_full_name', value:value, labelText:labelText, 
        colMd:colMd, hidden:hidden, cls:'clickable-input', allowEmpty:allowEmpty,
        events:{
            click:function(el){
                openSelectUserModal({
                    callbackFunc: $.proxy(function(selectedRowData){
                        if(isUndefined(selectedRowData) || !selectedRowData) {
                            openMsgModal({msgText:"לא אותרו נתוני משתמש", msgType:'error'});
                            return;
                        }

                        var gymBranchIdElem = gymIdElem = userFullNameElem = userIdElem = null;
                        var elementClassType = setDefaultWhenUndefined(this.elementClassType, null);
                        var newUserId = setDefaultWhenUndefined(selectedRowData.user_id, 0);
                        var newUserFullName = setDefaultWhenUndefined(selectedRowData.full_name, '');
                        var newGymId = setDefaultWhenUndefined(selectedRowData.gym_id, 0);
                        var newGymBranchId = setDefaultWhenUndefined(selectedRowData.gym_branch_id, 0);

                        if(elementClassType == 'grid') {
                            userFullNameElem = this.findTopToolBarFieldByName('user_full_name');
                            userIdElem = this.findTopToolBarFieldByName('user_id');
                            gymIdElem = this.findTopToolBarFieldByName('gym_id');
                            gymBranchIdElem = this.findTopToolBarFieldByName('gym_branch_id');
                        }
                        else if(elementClassType == 'form') {
                            userFullNameElem = this.getFormFieldElemByName('user_full_name');
                            userIdElem = this.getFormFieldElemByName('user_id');
                            gymIdElem = this.getFormFieldElemByName('gym_id');
                            gymBranchIdElem = this.getFormFieldElemByName('gym_branch_id');
                        }
                        else {
                            openMsgModal({msgText:"סוג האובייקט אינו תקין", msgType:'error'});
                            return;   
                        }
                        
                        if(!userFullNameElem || !userIdElem) {
                            openMsgModal({msgText:"לא אותרו שדות לעריכה [user_full_name|user_id]", msgType:'error'});
                            return;
                        }
                        
                        currId = userIdElem.val();

                        var setValuesObj = [
                            {elem:userFullNameElem, val:newUserFullName},
                            {elem:userIdElem, val:newUserId},
                            {elem:gymIdElem, val:newGymId},
                            {elem:gymBranchIdElem, val:newGymBranchId},
                        ];

                        setElementsValue(setValuesObj);
                        
                        if(elementClassType == 'grid') {
                            this.loadStore();
                        }
                        else if(elementClassType == 'form') {
                            if(userFullNameElem && currId != newUserId) {
								userFullNameElem.attr("changed",true);
							}
                        }

                        return setValuesObj;								
                    },this)
                });
            }
        }
    };
}