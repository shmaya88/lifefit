// ----------------------------------------------
function openUserModal(properties) {
	if(isUndefined(properties)) return false;
	
	// ----- Define default params values ----- //
	var modalId = 'dialog_user_edit_modal';
	$('#'+modalId).remove();
	
	var userTypes = getUsersTypes();

	var submitAction = setDefaultWhenUndefined(properties.submitAction, "add_user", 'checkIfEmpty');
	var mainTitleText = setDefaultWhenUndefined(properties.mainTitleText, "");
	var submitBtnText = setDefaultWhenUndefined(properties.submitBtnText, "");
	var fillData = setDefaultWhenUndefined(properties.fillData, "");

	var typeCodeColMd = gymBranchNameColMd = 4;

	if(!isAdmin()) {
		typeCodeColMd = gymBranchNameColMd = 6;
		if(!isManagerUser()) typeCodeColMd = 12;
	}

	// ----- Create form farams object ----- // 
	var formRowsObj = [
		{fields:[
			{name:'type_code', category:'selectBox', colMd:typeCodeColMd, labelText:'סוג משתמש:', 
				store:getUsersTypesListStore(), allowEmpty:false,
				events:{
					change:function(el){
						var fieldValue = $(el.currentTarget).val();
						var formBody = this.parentElem;

						// ----- Set 'gym_branch_id' field 'AllowEmpty' attribute ----- //
						var selectedUserTypeInfo = setDefaultWhenUndefined(userTypes.by_code[fieldValue], null);
						setGymFieldsByUserType(formBody, selectedUserTypeInfo);							
					}
				}
			},
			getSelectGymButton({colMd:4, hidden:!isAdmin(), labelText:'חברה:', allowEmpty:!isAdmin()}),
			getSelectGymBranchButton({
				colMd:gymBranchNameColMd, hidden:(!isManagerUser() && !isAdmin()), 
				labelText:'סניף:', allowEmpty:(!isManagerUser() && !isAdmin())
			})
		]},
		{fields:[
			{name:'first_name', colMd:4, labelText:'שם פרטי:', placeholder:'שם פרטי', allowEmpty:false},
			{name:'last_name', colMd:4, labelText:'שם משפחה:', placeholder:'שם משפחה', allowEmpty:false},
			{name:'id_number', colMd:4, labelText:'תעודת זהות:', placeholder:'תעודת זהות', 
				maxlength:9, onlyNumbers:true, isIdNumber:true
			}
		]},
		{fields:[
			{name:'tel_type1', category:'selectBox', colMd:2, labelText:'סוג התקשרות:', 
				store:getCommunicationsTypesListStore(),
				value:getCommunicationsTypesDefaultValue(),
				allowEmpty:false
			},
			{name:'tel_num1', colMd:4, labelText:'מספר טלפון:', placeholder:'מספר טלפון', allowEmpty:false},
			{name:'tel_type2', category:'selectBox', colMd:2, labelText:'סוג התקשרות:', 
				store:getCommunicationsTypesListStore(),
				value:getCommunicationsTypesDefaultValue()
			},
			{name:'tel_num2', colMd:4, labelText:'מספר טלפון נוסף:', placeholder:'מספר טלפון נוסף'},
		]},
		{fields:[
			{name:'adr_type1', category:'selectBox', colMd:2, labelText:'סוג הכתובת:', 
				store:getAddressesTypesListStore(),
				value:getAddressesTypesDefaultValue(),
				allowEmpty:false
			},
			{name:'adr1', colMd:4, labelText:'כתובת:', placeholder:'כתובת', allowEmpty:false},
			{name:'adr_type2', category:'selectBox', colMd:2, labelText:'סוג הכתובת:', 
				store:getAddressesTypesListStore(),
				value:getAddressesTypesDefaultValue()
			},
			{name:'adr2', colMd:4, labelText:'כתובת נוספת:', placeholder:'כתובת נוספת'},
		]},
		{fields:[
			{name:'email', colMd:3, labelText:'כתובת דוא"ל:', placeholder:'כתובת דוא"ל', allowEmpty:false},
			{name:'username', colMd:3, labelText:'שם משתמש:', placeholder:'שם משתמש', allowEmpty:false},
			{name:'password', type:'password', colMd:3, labelText:'סיסמא:', placeholder:'סיסמא', allowEmpty:false},
			{name:'password_repeat', type:'password', colMd:3, labelText:'סיסמא שנית:', 
				placeholder:'סיסמא שנית', 
				allowEmpty:false
			},
		]},
		{type:'hidden', fields:[
			{name:'gym_id', type:'hidden', value:getGymsDefaultValue()},
			{name:'gym_branch_id', type:'hidden', value:getGymsBranchesDefaultValue()},
			{name:'user_id', type:'hidden'},
			{name:'submit_action', type:'hidden', value:submitAction}
		]}
	];

	// ----- Fill form ----- // 
	if(fillData) formRowsObj = fillFormObject(formRowsObj, fillData);

	// ----- Create user add/update modal ----- // 
	UserModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:mainTitleText
		},
		bodyParams:{formRowsObj:formRowsObj},
		events:{
			afterLoad:function(){
				var modal = this;
				var formBody = modal.modalBodyElem;

				// ----- Run on all field ----- //
				$.each(formBody.find('.form-field-e'), function( key, el ) {
					var fieldName = $(this).attr('name');
					var fieldValue = $(this).val();

					// ----- Set 'gym_branch_id' field 'AllowEmpty' attribute ----- //
					if(fieldName=='type_code') {
						var selectedUserTypeInfo = setDefaultWhenUndefined(userTypes.by_code[fieldValue], null);
						setGymFieldsByUserType(formBody, selectedUserTypeInfo);
					}

					if($(this).attr("type")=='password') {
						if(submitAction=='edit_user') {
							// ----- Set password inputs 'AllowEmpty' attribute ----- //
							setFormFieldAllowEmpty($(this), true);
						}

						// ----- Default password inputs value ----- //
						$(this).val('         ');
					}

					// ----- Set inputs events ----- //
					$(this).hover(function() {
						// ----- Run on all field, to reset them ----- //
						$.each(formBody.find('.form-field-e'), function( pK, pEl ) {
							if(
								booleanCheckAdv($(this).attr("focused")) || 
								booleanCheckAdv($(this).attr("forceValued")) || 
								$(this).attr("type")=='hidden'
							) return;

							$(this).val("");
						});
					});
				});
			}
		},		
		buttonsParams:[
			{text:submitBtnText, name:'submit_btn',
				iconCls:(submitAction=='edit_user' ? 'fas fa-user-edit' : 'fas fa-user-plus'), 
				styleClasses:'add-btn-s',
				events:{
					click:function(){
						var modal = this;
						var formBody = modal.modalBodyElem;

						var params = getFormParamsWithValidate(formBody);
						if(!params) return;

						if(isUndefined(params.submit_action) || !inArray(['add_user','edit_user'], params.submit_action)) {
							openMsgModal({msgText:'שגיאת מערכת! לא אותר סוג הפעולה', msgType:'error'});
							return;
						}
						
						showPreloader('#'+modalId, "1");

						if(!checkIfFormChanged(formBody)) {
							openMsgModal({msgText:'לא בוצעו שינויים בנתונים.'});
							hidePreloader();
							return;
						}

						$.ajax({
							type:"POST",
							url:"users/"+params.submit_action,
							data: params,
							success:function (record) {
								if(record.success) {
									openMsgModal({msgText:record.msg});
									UsersGridTable.loadStore();
									modal.closeModal();															
								}
								else {
									hidePreloader();
									openMsgModal({msgText:record.msg, msgType:'error'});
								}
							}
						});
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times',
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
	}).showModal();
};

// ----------------------------------------------
function openSelectUserModal(properties) {
	if(isUndefined(properties)) properties = {};

	properties.callbackFunc = setDefaultWhenUndefined(properties.callbackFunc, null);

	var selectedUserData = null;

	var modalId = 'dialog_select_user_modal';
	$('#'+modalId).remove();

	selectUserModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:'בחירת משתמש'
		},
		bodyParams:{
			html:'<div id="select_user_modal_grid"></div>'
		},
		events:{
			afterLoad:function(){
				UserSelectGridTable = new GridTable({
					parentElemId:"select_user_modal_grid",
					columns:[
						{name:"full_name", text:"שם מלא", sortable:true},
						{name:"id_number", text:"ת.ז.", sortable:true},
						{name:"gym_name", text:"חברה", hidden:(!isAdmin()), sortable:true},
						{name:"gym_branch_name", text:"סניף", hidden:(isBranchUser()), sortable:true}
					],
					events:{
						onSelectChange:function(){				
							selectedUserData = this.getSelectedRowData();

							$('#'+modalId).find('[name="select_btn"]')
								.prop("disabled", (selectedUserData ? false : true ));
						},
					},
					store:{
						url:'users/get_users_list',
						sortBy:'full_name',
						sortDirection:'ASC',
						rowsLimit:5
					},
					topToolBar:{
						search:{minChars:2, placeholder:'חיפוש'}
					}
				});
			}
		},		
		buttonsParams:[
			{text:'בחר', name:'select_btn', iconCls:'fas fa-check-circle', 
				styleClasses:'add-btn-s', disabled:true,
				events:{
					click:function(){
						if(!selectedUserData) {
							openMsgModal({msgText:'לא נבחר משתמש', msgType:'error'});
							return;
						}

						if(properties.callbackFunc) properties.callbackFunc(selectedUserData);
						this.closeModal();
					}
				}
			},
			{text:"סגור", name:'close_btn', iconCls:'fas fa-times', css:{'margin-right':'6px'},
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		]
	}).showModal();
};