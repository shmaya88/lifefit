// ----------------------------------------------
function createFormElement(properties) {
	if(isUndefined(properties)) {
		openMsgModal({msgText:'נתוני הטופס אינם תקינים[createForm-properties]', msgType:'error'});
		return false;
	}

	var errorMsg = '';
	this.elementClassType = "form";
	this.formUniqueId = "form"+generateUniqueId();
	
	// -- Define default params values -- //
	this.formRows = setDefaultWhenUndefined(properties.formRows, null, 'checkIfEmpty');
	this.parentElemId = setDefaultWhenUndefined(properties.parentElemId, null, 'checkIfEmpty');

	if(this.parentElemId) this.parentElem = $('#'+this.parentElemId);

	// -- Necessary params checking -- //
	if(!this.formRows || !isArray(formRows) || !formRows.length) errorMsg = 'לא הוגדרו שדות לטופס[createForm-formRows]';
	else if(!this.parentElemId) errorMsg = 'לא מזהה של בעל הטופס[createForm-parentElemId]';
	else if(!this.parentElem || !this.parentElem.length) errorMsg = 'לא אותא בעל הטופס [createForm-parentElem]';

	if(errorMsg) {
		openMsgModal({msgText:errorMsg, msgType:'error'});
		return false;
	}

	// ----------------------------------------------
	this.createForm = function() {
		this.createRows();
	};

	// ----------------------------------------------
	this.createRows = function() {
		var fieldCnt = rowCnt = 1;

		// -- Run on rows -- //
		$.each(this.formRows, $.proxy(function(rIndex, fRowData) {
			if(isUndefined(fRowData.fields) || !isArray(fRowData.fields) || !fRowData.fields.length) return;

			var rowType = setDefaultWhenUndefined(fRowData.type, '');
			var rowCls = setDefaultWhenUndefined(fRowData.cls, '');
			var rowId = this.formUniqueId+'_row'+rowCnt;

			this.parentElem.append('<div id="'+rowId+'" class="form-row"></div>');

			var rowElem = $('#'+rowId);

			if(rowType=='hidden') rowElem.addClass('hidden-class');
			if(rowCls) rowElem.addClass(rowCls);

			// -- Run on fields -- //
			$.each(fRowData.fields, $.proxy(function(fIndex, fieldData) {
				if(!this.createField(rowElem, fieldData, fieldCnt++)) return false;
			},this));

			rowCnt++;

		},this));
	};

	// ----------------------------------------------
	this.createField = function(rowElem, fieldData, fieldCnt) {
		var errorMsg = '';
		
		fieldData.fieldId = this.formUniqueId+'_field'+fieldCnt;
		fieldData.type = setDefaultWhenUndefined(fieldData.type, 'text');
		fieldData.category = setDefaultWhenUndefined(fieldData.category, 'input');
		fieldData.name = setDefaultWhenUndefined(fieldData.name, null, 'checkIfEmpty');
		fieldData.placeholder = setDefaultWhenUndefined(fieldData.placeholder, '');
		fieldData.labelText = setDefaultWhenUndefined(fieldData.labelText, '');
		fieldData.fieldDescription = setDefaultWhenUndefined(fieldData.fieldDescription, '', 'checkIfEmpty');
		fieldData.scope = setDefaultWhenUndefined(fieldData.scope, null, 'checkIfEmpty');
		fieldData.cls = setDefaultWhenUndefined(fieldData.cls, '', 'checkIfEmpty');
		fieldData.colMd = setDefaultWhenUndefined(fieldData.colMd, 0);
		fieldData.hidden = setDefaultWhenUndefined(fieldData.hidden, false);

		if(!fieldData.fieldDescription) {
			if(fieldData.placeholder) fieldData.fieldDescription = fieldData.placeholder;
			else if(fieldData.labelText) fieldData.fieldDescription = fieldData.labelText;
		}

		if(!fieldData.colMd && !fieldData.hidden && fieldData.type!='hidden') {
			errorMsg = 'נא להזין את גודל השדה בטופס[createField-colMd]';
		}
		else if(!fieldData.name) errorMsg = 'נא להזין את שם השדה[createField-name]';
		else if(!fieldData.category) errorMsg = 'הקטגוריה של השדה אינה תקינה[createField-category]';

		if(errorMsg) {
			openMsgModal({msgText:errorMsg, msgType:'error'});
			return false;
		}

		rowElem.append(
			'<div id="'+fieldData.fieldId+'_div" class="form-group col-md-'+fieldData.colMd+'"></div>'
		);
		
		var fieldDivElem = $('#'+fieldData.fieldId+'_div');

		// -- Create label -- //
		fieldDivElem.append(
			'<label for="'+fieldData.fieldId+'">'
				+'<span class="form-asterisk-cp1">'
					+'<i class="fas fa-asterisk"></i>'
				+'</span>'
				+fieldData.labelText
			+'</label>'
		);

		if(fieldData.hidden) {
			fieldDivElem.hide();
			fieldDivElem.find('[for="'+fieldData.fieldId+'"]').hide();
		}

		if(!fieldData.labelText) $('label[for="'+fieldData.fieldId+'"]').hide();

		fieldData.parentElem = fieldDivElem;
		if(!fieldData.scope) fieldData.scope = this;
		fieldData.cls += ' form-field-e input-sm';
		
		fieldData.fieldElem = new FormFieldCreator(fieldData);
		
		return true;
	};

	// ----------------------------------------------
	this.getFormFieldElemByName = function(fieldName) {
		if(!this.parentElem || isUndefined(fieldName) || !fieldName) return false;

		var elem = this.parentElem.find('[name="'+fieldName+'"]');

		if(elem.length) return elem;
		else return false; 
	};

	this.createForm();
};

// ----------------------------------------------
function FormFieldCreator(fieldData) {
	if(isUndefined(fieldData)) {
		openMsgModal({msgText:"נא להגדיר נתוני השדה", msgType:'error'});
		return false;
	}

	var errorMsg = '';

	this.fieldData = {};
	this.fieldElem = null;

	this.fieldData.parentElem = setDefaultWhenUndefined(fieldData.parentElem, null);
	this.fieldData.fieldId = setDefaultWhenUndefined(fieldData.fieldId, '');
	this.fieldData.category = setDefaultWhenUndefined(fieldData.category, 'undefined_category');
	this.fieldData.type = setDefaultWhenUndefined(fieldData.type, '');
	this.fieldData.placeholder = setDefaultWhenUndefined(fieldData.placeholder, '');
	this.fieldData.value = setDefaultWhenUndefined(fieldData.value, '');
	this.fieldData.disabled = setDefaultWhenUndefined(fieldData.disabled, false);
	this.fieldData.readOnly = setDefaultWhenUndefined(fieldData.readOnly, false);
	this.fieldData.hidden = setDefaultWhenUndefined(fieldData.hidden, false);
	this.fieldData.iconOnsetElem = setDefaultWhenUndefined(fieldData.iconOnsetElem, null);
	// this.fieldData.labelText = setDefaultWhenUndefined(fieldData.labelText, '');
	this.fieldData.text = setDefaultWhenUndefined(fieldData.text, 'לא מוגדר');
	this.fieldData.btnCls = setDefaultWhenUndefined(fieldData.btnCls, '');
	this.fieldData.maxlength = setDefaultWhenUndefined(fieldData.maxlength, 255);
	this.fieldData.onlyNumbers = setDefaultWhenUndefined(fieldData.onlyNumbers, false);
	// this.fieldData.colMd = parseInt(setDefaultWhenUndefined(fieldData.colMd, 0));
	this.fieldData.iconCls = setDefaultWhenUndefined(fieldData.iconCls, null, 'checkIfEmpty');
	this.fieldData.moreAttr = setDefaultWhenUndefined(fieldData.moreAttr, null, 'checkIfEmpty');
	this.fieldData.name = setDefaultWhenUndefined(fieldData.name, null, 'checkIfEmpty');
	this.fieldData.store = setDefaultWhenUndefined(fieldData.store, null, 'checkIfEmpty');
	this.fieldData.items = setDefaultWhenUndefined(fieldData.items, null, 'checkIfEmpty');
	this.fieldData.css = setDefaultWhenUndefined(fieldData.css, null, 'checkIfEmpty');
	this.fieldData.cls = setDefaultWhenUndefined(fieldData.cls, '');
	this.fieldData.allowEmpty = setDefaultWhenUndefined(fieldData.allowEmpty, true);
	this.fieldData.isIdNumber = setDefaultWhenUndefined(fieldData.isIdNumber, false);
	this.fieldData.fieldDescription = setDefaultWhenUndefined(fieldData.fieldDescription, '');
	this.fieldData.events = setDefaultWhenUndefined(fieldData.events, null, 'checkIfEmpty');
	this.fieldData.scope = setDefaultWhenUndefined(fieldData.scope, null, 'checkIfEmpty');

	if(!this.fieldData.fieldId) this.fieldData.fieldId = generateUniqueId()+'_field';
	if(!this.fieldData.parentElem) this.fieldData.parentElem = $("body");
	
	if(!this.fieldData.name) errorMsg = 'נא להזין את שם השדה[FormFieldCreator-name]';
	else if(!inArray(
			['input','selectBox','date','button','dropDownMenu','dropDownItem','textEditor','textarea'], 
			this.fieldData.category
		)) {
		errorMsg = 'הקטגוריה של השדה אינה תקינה[formFieldCreator-category-'+this.fieldData.category+']';
	}

	if(errorMsg) {
		openMsgModal({msgText:errorMsg, msgType:'error'});
		return false;
	}

	// ----------------------------------------------
	this.createFieldByCategory = function() {
		if(this.fieldData.category == 'selectBox') this.createSelectBoxField();
		else if(this.fieldData.category == 'button') this.createButtonField();
		else if(this.fieldData.category == 'input') this.createInputField();
		else if(this.fieldData.category == 'textarea') this.createTextareaField();
		else if(this.fieldData.category == 'date') this.createDateField();
		else if(this.fieldData.category == 'textEditor') this.createTextEditorField();
		else if(this.fieldData.category == 'dropDownMenu') this.createDropDownMenuField();
		else if(this.fieldData.category == 'dropDownItem') this.createDropDownItemField();
	};

	// ----------------------------------------------
	this.createDropDownMenuField = function() {
		// -- Create 'dropDownMenu' div -- //
		this.fieldData.parentElem.append(
			'<div '
				+'id="'+this.fieldData.fieldId+'" '
				+'class="btn-group" '
			+'></div>'
		);

		this.fieldElem = $('#'+this.fieldData.fieldId);

		this.setFieldBasicDefinitions();
		this.setFieldEvents();

		// - Copy without reference - //
		var dropDownBtnData = $.extend(true, {}, this.fieldData);

		dropDownBtnData.fieldId = this.fieldData.fieldId+'_btn';
		dropDownBtnData.name = dropDownBtnData.name+'_btn';
		dropDownBtnData.category = 'button';
		dropDownBtnData.cls = 'dropdown-toggle';
		dropDownBtnData.parentElem = $.extend(true, {}, this.fieldElem);//this.fieldElem;
		dropDownBtnData.items = null;
		dropDownBtnData.moreAttr = {
			'data-toggle':'dropdown',
			'aria-haspopup':'true',
			'aria-expanded':'false'
		};

		dropDownBtnElem = new FormFieldCreator(dropDownBtnData);

		if(!dropDownBtnData.iconCls) dropDownBtnElem.prepend('<span class="caret drop-down-menu-caret"></span>');		

		this.fieldElem.append(
			'<ul '
				+'id="'+this.fieldData.fieldId+'_dropdown_menu" '
				+'class="dropdown-menu" '
			+'></ul>'
		);
		
		var dropdownMenuElem = $('#'+this.fieldData.fieldId+'_dropdown_menu');

		// -- Set 'dropDownMenu' items -- //
		if(this.fieldData.items && isArray(this.fieldData.items) && this.fieldData.items.length) {

			$.each(this.fieldData.items, $.proxy(function(itemIndex, itemData) {

				if(isDefined(itemData.type) && itemData.type=='divider') {
					dropdownMenuElem.append('<li role="separator" class="divider"></li>');
					return;
				}

				// -- Create 'dropDownMenu' item -- //
				itemData.fieldId = this.fieldData.fieldId+'_item_'+itemIndex;
				itemData.category = 'dropDownItem';
				itemData.parentElem = dropdownMenuElem;

				if(isUndefined(!itemData.name) || !itemData.name) itemData.name = this.fieldData.name+'_item'+itemIndex;
				if(isUndefined(!itemData.scope) || !itemData.scope) itemData.scope = this.fieldData.scope;
				if(isUndefined(!itemData.iconOnsetElem) || itemData.iconOnsetElem == null) itemData.iconOnsetElem = true;

				new FormFieldCreator(itemData);
			}, this));
		}
	};

	// ----------------------------------------------
	this.createDropDownItemField = function() {

		this.fieldData.parentElem.append(
			'<li>'
				+'<a '
					+'id="'+this.fieldData.fieldId+'" '
					+'href="#" '
				+'>'
					+this.fieldData.text
				+'</a>'
			+'</li>'
		);

		this.fieldElem = $('#'+this.fieldData.fieldId);

		this.setFieldBasicDefinitions();
		this.setFieldIcon();
		this.setFieldEvents();
	};

	// ----------------------------------------------
	this.createTextEditorField = function() {
		this.fieldData.parentElem.append(
			'<div '
				+'id="'+this.fieldData.fieldId+'" '
			+'></div>'
		);

		$('#'+this.fieldData.fieldId).summernote({
			lang: 'he-IL',
			toolbar: [
				['style', ['bold', 'italic', 'underline', 'clear']],
				['fontsize', ['fontsize','color']],
				['para', ['paragraph']],
				['height', ['height']],
				['insert', ['picture', 'link', 'video', 'table','hr']]
			],
			height: 130,
			minHeight: 130,
			maxHeight: 130,
			focus: true,
			disableResizeEditor: true
		});

		this.fieldElem = $('#'+this.fieldData.fieldId);

		this.setFieldBasicDefinitions();

		setFormFieldValue(this.fieldElem, this.fieldData.value);
		
		this.fieldElem.attr({
			'placeholder':this.fieldData.placeholder,
			'forceValued':(this.fieldData.value ? true : false),
			'changed':false,
			'focused':false
		});

		this.setFieldEvents();
	};

	// ----------------------------------------------
	this.createDateField = function() {
		var dateTimePickerId = 'dateTimePicker-'+this.fieldData.fieldId;

		this.fieldData.parentElem.append(
			'<div '
				+'id="'+dateTimePickerId+'" '
				+'class="input-group date datetimepicker"'
			+'>'
				+'<input '
					+'id="'+this.fieldData.fieldId+'" '
					+'dateTimePickerId="'+dateTimePickerId+'"'
					+'type="text" '
					+'class="form-control" '
				+'/>'
				+'<span class="input-group-addon datetimepicker-icon">'
					+'<span class="glyphicon glyphicon-calendar"></span>'
				+'</span>'
			+'</div>'
		);

		var dateTimePickerDefaultOptions = getDateTimePickerDefaultOptions();
		$('#'+dateTimePickerId).datetimepicker(dateTimePickerDefaultOptions);

		this.fieldElem = $('#'+this.fieldData.fieldId);

		this.setFieldBasicDefinitions();

		this.fieldElem.attr({
			'type':this.fieldData.type,
			'placeholder':this.fieldData.placeholder,
			'value':this.fieldData.value,
			'forceValued':(this.fieldData.value ? true : false),
			'onlyNumbers':this.fieldData.onlyNumbers,
			'maxlength':this.fieldData.maxlength,
			'changed':false,
			'focused':false
		});

		this.setFieldEvents();

		$('#'+this.fieldData.fieldId).change(function (event) {
			this.attr('changed',true);
		});
	};

	// ----------------------------------------------
	this.createInputField = function() {
		this.fieldData.parentElem.append(
			'<input '
				+'id="'+this.fieldData.fieldId+'" '
				+'class="form-control" '
			+'>'
		);

		this.fieldElem = $('#'+this.fieldData.fieldId);

		this.setFieldBasicDefinitions();

		this.fieldElem.attr({
			'type':this.fieldData.type,
			'placeholder':this.fieldData.placeholder,
			'value':this.fieldData.value,
			'forceValued':(this.fieldData.value ? true : false),
			'onlyNumbers':this.fieldData.onlyNumbers,
			'maxlength':this.fieldData.maxlength,
			'changed':false,
			'focused':false
		});

		this.setFieldEvents();
	};

	// ----------------------------------------------
	this.createTextareaField = function() {
		this.fieldData.parentElem.append(
			'<textarea '
				+'id="'+this.fieldData.fieldId+'" '
				+'class="form-control" '
			+'>'
		);

		this.fieldElem = $('#'+this.fieldData.fieldId);
		
		if(!this.fieldData.css) this.fieldData.css = {};
		if(isUndefined(this.fieldData.css.resize)) this.fieldData.css.resize = 'none';

		this.setFieldBasicDefinitions();

		this.fieldElem.attr({
			'type':this.fieldData.type,
			'placeholder':this.fieldData.placeholder,
			'value':this.fieldData.value,
			'forceValued':(this.fieldData.value ? true : false),
			'onlyNumbers':this.fieldData.onlyNumbers,
			'maxlength':this.fieldData.maxlength,
			'changed':false,
			'focused':false
		});

		this.setFieldEvents();
	}

	// ----------------------------------------------
	this.createSelectBoxField = function() {
		var forceSelected = false;

		this.fieldData.parentElem.append(
			'<select '
				+'id="'+this.fieldData.fieldId+'" '
				+'class="form-control" '
			+'></select>'
		);

		this.fieldElem = $('#'+this.fieldData.fieldId);

		this.setFieldBasicDefinitions();

		this.fieldElem.attr({
			'changed':false
		});

		// -- Set 'selectBox' store -- //
		if(this.fieldData.store && isArray(this.fieldData.store) && this.fieldData.store.length) {

			$.each(this.fieldData.store, $.proxy(function(optionIndex, optionData) {
				if(isUndefined(optionData.value) || isUndefined(optionData.text)) return;

				var forceSelectedHtml = '';

				if(this.fieldData.value == optionData.value) {
					forceSelectedHtml = ' selected';
					forceSelected = true;
				}
				
				this.fieldElem.append(
					'<option value="'+optionData.value+'"'+forceSelectedHtml+'>'
						+optionData.text
					+'</option>'
				);
			}, this));
		}

		this.fieldElem.attr('forceValued', forceSelected);
		
		this.setFieldEvents();
	};

	// ----------------------------------------------
	this.createButtonField = function() {
		var btnCls = '';

		if(
			isDefined(this.fieldData.btnCls) && 
			inArray(['primary','success','danger','warning','info'], fieldData.btnCls)
		) {
			btnCls = 'btn-'+this.fieldData.btnCls;
		}

		this.fieldData.parentElem.append(
			'<button '
				+'id="'+this.fieldData.fieldId+'" '
				+'class="btn '+btnCls+' form-control" '
			+'>'
				+this.fieldData.text
			+'</button>'
		);

		this.fieldElem = $('#'+this.fieldData.fieldId);

		this.setFieldBasicDefinitions();
		this.setFieldIcon();
		this.setFieldEvents();
	};

	// ----------------------------------------------
	this.setFieldBasicDefinitions = function() {
		// -- Set general fields definitions -- //
		this.fieldElem.attr({
				'category':this.fieldData.category,
				'name':this.fieldData.name,
				'fieldDescription':this.fieldData.fieldDescription
			})
			.prop({
				"readonly":this.fieldData.readOnly
			});
		
		if(this.fieldData.cls) this.fieldElem.addClass(this.fieldData.cls);
		setFormFieldDisabled(this.fieldElem, this.fieldData.disabled);
		setFormFieldAllowEmpty(this.fieldElem, this.fieldData.allowEmpty);
		setFormFieldIsIdNumber(this.fieldElem, this.fieldData.isIdNumber);
		setFormFieldVisible(this.fieldElem, !this.fieldData.hidden);
		if(this.fieldData.css) this.fieldElem.css(this.fieldData.css);

		if(isDefined(this.fieldData.moreAttr) && this.fieldData.moreAttr && isObject(this.fieldData.moreAttr)){
			this.fieldElem.attr(this.fieldData.moreAttr);
		}
	};

	// ----------------------------------------------
	this.setFieldEvents = function() {
		var funcGlobalFieldElem = this.fieldElem;
		var onChangeElem = this.fieldElem;
		var onChangeEventText = 'change';
		var onFocusEventText = 'focus';
		var dateTimePickerId = this.fieldElem.attr('dateTimePickerId');
		var currFieldDataValue = this.fieldData.value;

		if(this.fieldData.category=='date' && dateTimePickerId) {
			onChangeElem = $('#'+dateTimePickerId);
			onChangeEventText = 'dp.change';
		}
		else if(this.fieldData.category=='textEditor') {
			onChangeEventText = 'summernote.change';
			onFocusEventText = 'summernote.focus';
		}

		// -- Set default 'input' event -- //
		setEventToElem({
			eventName:'input',
			elem:this.fieldElem,
			function:function() {
				var fieldValue = $.trim(getFormFieldValue($(this)));
				onlyNumbers = booleanCheckAdv($(this).attr('onlyNumbers'));

				if(onlyNumbers) {
					setFormFieldValue($(this), getNumberFromStr(fieldValue));
				}
			}
		});
		
		// -- Set default 'focus' event -- //
		setEventToElem({
			eventName:onFocusEventText, 
			elem:this.fieldElem, 
			function:function() {
				// ----- Set 'focused' attribute ----- //
				$(this).attr("focused",true);
			}
		});

		// -- Set default 'change' event -- //
		setEventToElem({
			eventName:onChangeEventText,
			elem:onChangeElem,
			function:function() {
				var fieldValue = $.trim(getFormFieldValue(funcGlobalFieldElem));
				if(fieldValue == currFieldDataValue) return;

				// ----- Set 'changed' attribute ----- //
				funcGlobalFieldElem.attr("changed",true);
			}
		});

		if(isDefined(this.fieldData.events)) {
			$.each(this.fieldData.events, $.proxy(function (eventName, funcContent) {
				var eventElem = this.fieldElem;

				if(eventName=='change') {
					eventElem = onChangeElem;
					eventName = onChangeEventText;
				}
				else if(eventName=='focus') {
					eventName = onFocusEventText;
				}

				setEventToElem({
					eventName:eventName,
					elem:eventElem,
					function:funcContent,
					scope:this.fieldData.scope
				});
			},this));
		}
	};

	// ----------------------------------------------
	this.setFieldIcon = function() {
		if(isDefined(this.fieldData.iconCls) && this.fieldData.iconCls && isString(this.fieldData.iconCls)) {
			var funName = (this.fieldData.iconOnsetElem ? 'prepend' : 'append');
			this.fieldElem[funName]('<i class="'+this.fieldData.iconCls+'"></i>');
		}
	};

	this.createFieldByCategory();

	return this.fieldElem;
};

// ----------------------------------------------
function generateSelectBoxStore(properties) {
	if(isUndefined(properties)) return false;

	var data = setDefaultWhenUndefined(properties.data, null, 'checkIfEmpty');
	var valueField = setDefaultWhenUndefined(properties.valueField, null, 'checkIfEmpty');
	var textField = setDefaultWhenUndefined(properties.textField, null, 'checkIfEmpty');
	var emptyRow = setDefaultWhenUndefined(properties.emptyRow, true);
	var emptyRowText = setDefaultWhenUndefined(properties.emptyRowText, '-- בחר --');

	if(!data || !valueField || !textField) return false;
	
	var store = [];

	if(emptyRow) store.push({value:'', text:emptyRowText});

	$.each(data, function(i, r) {
		if(isUndefined(r[valueField]) || isUndefined(r[textField])) return;
		store.push({value:r[valueField], text:r[textField]});
	});
	
	return store;
};

// ----------------------------------------------
function fillFormObject(formObj, fillData) {
	if(isUndefined(formObj) || !formObj) return false;
	if(isUndefined(fillData) || !fillData || !isObject(fillData) || !objLength(fillData)) return formObj;

	$.each(formObj, function( obKey, obRow ) {
		if(isDefined(obRow.fields)) {
			$.each(obRow.fields, function( fKey, fRow ) {				
				if(isUndefined(fRow.name) || isUndefined(fillData[fRow.name])) return;
				formObj[obKey].fields[fKey].value = fillData[fRow.name];
			});
		}
	});

	return formObj;
};

// ----------------------------------------------
function getFormParamsWithValidate(formParentElem) {
	if(isUndefined(formParentElem) || !formParentElem) return false;

	formParams = false;

	$.each(formParentElem.find('.form-field-e'), function( key, el ) {
		var value = $.trim(getFormFieldValue($(this)));
		var allowEmpty = booleanCheckAdv($(this).attr('allowEmpty'));
		var isIdNumber = booleanCheckAdv($(this).attr('isIdNumber'));
		var name = $(this).attr('name');
		var fieldDescription = $(this).attr('fieldDescription');

		if(isUndefined(fieldDescription) || !fieldDescription) fieldDescription = '';
		if(!fieldDescription) fieldDescription = name;

		if(!allowEmpty && !value) {
			openMsgModal({
				msgText:"יש להזין את כל השדות, המסומנים בכוכב."+"<br>"+fieldDescription+" הוא שדה חובה.", 
				msgType:'error'
			});
			
			formParams = false;
			return false;
		}
		
		if(isIdNumber && value && !idNumberValidation(value)) {
			openMsgModal({
				msgText:"מספר תעודת הזהות אינו תקין.", 
				msgType:'error'
			});

			formParams = false;
			return false;
		}

		if(formParams==false) formParams = {};		
		formParams[name] = value;
	});

	return formParams;
};

// ----------------------------------------------
function checkIfFormChanged(formParentElem) {	
	if(isUndefined(formParentElem) || !formParentElem) return false;

	var formChanged = false;

	$.each(formParentElem.find('.form-field-e'), function( k, el ) {								
		if(booleanCheckAdv($(this).attr("changed"))) {
			formChanged = true;
			return false;
		}
	});

	return formChanged;
};

// ----------------------------------------------
function getFormFieldValue(fieldElement) {
	if(isUndefined(fieldElement) || !fieldElement || !fieldElement.length) return '';

	var fieldCategory = fieldElement.attr('category');
	
	if(fieldCategory == 'textEditor') return fieldElement.summernote('code');
	return fieldElement.val();
};

// ----------------------------------------------
function setFormFieldValue(fieldElement, value) {
	if(isUndefined(fieldElement) || !fieldElement || !fieldElement.length) return false;

	var fieldCategory = fieldElement.attr('category');

	if(fieldCategory == 'textEditor') fieldElement.summernote('code', value);
	return fieldElement.val(value);
};

// ----------------------------------------------
function setFormFieldDisabled(fieldElement, disabled) {
	if(isUndefined(disabled) || isUndefined(fieldElement) || !fieldElement || !fieldElement.length) return false;

	var fieldCategory = fieldElement.attr('category');
	var fieldName = fieldElement.attr('name');

	if(fieldCategory == 'dropDownMenu') {
		fieldElement = fieldElement.find('[name="'+fieldName+'_btn"]');
		if(!fieldElement || !fieldElement.length) return false;
	}
	
	fieldElement.prop("disabled", disabled);

	return true;
};

// ----------------------------------------------
var setFormFieldVisible = function(fieldElement, visible) {
	if(isUndefined(fieldElement) || !objLength(fieldElement)) {
		openMsgModal({msgText:'שגיאת מערכת! [setFormFieldVisible-fieldElement]', msgType:'error'});
		return false;
	}
	
	var fieldCategory = fieldElement.attr('category');
	var fieldName = fieldElement.attr('name');

	if(fieldCategory == 'dropDownMenu') {
		fieldElement = fieldElement.find('[name="'+fieldName+'_btn"]');
		if(!fieldElement || !fieldElement.length) return false;
	}
	else if(fieldCategory == 'textEditor') return false;

	if(isDefined(visible) && !visible) fieldElement.hide();
	else fieldElement.show();
};

// ----------------------------------------------
function setFormFieldAllowEmpty(fieldElement, allowEmpty) {	
	if(isUndefined(allowEmpty) || isUndefined(fieldElement) || !fieldElement) return false;

	var fieldId = fieldElement.attr('id');
	if(!fieldId) return false;

	fieldElement.attr('allowEmpty', (allowEmpty?true:false));

	var labelElem = $('label[for="'+fieldId+'"] span');

	if(labelElem && labelElem.length) {
		if(allowEmpty) labelElem.hide();
		else labelElem.show();
	}
	
	return true;
};

// ----------------------------------------------
function setFormFieldIsIdNumber(fieldElement, isIdNumber) {
	if(isUndefined(isIdNumber) || isUndefined(fieldElement) || !fieldElement) return false;

	var fieldId = fieldElement.attr('id');
	if(!fieldId) return false;

	fieldElement.attr('isIdNumber', (isIdNumber?true:false));
	
	return true;
};
