// ----------------------------------------------
function ModalWindow(properties) {
	if(isUndefined(properties)) return false;

	this.modalElem = 
		this.modalTitleElem = 
		this.modalButtonsElem = 
		this.buttonsElem = 
		this.callbackFunc = 
		this.modalEvents = 
		this.beforeLoadEvent = 
		this.afterLoadEvent = 
		this.onCloseEvent = 
		this.buttonsParams = 
		this.bodyParams = 
		this.modalBodyElem = null;

	this.elementClassType = "modal";
	this.modalUniqueId = "modal_win"+generateUniqueId();

	this.autoShow = booleanCheckAdv(setDefaultWhenUndefined(properties.autoShow, false));
	this.showCloseButton = booleanCheckAdv(setDefaultWhenUndefined(properties.showCloseButton, true));
	this.buttonsParams = setDefaultWhenUndefined(properties.buttonsParams, null, 'checkIfEmpty');
	this.bodyParams = setDefaultWhenUndefined(properties.bodyParams, null, 'checkIfEmpty');
	this.mainTitleParams = setDefaultWhenUndefined(properties.mainTitleParams, null, 'checkIfEmpty');

	if(isDefined(properties.modalId) && properties.modalId) this.modalUniqueId = properties.modalId;
	if(isDefined(properties.callbackFunc) && properties.callbackFunc) this.callbackFunc = properties.callbackFunc;
	
	if(!this.mainTitleParams) this.mainTitleParams = {text:"חלון", css:{"background-color":"#00B8C0"}};
	if(isUndefined(this.mainTitleParams.text)) this.mainTitleParams.text = "חלון";
	if(isUndefined(this.mainTitleParams.css)) this.mainTitleParams.css = {"background-color":"#00B8C0"};

	if(isDefined(properties.events) && objLength(properties.events)) {
		this.modalEvents = properties.events;

		if(isDefined(this.modalEvents.beforeLoad) && isFunction(this.modalEvents.beforeLoad)) {
			this.beforeLoadEvent = this.modalEvents.beforeLoad;
		}

		if(isDefined(this.modalEvents.afterLoad) && isFunction(this.modalEvents.afterLoad)) {
			this.afterLoadEvent = this.modalEvents.afterLoad;
		}

		if(isDefined(this.modalEvents.onClose) && isFunction(this.modalEvents.onClose)) {
			this.onCloseEvent = this.modalEvents.onClose;
		}
	}

	// ----------------------------------------------
	this.createModal = function() {
		if(this.beforeLoadEvent) $.proxy(this.beforeLoadEvent(),this);

		this.createBase();
		this.setCloseButton();
		this.setMainTitle();
		this.setBodyHtml();
		this.setButtons();

		if(this.afterLoadEvent) $.proxy(this.afterLoadEvent(),this);
		if(this.autoShow) this.showModal();

		$(document).keyup($.proxy(function(e) {
			// enter
			if (e.keyCode === 13) {
				/*
				this.closeModal();
				openMsgModal({
					msgType:'error',
					msgText:
						'לא ניתן להשלים את הפעולה בלחיצה על כפותר ה-ENTER,</br>'
						+'אפשרות זו חסומה בשל אילוצי אבטחה.</br>'
						+'יש להשלים את הפעולה באופן ידני.'
				});
				*/
			}
			if (e.keyCode === 27) this.closeModal();   // esc click
		},this));
	};

	// ----------------------------------------------
	this.createBase = function() {
		$('body').append(
			'<div name="modal_window" id="'+this.modalUniqueId+'" class="modal">'
				+'<div class="modal-content">'
					+'<div class="close">&times;</div>'
					+'<h2 id="'+this.modalUniqueId+'_title" class="modal-title"></h2>'
					+'<div id="'+this.modalUniqueId+'_body" class="modal-body"></div>'
				+'</div>'
			+'</div>'
		);

		this.modalElem = $("#"+this.modalUniqueId);
		this.modalTitleElem = $("#"+this.modalUniqueId+'_title');
		this.modalBodyElem = $("#"+this.modalUniqueId+'_body');
		this.modalButtonsElem = $("#"+this.modalUniqueId+'_buttons');		
	};

	// ----------------------------------------------
	this.setMainTitle = function() {
		if(!this.mainTitleParams) return false;
		this.modalTitleElem.html(this.mainTitleParams.text).css(this.mainTitleParams.css);
	}

	// ----------------------------------------------
	this.setButtons = function() {
		if(!this.buttonsParams) return false;
		
		this.modalBodyElem.append('<div id="'+this.modalUniqueId+'_buttons" class="modal-buttons"></div>');
		this.buttonsElem = $("#"+this.modalUniqueId+'_buttons');

		$.each(this.buttonsParams, $.proxy(function (index, buttonData) {
			if(isUndefined(buttonData.text)) return false;

			var btnNameHtml = iconClsHtml = '';
			
			var buttonRowId = this.modalUniqueId+'_button'+index;
			var btnName = setDefaultWhenUndefined(buttonData.name, buttonRowId);
			var iconCls = setDefaultWhenUndefined(buttonData.iconCls, '');
			buttonData.disabled = setDefaultWhenUndefined(buttonData.disabled, false);
			buttonData.hidden = setDefaultWhenUndefined(buttonData.hidden, false);

			if(iconCls) iconClsHtml = '<i class="btn-fa-icon-cls '+iconCls+'"></i>';
			btnNameHtml = 'name="'+btnName+'"';

			this.buttonsElem.append(
				'<button '
					+'class="btn modal-win-button" '
					+'id="'+buttonRowId+'" '
					+btnNameHtml+' '
				+'>'
					+iconClsHtml
					+buttonData.text
				+'</button>'
			);

			if(isDefined(buttonData.css)) $("#"+buttonRowId).css(buttonData.css);
			if(isDefined(buttonData.styleClasses)) $("#"+buttonRowId).addClass(buttonData.styleClasses);

			$("#"+buttonRowId).prop({
				"disabled":buttonData.disabled
			});

			if(buttonData.hidden) $("#"+buttonRowId).hide();
			else $("#"+buttonRowId).show();

			if(isDefined(buttonData.events)) {
				$.each(buttonData.events, $.proxy(function (eventName, funcContent) {
					$("#"+buttonRowId).on(eventName, $.proxy(funcContent,this));
				},this));
			}
		},this));
	}

	// ----------------------------------------------
	this.setBodyHtml = function() {
		if(!this.bodyParams) return false;
		this.bodyParams.formClass = null;

		if(isDefined(this.bodyParams.html)) this.modalBodyElem.append(this.bodyParams.html);
		
		if(isDefined(this.bodyParams.formRowsObj)) {
			this.bodyParams.formClass = createFormElement({
				formRows:this.bodyParams.formRowsObj, 
				parentElemId:this.modalBodyElem.attr('id')
			});
		};
	}

	// ----------------------------------------------
	this.setCloseButton = function() {
		if(this.showCloseButton) {
			this.modalElem.find('.close').click($.proxy(function() {
				this.closeModal();
			},this)).show();
		}
		else this.modalElem.find('.close').remove();
	}

	// ----------------------------------------------
	this.findButtonByName = function(btnName) {
		if(isUndefined(btnName) || !btnName) return false;

		btnElem = this.buttonsElem.find('button[name="'+btnName+'"]');
		
		if(btnElem.length) return btnElem;
		else return false;
	}

	// ----------------------------------------------
	this.closeModal = function() {
		this.modalElem.remove();
		if(this.callbackFunc) this.callbackFunc();
		if(this.onCloseEvent) $.proxy(this.onCloseEvent(),this);
	}

	// ----------------------------------------------
	this.showModal = function() {
		this.modalElem.show();
	}

	this.createModal();
};

// ----------------------------------------------
function openMsgModal(properties) {
	var modalId = 'dialog_msg_modal';
	$('#'+modalId).remove();
	
	if(isUndefined(properties)) {
		alert("שגיאה בעת פתיחת חלון ההודעה.[properties - openMsgModal]");
		return;
	}

	// -- Define default params values -- //
	properties.msgText = setDefaultWhenUndefined(properties.msgText, "אופס! משהו השתבש");
	properties.okBtnText = setDefaultWhenUndefined(properties.okBtnText, "אישור");
	properties.okBtnIconCls = setDefaultWhenUndefined(properties.okBtnIconCls, "fas fa-check-circle");
	properties.callbackFunc = setDefaultWhenUndefined(properties.callbackFunc, null);
	properties.title = setDefaultWhenUndefined(properties.title, '');
	properties.modalColor = setDefaultWhenUndefined(properties.modalColor, '');
	properties.msgType = setDefaultWhenUndefined(properties.msgType, '');

	if(!inArray(['info','error','warning'], properties.msgType)) properties.msgType = 'info';

	// -- Define modal styles -- //
	switch(properties.msgType) {
		case 'info':
			if(!properties.title) properties.title = 'הודעה';
			if(!properties.modalColor) properties.modalColor = '#00B8C0';
			break;
		case 'error':
			if(!properties.title) properties.title = 'שגיאה';
			if(!properties.modalColor) properties.modalColor = '#EF5350';
			break;
		case 'warning':
			if(!properties.title) properties.title = 'התראה';
			if(!properties.modalColor) properties.modalColor = '#FF8F00';
			break;
		break;
	}

	// -- Create modal -- //
	DialogMsgModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:properties.title, 
			css:{"background-color":properties.modalColor}
		},
		bodyParams:{
			html:'<p class="modal-text" style="min-height:100px;">'+properties.msgText+'</p>'
		},
		buttonsParams:[
			{text:properties.okBtnText, iconCls:properties.okBtnIconCls,
				events:{
					click:function(){
						this.closeModal();
					}
				}
			}
		],
		events:{
			onClose:properties.callbackFunc
		}
	}).showModal();
};

// ----------------------------------------------
function openYesNoQuestionModal(properties) {
	var modalId = 'dialog_yes_no_question_modal';
	closeOpenYesNoQuestionModal();

	if(isUndefined(properties)) properties = {};

	properties.callbackFunc = setDefaultWhenUndefined(properties.callbackFunc, null);
	properties.title = setDefaultWhenUndefined(properties.title, 'התראה');
	properties.modalColor = setDefaultWhenUndefined(properties.modalColor, '#FF8F00');
	properties.yesBtnText = setDefaultWhenUndefined(properties.yesBtnText, 'כן');
	properties.yesBtnIconCls = setDefaultWhenUndefined(properties.yesBtnIconCls, 'far fa-thumbs-up');
	properties.yesBtnOnClick = setDefaultWhenUndefined(properties.yesBtnOnClick, null);
	properties.noBtnText = setDefaultWhenUndefined(properties.noBtnText, 'לא');
	properties.noBtnIconCls = setDefaultWhenUndefined(properties.noBtnIconCls, 'far fa-thumbs-down');
	properties.noBtnOnClick = setDefaultWhenUndefined(properties.noBtnOnClick, null);
	properties.msgText = setDefaultWhenUndefined(properties.msgText, "");

	if(!properties.noBtnOnClick) properties.noBtnOnClick = function(){this.closeModal();}

	// -- Create modal -- //
	DialogMsgModal = new ModalWindow({
		modalId:modalId,
		mainTitleParams:{
			text:properties.title, 
			css:{"background-color":properties.modalColor}
		},
		bodyParams:{
			html:'<p class="modal-text" style="min-height:100px;">'+properties.msgText+'</p>'
		},
		buttonsParams:[
			{text:properties.yesBtnText, iconCls:properties.yesBtnIconCls, css:{'margin-left':'6px'},
				events:{
					click:properties.yesBtnOnClick
				}
			},
			{text:properties.noBtnText, iconCls:properties.noBtnIconCls, 
				events:{
					click:properties.noBtnOnClick
				}
			}
		],
		events:{
			onClose:properties.callbackFunc
		}
	}).showModal();
};

// ----------------------------------------------
function closeOpenYesNoQuestionModal() {
	$('#dialog_yes_no_question_modal').remove();
}