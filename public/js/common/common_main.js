$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	data: {
		_token: $('meta[name="csrf-token"]').attr('content')
	}	
});

var SVARS = SharedMessagesList = {};

// ----------------------------------------------
function showPreloader(blockElementFindKey, opacity) {
	if(isUndefined(blockElementFindKey) || !blockElementFindKey) blockElementFindKey='#main_body_content';
	if(isUndefined(opacity) || !opacity) opacity="0.15";

	$("#preloader").remove();

	$('body').append(
		'<div id="preloader" class="preloader" blockElementFindKey="'+blockElementFindKey+'">'
			+'<div class="loader-simplistic"></div>'
		+'</div>'
	);

	$(blockElementFindKey).css({
		"opacity":opacity,
		"pointer-events":"none"
	});
	$("#preloader").show();
};

// ----------------------------------------------
function hidePreloader() {
	$($("#preloader").attr('blockElementFindKey')).css({
		"opacity":"1",
		"pointer-events":"unset"
	});
	$("#preloader").remove();
};

// ----------------------------------------------
var getSvars = function() {
	if(isUndefined(SVARS) || !SVARS || !objLength(SVARS)) return null;
	return SVARS;
};

// ----------------------------------------------
var getUserData = function() {
	if(!getSvars() || isUndefined(SVARS.user_data) || !SVARS.user_data || !objLength(SVARS.user_data)) return null;
	return SVARS.user_data;
};

// ----------------------------------------------
var getUsersTypes = function(PreventPermissionCheck) {
	if(!getSvars() || isUndefined(SVARS.users_types) || !SVARS.users_types || !objLength(SVARS.users_types)) return null;

	if(isDefined(PreventPermissionCheck) && PreventPermissionCheck) return SVARS.users_types;

	var usersTypes = {};

	if(isBasicUser()) return null;

	$.each(SVARS.users_types, function (pKey, tContent) {		
		if(tContent && objLength(tContent)) {
			usersTypes[pKey] = {};

			$.each(tContent, function (k, row) {

				if(
					(isBranchUser() && !inArray(['branch_user','basic_user'], row.name_eng)) ||
					(isManagerUser() && !inArray(['manager_user','branch_user','basic_user'], row.name_eng))
				) return; 

				usersTypes[pKey][k] = row;
			});
		}
	});
	
	return usersTypes;
};

// ----------------------------------------------
var getGymsList = function() {
	if(!getSvars() || isUndefined(SVARS.gyms_list) || !SVARS.gyms_list || !objLength(SVARS.gyms_list)) return null;
	return SVARS.gyms_list;
};

// ----------------------------------------------
var getGymsBranchesList = function() {
	if(
		!getSvars() || 
		isUndefined(SVARS.gyms_branches_list) || 
		!SVARS.gyms_branches_list || 
		!objLength(SVARS.gyms_branches_list)
	) return null;

	return SVARS.gyms_branches_list;
};

// ----------------------------------------------
var getModules = function() {
	if(!getSvars() || isUndefined(SVARS.modules) || !SVARS.modules || !objLength(SVARS.modules)) return null;
	return SVARS.modules;
};

// ----------------------------------------------
var isAdmin = function() {
	var userData = getUserData();
	return (userData && userData.type_name == "super_user");
};

// ----------------------------------------------
var isManagerUser = function() {
	var userData = getUserData();
	return (userData && userData.type_name == "manager_user");
};

// ----------------------------------------------
var isBranchUser = function() {
	var userData = getUserData();
	return (userData && userData.type_name == "branch_user");
};

// ----------------------------------------------
var isBasicUser = function() {
	var userData = getUserData();
	return (userData && userData.type_name == "basic_user");
};

// ----------------------------------------------
var getElementAttributes = function(elem) {
	var attributesStore = null;
	var attributesObj = {};

	if(isDefined(elem) && isDefined(elem[0]) && isDefined(elem[0].attributes)) {
		attributesStore = elem[0].attributes;
		if(!attributesStore) return false;

		$.each(attributesStore, function(name, attr) {
			attributesObj[attr.name] = attr.value;
		});

		if(objLength(attributesObj)) return attributesObj;
		return false;
	}
	else return false;
};

// ----------------------------------------------
var getNumberFromStr = function(str) {
	if(isUndefined(str) || !str) return '';
	
	var string = str.match(/\d/g);
	if(!string) return '';

	return string.join("");
};

// ----------------------------------------------
var objLength = function(obj) {
	if(isUndefined(obj) || !obj) return 0;
	return Object.keys(obj).length;
};

// ----------------------------------------------
var booleanCheckAdv = function(input) {
	if(isUndefined(input) || !input) return false;
	
	if(input===true || input==="true" || (input!=="false" && input!=="0" && input!==0 && input!=="null")) return true;
	else return false;
};

// ----------------------------------------------
function setDefaultWhenUndefined(input, defaultVal, checkIfEmpty) {
	if(isUndefined(checkIfEmpty)) checkIfEmpty = false;

	condition = isUndefined(input);
	if(!condition && checkIfEmpty && !input) condition = true;

	if(condition) return defaultVal;
	return input;
};

// ----------------------------------------------
var generateUniqueId = function() {
	return 'id-' + (new Date()).getTime() + '-' + Math.random().toString(36).substr(2, 16);
};

// ----------------------------------------------
function isNumber(input) {
	if(isUndefined(input)) return false;

	return !isNaN(input);
};

// ----------------------------------------------
function isObject(input) {
	if(isUndefined(input)) return false;

	return ($.type(input) === 'object');
};

// ----------------------------------------------
function isArray(input) {
	if(isUndefined(input)) return false;

	return ($.type(input) === 'array');
};

// ----------------------------------------------
function isString(input) {
	if(isUndefined(input)) return false;

	return ($.type(input) === 'string');
};

// ----------------------------------------------
function inArray(arrayToCheck, value) {
	return (arrayToCheck.indexOf(value) != -1);
};

// ----------------------------------------------
function isFunction(functionToCheck) {
	return (functionToCheck && {}.toString.call(functionToCheck) === '[object Function]');
}

// ----------------------------------------------
function isUndefined(input) {
	return (typeof input === 'undefined');
};

// ----------------------------------------------
function isDefined(input) {
	return (typeof input !== 'undefined');
};

// ----------------------------------------------
// Function to convert rgb color to hex format
function rgb2hex(rgb) {
	if(isUndefined(rgb) || !rgb) return '';

	rgbMatched = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	if(!rgbMatched) return rgb;

	return "#" + hex(rgbMatched[1]) + hex(rgbMatched[2]) + hex(rgbMatched[3]);
};

function hex(x) {
	return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
};