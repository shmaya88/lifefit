// ----------------------------------------------
function GridTable(properties) {
	if(isUndefined(properties)) {
		openMsgModal({msgText:"נא להגדיר נתוני טבלה", msgType:'error'});
		return false;
	}	

	var errorMsg = '';

	this.gridElem = 
		this.gridHeadElem = 
		this.gridBodyElem =
		this.searchFieldElem = 
		this.gridEvents = 
		this.beforeLoadEvent = 
		this.afterLoadEvent = 
		this.storeLoadEvent = 
		this.selectionChangeEvent = 
		this.topToolBarElem = 
		this.bottomToolBarElem = 
		this.topToolBarSearchParams = 
		this.topToolBarFieldsParams = null;

	this.elementClassType = "grid";
	this.gridUniqueId = "grid_table"+generateUniqueId();
	this.emptyStoreMsg = "לא אותרו נתונים";
	
	// -- Define default params values -- //
	this.columns = setDefaultWhenUndefined(properties.columns, null, 'checkIfEmpty');
	this.store = setDefaultWhenUndefined(properties.store, null, 'checkIfEmpty');
	this.topToolBarParams = setDefaultWhenUndefined(properties.topToolBar, null, 'checkIfEmpty');

	this.parentElemId = setDefaultWhenUndefined(properties.parentElemId, null);

	this.hiddenGrid = booleanCheckAdv(setDefaultWhenUndefined(properties.hiddenGrid, false));
	this.autoLoadGrid = booleanCheckAdv(setDefaultWhenUndefined(properties.autoLoadGrid, true));
	this.numerationColumnShow = booleanCheckAdv(setDefaultWhenUndefined(properties.numerationColumnShow, true));

	if(this.topToolBarParams) {
		if(isDefined(this.topToolBarParams.fields)) this.topToolBarFieldsParams = this.topToolBarParams.fields;
		if(isDefined(this.topToolBarParams.search)) this.topToolBarSearchParams = this.topToolBarParams.search;	
	}

	if(isDefined(properties.events) && objLength(properties.events)) {
		this.gridEvents = properties.events;

		if(isDefined(this.gridEvents.beforeLoad) && isFunction(this.gridEvents.beforeLoad)) {
			this.beforeLoadEvent = this.gridEvents.beforeLoad;
		}

		if(isDefined(this.gridEvents.afterLoad) && isFunction(this.gridEvents.afterLoad)) {
			this.afterLoadEvent = this.gridEvents.afterLoad;
		}

		if(isDefined(this.gridEvents.onSelectChange) && isFunction(this.gridEvents.onSelectChange)) {
			this.selectionChangeEvent = this.gridEvents.onSelectChange;
		}

		if(isDefined(this.gridEvents.onStoreLoad) && isFunction(this.gridEvents.onStoreLoad)) {
			this.storeLoadEvent = this.gridEvents.onStoreLoad;
		}
	}

	// -- Necessary params checking -- //
	if(!this.columns) errorMsg = "לא הוגדרו עמודות לטבלה";
	else if(!this.store) errorMsg = "לא הוגדרו נתוני השורות";

	if(errorMsg) {
		if(this.parentElemId) errorMsg = errorMsg+" : "+this.parentElemId;

		openMsgModal({msgText:errorMsg, msgType:'error'});
		return false;
	}

	if(this.parentElemId) this.parentElem = $("#"+this.parentElemId);
	if(!this.parentElem) this.parentElem = $("body");


	// ----------------------------------------------
	this.createGrid = function() {
		if(this.beforeLoadEvent) $.proxy(this.beforeLoadEvent(),this);

		this.createGridBase();
		this.loadStore();

		if(this.afterLoadEvent) $.proxy(this.afterLoadEvent(),this);
	};

	// ----------------------------------------------
	this.createGridBase = function() {
		this.parentElem.attr('grid-table',true);

		if(this.topToolBarParams) {
			this.parentElem.append(
				'<div>'
					+'<div class="navbar-form navbar-right top-tool-bar">'
						+'<div class="form-group top-tool-bar-group">'
        				+'</div>'
					+'</div>'
				+'</div>'
			);

			this.topToolBarElem = this.parentElem.find(".top-tool-bar-group");

			this.setTopToolBarSearchField();
			this.setTopToolBarFields();
		}

		// Create table
		this.parentElem.append(
			'<table '
				+'id="'+this.gridUniqueId+'" '
				+'data-toggle="table" '
				+'class="grid-table-base display table table-bordered table-hover" '
			+'>'
				+'<thead '
					+'id="'+this.gridUniqueId+'_head" '
					+'owner-id="'+this.gridUniqueId+'" '					
				+'></thead>'

				+'<tbody '
					+'id="'+this.gridUniqueId+'_body" '
					+'owner-id="'+this.gridUniqueId+'" '					
				+'></tbody>'
			+'</table>'

			+'<div class="bottom-tool-bar">'
			+'</div>'
		);

		this.gridElem = $("#"+this.gridUniqueId);
		this.gridHeadElem = $("#"+this.gridUniqueId+"_head");
		this.gridBodyElem = $("#"+this.gridUniqueId+"_body");
		this.bottomToolBarElem = this.parentElem.find(".bottom-tool-bar");

		$.each(this.gridElem.find("thead,tbody"), function( key, el ) {
			var ownerIdAtrr = $(this).attr("owner-id");
			if(!ownerIdAtrr) {
				$(this).remove();
				return;
			}
		});

		this.createColumns();

		if(this.hiddenGrid) this.gridElem.hide();
	};

	// ----------------------------------------------
	this.loadStore = function(properties) {
		if(!this.store) return false;

		if(isUndefined(properties)) properties = {};
		var searchText = '';

		this.store.data = setDefaultWhenUndefined(this.store.data, []);
		this.store.url = setDefaultWhenUndefined(this.store.url, '');
		this.store.sortBy = setDefaultWhenUndefined(this.store.sortBy, '');
		this.store.sortDirection = setDefaultWhenUndefined(this.store.sortDirection, 'ASC');
		this.store.showPreloaderOnLoad = setDefaultWhenUndefined(this.store.showPreloaderOnLoad, true);
		this.store.dataIndex = setDefaultWhenUndefined(this.store.dataIndex, 'data', 'checkIfEmpty');
		this.store.rowsCountIndex = setDefaultWhenUndefined(this.store.rowsCountIndex, 'rowsCount', 'checkIfEmpty');

		this.store.rowsStart = parseInt(setDefaultWhenUndefined(this.store.rowsStart, 0));
		this.store.loadAttempts = parseInt(setDefaultWhenUndefined(this.store.loadAttempts, 0));
		this.store.rowsLimit = parseInt(setDefaultWhenUndefined(this.store.rowsLimit, 10, 'checkIfEmpty'));
		this.store.rowsCount = parseInt(setDefaultWhenUndefined(this.store.rowsCount, 0, 'checkIfEmpty'));
		this.store.currentPageNum = parseInt(setDefaultWhenUndefined(this.store.currentPageNum, 1, 'checkIfEmpty'));
		this.store.maxPageNum = parseInt(setDefaultWhenUndefined(this.store.maxPageNum, 1, 'checkIfEmpty'));

		this.store.autoLoad = booleanCheckAdv(setDefaultWhenUndefined(this.store.autoLoad, true));

		var resetStoreParams = setDefaultWhenUndefined(properties.resetStoreParams, true);

		if(!this.store.rowsLimit) this.store.rowsLimit = 10;

		if(!this.store.autoLoad) return;

		if(resetStoreParams) this.resetStoreParams();

		if(this.store.url) {
			if(this.store.showPreloaderOnLoad) showPreloader('#'+this.gridUniqueId);
			if(this.searchFieldElem) searchText = this.searchFieldElem.attr("currSearchVal");

			var ajaxData = {
				search_text: searchText,
				rows_start: this.store.rowsStart,
				rows_limit: this.store.rowsLimit,
				data_index: this.store.dataIndex,
				rows_count_index: this.store.rowsCountIndex,
				sort_by: this.store.sortBy,
				sort_direction: this.store.sortDirection,
			};
			
			if(this.topToolBarElem) {
				$.each(this.topToolBarElem.find('[attach-to-store-load="true"]'), function( key, el ) {
					var fieldName = $(this).attr("name");
					var fieldId = $(this).attr("id");

					var axDataKey = (fieldName?fieldName:fieldId);
					if(!axDataKey) return;

					ajaxData[axDataKey] =  $(this).val();
				});
			}			

			$.ajax({
				type:"POST",
				url:this.store.url,
				data: ajaxData,
				success:$.proxy(function (record) {
					this.store.loadAttempts = 0;

					if(record.success) {
						this.store.data = record.data;
						this.store.rowsCount = parseInt(setDefaultWhenUndefined(record[this.store.rowsCountIndex], 0));
					}
					else {
						this.store.data = [];
					}
					
					this.buildGridList();
					if(this.store.showPreloaderOnLoad) hidePreloader();
				},this),
				error:$.proxy(function (record) {
					if(record.status == 500 && this.store.loadAttempts<4) {
						this.store.loadAttempts++;
						this.loadStore();
					}
					else openMsgModal({msgText:record.status+" - "+record.statusText, msgType:'error'});
				},this)
			});


		}
		else if(this.store.data) {
			this.store.rowsCount = this.store.data.length;
			this.buildGridList();
			if(this.store.showPreloaderOnLoad) hidePreloader();
		}
		else {
			this.store.data = [];
			this.resetStoreParams();
			this.buildGridList();
			if(this.store.showPreloaderOnLoad) hidePreloader();
		}
	};

	// ----------------------------------------------
	this.createColumns = function(columnsData) {		
		this.clearGridData();
		if(isDefined(columnsData) && isArray(properties.columns)) this.columns = columnsData;
		this.gridHeadElem.append('<tr row-type="columns"></tr>');

		var gridColumnsRowElem = this.gridHeadElem.find('tr');

		if(this.numerationColumnShow) this.columns.unshift({name:'numeration_column', styleClasses:'numeration_column'});	

		$.each(this.columns, $.proxy(function (index, columnData) {
			var columnText = columnName = styleClasses = '';
			var columnHidden = sortable = false;

			if(isDefined(columnData.name)) columnName = columnData.name;
			if(isDefined(columnData.text)) columnText = columnData.text;
			if(isDefined(columnData.styleClasses)) styleClasses += columnData.styleClasses+' ';
			if(isDefined(columnData.hidden) && booleanCheckAdv(columnData.hidden)) columnHidden = true;
			if(isDefined(columnData.sortable) && booleanCheckAdv(columnData.sortable)) sortable = true;

			// Continue, if exists same name
			if(this.findColumnElemByName(columnName, gridColumnsRowElem)) {
				this.columns[index].name = columnName = columnName+index;
				this.columns[index].hidden = columnHidden = true;
			}

			var columnId = this.gridUniqueId+'_column'+columnName;

			gridColumnsRowElem.append(
				'<th ' 
					+'id="'+columnId+'" '
					+'col-hidden="'+columnHidden+'" '
					+'col-name="'+columnName+'" '
					+'sortable="'+sortable+'" '
					+(styleClasses ? 'class="'+styleClasses+'" ' : '')
				+'>'					
					+columnText
					+'<i icon-name="column-sort-icon"></i>'
				+'</th>'
			);

			if(columnHidden) $("#"+columnId).hide();

			$("#"+columnId).click($.proxy(function (event) {
				var target = $(event.delegateTarget);
				var colName = target.attr('col-name');
				var sortable = target.attr('sortable');

				if(!booleanCheckAdv(sortable)) return;
				
				if(isDefined(this.store) && this.store && isDefined(colName) && colName){
					this.store.sortBy = target.attr('col-name');

					if(isDefined(this.store.sortDirection) && this.store.sortDirection=='ASC') {
						this.store.sortDirection ='DESC';
					}
					else this.store.sortDirection ='ASC';
					
					this.loadStore();
					this.resetColumnsSortableIcons();
				}
			},this));
		},this));

		this.resetColumnsSortableIcons();
	};

	// ----------------------------------------------
	this.resetColumnsSortableIcons = function(storeData) {
		if(!this.gridHeadElem) return false;

		sortBy = '';
		sortDirection = 'ASC';

		if(isDefined(this.store) && this.store){
			if(isDefined(this.store.sortBy) && this.store.sortBy) sortBy = this.store.sortBy
			if(isDefined(this.store.sortDirection) && this.store.sortDirection) sortDirection = this.store.sortDirection
		}

		$.each(this.gridHeadElem.find('th'), $.proxy(function (index, columnElem) {
			columnElem = $(columnElem);
			var columnIconElem = columnElem.find('[icon-name="column-sort-icon"]');
			
			columnIconElem.removeAttr('class');

			var columnAttr = getElementAttributes(columnElem);
			if(!columnAttr || isUndefined(columnAttr['col-name'])) return;

			if(booleanCheckAdv(columnAttr.sortable)) columnIconElem.show();
			else columnIconElem.hide();

			if(columnAttr['col-name']==sortBy && sortDirection=='ASC') columnIconElem.addClass('fas fa-sort-up');
			if(columnAttr['col-name']==sortBy && sortDirection=='DESC') columnIconElem.addClass('fas fa-sort-down');
			else columnIconElem.addClass('fas fa-sort');
		},this));
	};

	// ----------------------------------------------
	this.buildGridList = function(storeData) {
		if(isDefined(storeData)) this.store.data = storeData;
		this.gridBodyElem.empty();

		if(isUndefined(this.store.data) || !this.columns || !this.columns.length) {
			this.setEmptyStoreMsg("נתוני הטבלה אינם תקינים");
			return false;
		}

		for (var rowNum = 1 ; rowNum <= this.store.rowsLimit; rowNum++) {

			rowData = this.store.data[rowNum-1];

			var rowId = this.gridUniqueId+'_row'+rowNum;

			this.gridBodyElem.append(
				'<tr ' 
					+'id="'+rowId+'" '
					+'row-num="'+rowNum+'" '
					+'elem-type="tableRow" '
					+'containing-row="'+isDefined(rowData)+'" '
				+'></tr>'
			);

			for (var columnIndex = 0 ; columnIndex < this.columns.length; columnIndex++) {
				columnData = this.columns[columnIndex];

				var columnName = fieldValue = styleClasses = '';
				var columnHidden = false;

				if(isDefined(columnData.name)) columnName = columnData.name;
				if(isDefined(columnData.hidden) && booleanCheckAdv(columnData.hidden)) columnHidden = true;
				if(isDefined(columnData.styleClasses)) styleClasses += columnData.styleClasses+' ';

				if(isDefined(rowData)) {
					if(this.numerationColumnShow) rowData.numeration_column = rowNum+this.store.rowsStart;
					if(isDefined(rowData[columnName])) fieldValue = rowData[columnName];
					styleClasses += 'containing-t-row ';
				}
				else {
					styleClasses += 'empty-t-row ';
				}
				
				var fieldId = this.gridUniqueId+'_'+columnName+'_field'+rowNum;
				var columnElem = this.findColumnElemByName(columnName);				

				$("#"+rowId).append(
					'<td ' 
						+'id="'+fieldId+'" '
						+'row-id="'+rowId+'" '
						+'row-num="'+rowNum+'" '
						+'col-id="'+columnElem.attr("id")+'" '
						+'col-name="'+columnName+'" '
						+'col-hidden="'+columnHidden+'" '
						+(styleClasses ? 'class="'+styleClasses+'" ' : '')
					+'>'
						+fieldValue
					+'</td>'
				);

				if(columnHidden) $("#"+fieldId).hide();
			};

			$("#"+rowId).click($.proxy(function (event) {
				var target = $(event.delegateTarget);
				
				this.clearSelectedRow();
				if(booleanCheckAdv($(target).attr("containing-row"))) {
					$(target).addClass("grid-table-selected-row").attr("selected-row", true);
				}

				if(this.selectionChangeEvent) $.proxy(this.selectionChangeEvent(),this);
			},this));
		};

		this.loadBottomToolBar();

		if(this.storeLoadEvent) $.proxy(this.storeLoadEvent(),this);
		if(this.selectionChangeEvent) $.proxy(this.selectionChangeEvent(),this);
	};

	// ----------------------------------------------
	this.loadBottomToolBar = function() {
		if(!this.bottomToolBarElem) return false;
		this.bottomToolBarElem.empty();

		if(!this.store) return false;

		countsInfoHtml = '';

		if(this.store.rowsCount) {
			this.store.maxPageNum = parseInt(this.store.rowsCount/this.store.rowsLimit);
			if((this.store.rowsCount%this.store.rowsLimit) != 0) this.store.maxPageNum++;
		}
		else this.resetStoreParams();

		this.bottomToolBarElem.append(
			'<div class="btb-div btb-navigation-pages">'
				+'<button type="button" class="btn btbnp" name="btb_double_right">'
					+'<i class=" fas fa-angle-double-right"></i>'
				+"</button>"

				+'<button type="button" class="btn btbnp" name="btb_single_right">'
					+'<i class="fas fa-caret-right"></i>'
				+"</button>"				
				+'עמוד'
				+'<input type="text" '
					+'class="btbnp btbnp-page-input form-control input-sm" '
					+'id="'+this.gridUniqueId+'_page_input" '
					+'value="'+this.store.currentPageNum+'"'
				+'>'

				+'מתוך '+this.store.maxPageNum

				+'<button type="button" class="btn btbnp" name="btb_single_left">'
					+'<i class="fas fas fa-caret-left"></i>'
				+"</button>"

				+'<button type="button" class="btn btbnp" name="btb_double_left">'
					+'<i class="fas fas fa-angle-double-left"></i>'
				+"</button>"
			+'</div>'
			+'<div class="btb-div btb-counts-info">'
				+'<b>'
					+'מציג '+(this.store.rowsCount < this.store.rowsLimit ? this.store.rowsCount : this.store.rowsLimit)
					+' מתוך '+this.store.rowsCount
					+' סה"כ'
				+'</b>'
			+'</div>'
		);
		

		this.bottomToolBarElem.find('button[name="btb_single_right"]')
			.click($.proxy(function () {
				var newValue = (this.store.currentPageNum-1);
				if(newValue>0) {
					this.store.currentPageNum = newValue;
					this.store.rowsStart -= this.store.rowsLimit;
					this.loadStore({resetStoreParams:false});
				}
			},this))
			.prop("disabled", (this.store.currentPageNum == 1));

		this.bottomToolBarElem.find('button[name="btb_double_right"]')
			.click($.proxy(function () {
				this.store.currentPageNum = 1;
				this.store.rowsStart = 0;
				this.loadStore({resetStoreParams:false});
			},this))
			.prop("disabled", (this.store.currentPageNum == 1));
		
		this.bottomToolBarElem.find('button[name="btb_single_left"]')
			.click($.proxy(function () {
				var newValue = (this.store.currentPageNum+1);
				if(newValue<=this.store.maxPageNum) {
					this.store.currentPageNum = newValue;
					this.store.rowsStart += this.store.rowsLimit;
					this.loadStore({resetStoreParams:false});
				}
			},this))
			.prop("disabled", (this.store.maxPageNum == this.store.currentPageNum));

		this.bottomToolBarElem.find('button[name="btb_double_left"]')
			.click($.proxy(function () {
				this.store.currentPageNum = this.store.maxPageNum;
				this.store.rowsStart = (this.store.rowsLimit*(this.store.currentPageNum-1));
				this.loadStore({resetStoreParams:false});
			},this))
			.prop("disabled", (this.store.maxPageNum == this.store.currentPageNum));
		
		pageNumInput = this.bottomToolBarElem.find('#'+this.gridUniqueId+'_page_input');

		pageNumInput.on('input', $.proxy(function() {
				inputValue = parseInt(getNumberFromStr(pageNumInput.val()));

				if(!inputValue || inputValue<0) inputValue = 1;
				if(inputValue>this.store.maxPageNum) inputValue = this.store.maxPageNum;

				pageNumInput.val(inputValue);
			},this))
			.focusout($.proxy(function () {
				inputValue = pageNumInput.val();
				
				if(inputValue==this.store.currentPageNum) return;

				this.store.currentPageNum = inputValue;
				this.store.rowsStart = (this.store.rowsLimit*(this.store.currentPageNum-1));

				this.loadStore({resetStoreParams:false});
			},this));
	};

	// ----------------------------------------------
	this.setTopToolBarFields = function() {
		if(!this.topToolBarFieldsParams) return false;		

		$.each(this.topToolBarFieldsParams, $.proxy(function (index, fieldData) {
			fieldData.attachToStoreLoad = setDefaultWhenUndefined(fieldData.attachToStoreLoad, false);
			fieldData.scope = setDefaultWhenUndefined(fieldData.scope, null, 'checkIfEmpty');
			fieldData.cls = setDefaultWhenUndefined(fieldData.cls, '', 'checkIfEmpty');
			
			if(!fieldData.scope) fieldData.scope = this;
			if(isDefined(fieldData.iconCls) && isString(fieldData.iconCls)) fieldData.iconCls += ' btn-fa-icon-cls';

			fieldData.parentElem = this.topToolBarElem;
			fieldData.fieldId = 'tool_bar_field_'+this.gridUniqueId+index;
			fieldData.cls = fieldData.cls+' top-tool-bar-field';
			fieldData.moreAttr = {'attach-to-store-load':fieldData.attachToStoreLoad};
			fieldData.iconOnsetElem = true;
			
			new FormFieldCreator(fieldData);
		},this));
	};

	// ----------------------------------------------
	this.setTopToolBarSearchField = function() {
		if(!this.topToolBarSearchParams) return false;

		var searchData = this.topToolBarSearchParams;

		this.topToolBarSearchParams.minChars = setDefaultWhenUndefined(this.topToolBarSearchParams.minChars, 3);
		this.topToolBarSearchParams.searchTimeout = setDefaultWhenUndefined(this.topToolBarSearchParams.searchTimeout, 0);

		searchData.placeholder = setDefaultWhenUndefined(searchData.placeholder, "חיפוש");
		searchData.cls = setDefaultWhenUndefined(searchData.cls, '', 'checkIfEmpty');
		searchData.events = setDefaultWhenUndefined(searchData.events, {}, 'checkIfEmpty');

		searchData.parentElem = this.topToolBarElem;
		searchData.category = 'input';
		searchData.fieldId = 'search_field'+this.gridUniqueId;
		searchData.name = 'search';
		searchData.type = 'text';
		searchData.cls = searchData.cls+' top-tool-bar-field';
		searchData.placeholder = this.topToolBarSearchParams.placeholder;
		searchData.moreAttr = {'currSearchVal':''}
		searchData.scope = this;
		
		if(isUndefined(searchData.events.input)) {
			searchData.events.input = function () {	
				var inputValue = $.trim(this.searchFieldElem.val());
				var currSearchVal = $.trim(this.searchFieldElem.attr("currSearchVal"));

				if(inputValue.length < this.topToolBarSearchParams.minChars) inputValue = '';
				if(inputValue==currSearchVal) return;

				this.searchFieldElem.attr("currSearchVal", inputValue);

				setTimeout($.proxy(function () {
					this.loadStore();
				},this), this.topToolBarSearchParams.searchTimeout);

				setTimeout($.proxy(function () {
					this.topToolBarSearchParams.searchTimeout = 0;
				},this), 3000);

				this.topToolBarSearchParams.searchTimeout += 200;
			};
		}

		this.searchFieldElem = new FormFieldCreator(searchData);
	};

	// ----------------------------------------------
	this.resetStoreParams = function() {
		if(!this.store) return false;

		this.store.rowsStart = 0;
		this.store.rowsCount = 0;
		this.store.currentPageNum = 1;
		this.store.maxPageNum = 1;
	};

	// ----------------------------------------------
	this.setEmptyStoreMsg = function() {
		this.gridBodyElem.empty();
		this.gridBodyElem.append(this.emptyStoreMsg);
	};

	// ----------------------------------------------
	this.getSelectedRowElem = function() {
		if(!this.gridElem) return false;

		var elem = this.gridElem.find('tr[selected-row="true"]');
		
		if(elem.length==1) return elem;
		else return false;
	};

	// ----------------------------------------------
	this.getSelectedRowData = function() {
		if(isUndefined(this.store.data) || !this.store.data.length) return null;

		var selectedRowElem = this.getSelectedRowElem();
		if(!selectedRowElem) return null;
		
		var rowNum = selectedRowElem.attr('row-num');
		if(isUndefined(rowNum) || !rowNum || isUndefined(this.store.data[rowNum-1])) return null;

		return this.store.data[rowNum-1];
	};

	// ----------------------------------------------
	this.findTopToolBarFieldByName = function(fieldName) {
		if(!this.topToolBarElem || isUndefined(fieldName) || !fieldName) return false;

		var elem = this.topToolBarElem.find('[name="'+fieldName+'"]');

		if(elem.length) return elem;
		else return false; 
	}

	// ----------------------------------------------
	this.findColumnElemByName = function(columnName, gridHeadElement) {
		if(isDefined(gridHeadElement)) this.gridHeadElem = gridHeadElement;
		
		var elem = this.gridHeadElem.find('th[col-name="'+columnName+'"]');

		if(elem.length) return elem;
		else return false; 
	};

	// ----------------------------------------------
	this.findColumnDataByName = function(columnName, gridHeadElement) {		
		var thElem = this.findColumnElemByName(columnName, gridHeadElement);

		return getElementAttributes(thElem);
	};

	// ----------------------------------------------
	this.clearSelectedRow = function() {
		if(!this.gridElem) return false;

		$.each(this.gridElem.find('tr[elem-type="tableRow"]'), function( key, el ) {
			$(this).removeClass('grid-table-selected-row').removeAttr("selected-row");
		});
	};

	// ----------------------------------------------
	this.clearGridData = function() {
		this.gridHeadElem.empty();
		this.gridBodyElem.empty();
	};

	if(this.autoLoadGrid) this.createGrid();
}
