// ----------------------------------------------
function getGymsListStore() {
	// ----- gyms combo params ----- // 
	if(isDefined(SVARS) && isDefined(SVARS.gyms_list)) {
		console.log(SVARS);

		return generateSelectBoxStore({
			data:SVARS.gyms_list, 
			valueField:'gym_id', 
			textField:'name',
		});
	}

	return null;
}

// ----------------------------------------------
function getGymsBranchesListStore() {
	// ----- gyms branches combo params ----- // 
	if(isDefined(SVARS) && isDefined(SVARS.gyms_branches_list)) {
		return generateSelectBoxStore({
			data:SVARS.gyms_branches_list, 
			valueField:'gym_branch_id', 
			textField:'name'
		});
	}

	return null;
}

// ----------------------------------------------
function getUsersTypesListStore() {
	var userTypes = getUsersTypes();

	// ----- user type combo params ----- // 
	if(userTypes && userTypes.by_name) {
		return generateSelectBoxStore({
			data:userTypes.by_name, 
			valueField:'code', 
			textField:'name_heb'
		});
	}

	return null;
}

// ----------------------------------------------
function getCommunicationsTypesListStore() {
	// ----- communications types combo params ----- // 
	if(	
		isDefined(SVARS) && 
		isDefined(SVARS.communications_types) && 
		isDefined(SVARS.communications_types.by_name)
	) {
		return generateSelectBoxStore({
			data:SVARS.communications_types.by_name, 
			valueField:'code', 
			textField:'name_heb'
		});
	}

	return null;
}

// ----------------------------------------------
function getAddressesTypesListStore() {
	// ----- addresses types combo params ----- // 
	if(	
		isDefined(SVARS) && 
		isDefined(SVARS.addresses_types) && 
		isDefined(SVARS.addresses_types.by_name)
	) {
		return generateSelectBoxStore({
			data:SVARS.addresses_types.by_name, 
			valueField:'code', 
			textField:'name_heb'
		});
	}

	return null;
}

// ----------------------------------------------
function getGymsDefaultValue() {
	var returnGymId = 0;

	var userData = getUserData();
	if(!userData) {
		openMsgModal({msgText:"שגיאה! [getGymsDefaultValue-userData]", msgType:'error'});
		return returnGymId;
	}

	if(!isAdmin()) {
		$.each(SVARS.gyms_list, function( key, el ) {
			if(el.gym_id==userData.gym_id) {
				returnGymId = userData.gym_id;
				return false;
			}
		});
	}

	return returnGymId;
}

// ----------------------------------------------
function getGymsBranchesDefaultValue() {
	var returnGymBranchId = 0;

	var userData = getUserData();
	if(!userData) {
		openMsgModal({msgText:"שגיאה! [getGymsDefaultValue-userData]", msgType:'error'});
		return returnGymBranchId;
	}

	if(!isManagerUser() && !isAdmin()) {
		$.each(SVARS.gyms_branches_list, function( key, el ) {
			if(el.gym_branch_id==userData.gym_branch_id) {
				returnGymBranchId = userData.gym_branch_id;
				return false;
			}
		});
	}

	return returnGymBranchId;
}

// ----------------------------------------------
function getCommunicationsTypesDefaultValue() {
	if(	
		isDefined(SVARS) && 
		isDefined(SVARS.communications_types) && 
		isDefined(SVARS.communications_types.by_name) &&
		isDefined(SVARS.communications_types.by_name['other']) 
	){
		return SVARS.communications_types.by_name['other'].code;
	}
}

// ----------------------------------------------
function getAddressesTypesDefaultValue() {
	if(	
		isDefined(SVARS) && 
		isDefined(SVARS.addresses_types) && 
		isDefined(SVARS.addresses_types.by_name) &&
		isDefined(SVARS.addresses_types.by_name['other']) 
	){
		return SVARS.addresses_types.by_name['other'].code;
	}
}

// ----------------------------------------------
function getDateTimePickerDefaultOptions() {
	return {
		format: 'DD/MM/YYYY',
		locale:'he',
		widgetPositioning: {
			horizontal: 'right',
			vertical: 'auto'
		},
		icons:{
            time: 'glyphicon glyphicon-time',
            date: 'glyphicon glyphicon-calendar',
            up: 'glyphicon glyphicon-chevron-up',
            down: 'glyphicon glyphicon-chevron-down',
            previous: 'glyphicon glyphicon-chevron-right',
            next: 'glyphicon glyphicon-chevron-left',
            today: 'glyphicon glyphicon-screenshot',
            clear: 'glyphicon glyphicon-trash',
            close: 'glyphicon glyphicon-remove'
        }
	};
}

// ----------------------------------------------
function setEventToElem(properties) {
	if(isUndefined(properties) || !properties) return false;

	properties.eventName = $.trim(setDefaultWhenUndefined(properties.eventName, '', 'checkIfEmpty'));
	properties.function = setDefaultWhenUndefined(properties.function, null, 'checkIfEmpty');
	properties.scope = setDefaultWhenUndefined(properties.scope, null, 'checkIfEmpty');
	properties.elem = setDefaultWhenUndefined(properties.elem, null, 'checkIfEmpty');
	
	if(!properties.elem || !properties.elem.length) return false;
	if(!properties.eventName || !properties.function) return false;
	if(!isFunction(properties.function) || !isString(properties.eventName)) return false;

	if(properties.scope) properties.function = $.proxy(properties.function, properties.scope);
	
	properties.elem.on(properties.eventName, properties.function);

	return true;
}

// ----------------------------------------------
function setElementsValue(obj) {
	if(isUndefined(obj) || !obj) return false;
	
	$.each(obj, function( obKey, obRow ) {
		if(isUndefined(obRow.elem) || isUndefined(obRow.val) || !obRow.elem || !obRow.elem.length) return;
		setFormFieldValue(obRow.elem, obRow.val);
	});

	return true;
}

// ----------------------------------------------
function idNumberValidation(idNumber) {
	if(isUndefined(idNumber) || !idNumber) return false;

	idNumber = String(idNumber);
	if(!idNumber || !idNumber.length || idNumber.length>9) return false;

	lengthDifference = 9 - idNumber.length;
	if(lengthDifference > 0) {
		for (var i = 0; i < lengthDifference; i++) {
			idNumber = "0"+idNumber;
		}
	}

	var mulNum = 1;
	var totalSum = 0;

	for (var i = 0; i < 9; i++) {
		if(!isNumber(idNumber[i])) return false;
		currSum = parseInt(idNumber[i]) * mulNum;
		
		if(currSum >= 10) {
			numberForSeparation = String(currSum);			
			if(numberForSeparation.length > 2) return false;
			
			currSum = parseInt(numberForSeparation[0]) + parseInt(numberForSeparation[1]);
		}
		
		totalSum += currSum;
		
		if(mulNum==1) mulNum = 2;
		else mulNum = 1;
	}

	if((totalSum%10) != 0) return false;
	return true;
}