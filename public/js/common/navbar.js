$( document ).ready(function() {
	var userData = getUserData();
	var modules = getModules();

	if(userData && modules) {
		$('#mainNavbarNavTag').show();

		$.each(modules, function (moduleIndex, moduleData) {
			if(!checkIfModuleAllowed(userData.type_code, moduleData.allowed_users_types)) return;
			addModuleToNavbar(moduleData, '');

			if(isDefined(moduleData.children)) {
				$.each(moduleData.children, function(childModuleIndex, childModuleData) {
					if(!checkIfModuleAllowed(userData.type_code, childModuleData.allowed_users_types)) return;
					addModuleToNavbar(childModuleData, moduleData.name_eng);
				});
			}
		});

		$.each($('#mainNavbar li'), function() {
			var moduleName = $(this).attr("module_name");
			var parentName = $(this).attr("parent_name");
			var moduleType = $(this).attr("module_type");

			if(moduleName=='logout' && !parentName) {
				$(this).css({'float':'left'});
			}

			$(this).click(function() {		
				if(moduleName && moduleType!='parent') {
					showPreloader();
					window.location.replace("/"+moduleName);
				}
			});
		});
	}
});

// ----------------------------------------------
function addModuleToNavbar(moduleData, parentElemName) {
	var activeClass = (moduleData.name_eng == currentModuleName ? 'active' : '');

	if(isDefined(moduleData.children)) {
		htmlStr = 
			'<li '
				+'module_type="parent" '
				+'module_name="'+moduleData.name_eng+'" '
				+'class="dropdown '+activeClass+'"'
			+'>'
				+'<a class="dropdown-toggle" data-toggle="dropdown" href="#">'
					+'<i class="'+moduleData.icon_cls+' main-navbar-icon-btn"></i>'
					+moduleData.name_heb
					+'<span class="caret" style="margin-right:5px;"></span>'
				+'</a>'
				+'<ul class="dropdown-menu"></ul>'
			+'</li>';
	}
	else {
		htmlStr = 
			'<li module_name="'+moduleData.name_eng+'" class="'+activeClass+'">'
				+'<a href="#">'
					+'<i class="'+moduleData.icon_cls+' main-navbar-icon-btn"></i>'
					+moduleData.name_heb
				+'</a>'
			+'</li>';
	}

	if(parentElemName) {
		$('#mainNavbar li[module_name="'+parentElemName+'"]').addClass(activeClass);
		$('#mainNavbar li[module_name="'+parentElemName+'"] .dropdown-menu').append(htmlStr);

		$('#mainNavbar li[module_name="'+moduleData.name_eng+'"] .main-navbar-icon-btn').css({
			'width':'25px',
			'margin-left':'0px'
		});
		
		$('#mainNavbar li[module_name="'+moduleData.name_eng+'"]').attr({
			'parent_name':parentElemName
		});
	}
	else {
		$('#mainNavbar .navbar-nav').append(htmlStr);
	}
}

// ----------------------------------------------
function checkIfModuleAllowed(userTypeCode, allowedUsersTypes) {
	if(isUndefined(userTypeCode) || !userTypeCode || !parseInt(userTypeCode)) return false;
	if(isUndefined(allowedUsersTypes) || !allowedUsersTypes || !objLength(allowedUsersTypes)) return false;

	userTypeCode = parseInt(userTypeCode);
	var isAllowed = false;

	$.each(allowedUsersTypes, function(tIndex, tCode) {
		if(userTypeCode == parseInt(tCode)) isAllowed = true;
	});

	return isAllowed;
}