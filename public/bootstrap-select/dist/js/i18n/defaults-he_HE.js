/*!
 * Bootstrap-select v1.12.4 (https://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2018 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define(["jquery"], function (a0) {
      return (factory(a0));
    });
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require("jquery"));
  } else {
    factory(root["jQuery"]);
  }
}(this, function (jQuery) {

(function ($) {
  $.fn.selectpicker.defaults = {
    noneSelectedText: 'כלום לא נבחר',
    noneResultsText: 'לא אותרו התאמות {0}',
	countSelectedText: function (numSelected, numTotal) {
      return (numSelected == 1) ? "{0} נבחר" : "{0} נבחרו";
    },
    maxOptionsText: ['הגעת לקמות המירבית ({n} {var} מקסימום)', 'הגעת לקמות המירבית בקבוצה ({n} {var} מקסימום)', ['יח.', 'יח.']],
    doneButtonText: 'סגור',
    selectAllText: 'בחר הכל',
    deselectAllText: 'סמן הכל',    
    multipleSeparator: ', '
  };
})(jQuery);


}));
