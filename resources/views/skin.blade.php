<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Life Fit</title>

	<!-- Packages (bootstrap, fontawesome, bootstrap, summernote) CSS files includes -->
	<link rel="stylesheet" type="text/css" href="/bootstrap-3.3.7/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/bootstrap-3.3.7/dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="/fontawesome-5.0.13/web-fonts-with-css/css/fontawesome-all.min.css">
	<link rel="stylesheet" type="text/css" href="/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" type="text/css" href="/summernote-0.8.9/dist/summernote.css">

	<!-- Global system CSS files includes -->
	<link rel="stylesheet" type="text/css" href="/css/main/main.css">
	<link rel="stylesheet" type="text/css" href="/css/common/modals.css">
	<link rel="stylesheet" type="text/css" href="/css/common/grids.css">
	<link rel="stylesheet" type="text/css" href="/css/common/forms.css">
	
	<!-- Packages (jquery, bootstrap, jssor, bootstrap, summernote) JS files includes -->
	<script type="text/javascript" src="/jquery-3.3.1/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="/moment-develop/min/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="/bootstrap-3.3.7/js/transition.js"></script>
	<script type="text/javascript" src="/bootstrap-3.3.7/js/collapse.js"></script>
	<script type="text/javascript" src="/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="/summernote-0.8.9/dist/summernote.js"></script>
	<script type="text/javascript" src="/summernote-0.8.9/dist/lang/summernote-he-IL.js"></script>
	<script type="text/javascript" src="/jssor.slider/js/jssor.slider.min.js"></script>

	<!-- Global system JS files includes -->
	<script type="text/javascript" src="/js/common/common_main.js"></script>
	<script type="text/javascript" src="/js/common/common_ext.js"></script>
	<script type="text/javascript" src="/js/common/modals.js"></script>
	<script type="text/javascript" src="/js/common/grids.js"></script>
	<script type="text/javascript" src="/js/common/forms.js"></script>
</head>
<body>
	<!-- Set Global JS 'SVARS' variable  -->
	@if(isset($SVARS))
		<script>
			SVARS = {!!$SVARS!!};
		</script>
	@endif

	<!-- Set Global JS 'SharedMessagesList' variable  -->
	@if(isset($SharedMessagesList))
		<script>
			SharedMessagesList = {!!$SharedMessagesList!!};
		</script>
	@endif

	<!-- Include navbar menu blade code  -->
	@include('navbar.navbar')

	<!-- Main content - variable dynamically  -->
	<div id="main_body_content">
	@yield('content')
	</div>
	
</body>
</html>
