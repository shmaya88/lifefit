<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
@include('modules.gyms.gyms_css')
@include('modules.gyms.gyms_branches_css')

<script>
	currentModuleName = 'gyms_branches';
</script>

@include('modules.gyms.gyms_js')
@include('modules.gyms.gyms_branches_js')

<!-- Module grid panel -->
<div id="gyms_branches_list_grid" class="main-modules-grids-s">
	<div class="panel-heading main-module-title">
		<h3 class="panel-title">
			<i class="fas fa-code-branch"></i>
			רשימת סניפי רשת
		</h3>
	</div>
</div>

<!-- End skin file 'content' section -->
@endsection
