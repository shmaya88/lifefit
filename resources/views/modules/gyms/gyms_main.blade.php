<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
@include('modules.gyms.gyms_css')

<script>
	currentModuleName = 'gyms';
</script>

@include('modules.gyms.gyms_js')

<!-- Module grid panel -->
<div id="gyms_list_grid" class="main-modules-grids-s">
	<div class="panel-heading main-module-title">
		<h3 class="panel-title">
			<i class="fas fa-copyright"></i>
			רשימת חברות/רשתות מכוני כושר
		</h3>
	</div>
</div>

<!-- End skin file 'content' section -->
@endsection
