<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
@include('modules.support.support_css')

<script>
	currentModuleName = 'support/notifications_services';
</script>

@include('modules.support.support_js')

<!-- Module grid panel -->
<div id="notifications_services_list_grid" class="main-modules-grids-s">
	<div class="panel-heading main-module-title">
		<h3 class="panel-title">
			<i class="fas fa-comment"></i>
			רשימת הודעות שירות
		</h3>
	</div>
</div>

<!-- End skin file 'content' section -->
@endsection
