<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
@include('modules.support.support_css')

<script>
	currentModuleName = 'support/support_page';
</script>

@include('modules.support.support_js')

<!-- Module grid panel -->
<div id="support_page_panel" class="main-modules-grids-s">
	<div class="form-row">
		<div class="form-group col-md-12">
			<div style="font-size:12px;">
				<br>
				<span class="contactus-title mrg-r-15">ליצירת קשר</span>
				<br><br>
				<div class="form-row">
					<div class="form-group col-md-4">
						<span>טלפון: 074-7144444</span>
						<br>
						<span>פקס: 073-7144500</span>
						<br>
						<span>דוא"ל: support@lifefit.co.il</span>
					</div>
					<div class="form-group col-md-4">
						<span>כתובת:</span>
						<br>
						<span>מעלה יצחק 1, נצרת עילית</span>
						<br>
						<span>ת.ד 12547, מיקוד 17001</span>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<span>ניתן להשאיר פניית שירות בעזרת הטופס המצורף.</span>
						<span>
							<br><br>
							אנו עושים לילות כימים, על מנת לשכלל ולשפר את ביצועי תוכנת LIFEFIT ולכן בקשתנו ממך 
							<br>
							לציין ולכתוב
							באופן ברור את מהות ההודעה על מנת שנטפל בה במהירות האפשרית.<br><br>
							תודה רבה מצוות האתר
						</span>
						<br>
						<span>נשמח לעמוד לשירותכם.</span>
						<br>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="support_page_footer_form" class="form-row"></div>
</div>

<!-- End skin file 'content' section -->
@endsection
