<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
@include('modules.shared_messages.shared_messages_css')
@include('modules.gyms.gyms_css')

<script>
	currentModuleName = 'shared_messages';
</script>

@include('modules.shared_messages.shared_messages_js')
@include('modules.gyms.gyms_js')

<!-- Module grid panel -->
<div id="shared_messages_list_grid" class="main-modules-grids-s">
	<div class="panel-heading main-module-title">
		<h3 class="panel-title">
			<i class="fas fa-comments"></i>
			הודעות בדף הבית
		</h3>
	</div>
</div>

<!-- End skin file 'content' section -->
@endsection
