<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
@include('modules.subscribers.subscribers_css')
@include('modules.users.users_css')
@include('modules.training.training_css')
@include('modules.gyms.gyms_css')
@include('modules.gyms.gyms_branches_css')

<script>
	currentModuleName = 'subscribers';
</script>

@include('modules.subscribers.subscribers_js')
@include('modules.users.users_js')
@include('modules.training.training_js')
@include('modules.gyms.gyms_js')
@include('modules.gyms.gyms_branches_js')

<!-- Module grid panel -->
<div id="subscribers_list_grid" class="main-modules-grids-s">
	<div class="panel-heading main-module-title">
		<h3 class="panel-title">
			<i class="fas fa-address-card"></i>
			רשימת מנויים
		</h3>
	</div>
</div>

<!-- End skin file 'content' section -->
@endsection
