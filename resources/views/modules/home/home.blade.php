<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
<script src="/js/modules/home/home_main.js"></script>

<link href="/css/modules/home/home_main.css" rel="stylesheet">

<script>
	currentModuleName = 'home_page';
</script>

<!-- Module panel -->
<div id="home_page_panel" class="main-modules-grids-s">
	<div id="home_page_welcome_block"></div>

	<!-- Home page slide bar HTML tags -->
	<div id="home_page_slide_bar" style="">
		<!-- Loading Screen -->
		<div data-u="loading" 
			class="jssorl-009-spin" 
			style=
				"position:absolute;top:0px;left:0px;width:100%;height:100%;
				text-align:center;background-color:rgba(0,0,0,0.7);"
			>
			<img 
				style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" 
				src="/images/slider_banners/spin.svg" 
			/>
		</div>
		<div data-u="slides"></div>
		<!-- Bullet Navigator -->
		<div 
			data-u="navigator" 
			class="jssorb052" 
			style="position:absolute;bottom:12px;right:12px;" 
			data-autocenter="1" 
			data-scale="0.5" 
			data-scale-bottom="0.75"
		>
			<div data-u="prototype" class="i" style="width:16px;height:16px;">
				<svg 
					viewbox="0 0 16000 16000" 
					style="position:absolute;top:0;left:0;width:100%;height:100%;"
				>
					<circle class="b" cx="8000" cy="8000" r="5800"></circle>
				</svg>
			</div>
		</div>
		<!-- Arrow Navigator -->
		<div 
			data-u="arrowleft" 
			class="jssora053" 
			style="width:55px;height:55px;top:0px;left:0px;" 
			data-autocenter="2" 
			data-scale="0.75" 
			data-scale-left="0.75"
		>
			<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
				<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
			</svg>
		</div>
		<div 
			data-u="arrowright" 
			class="jssora053" 
			style="width:55px;height:55px;top:0px;right:0px;" 
			data-autocenter="2" 
			data-scale="0.75" 
			data-scale-right="0.75"
		>
			<svg 
				viewbox="0 0 16000 16000" 
				style="position:absolute;top:0;left:0;width:100%;height:100%;"
			>
				<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
			</svg>
		</div>
	</div>
</div>

<!-- Footer -->
<footer id="copyright_footer">Lifefit.co.il © 2018</footer>

<!-- End skin file 'content' section -->
@endsection
