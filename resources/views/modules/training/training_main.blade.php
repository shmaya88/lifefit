<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
@include('modules.training.training_css')

<script>
	currentModuleName = 'training';
</script>

@include('modules.training.training_js')

<!-- Module grid panel -->
<div id="training_list_grid" class="main-modules-grids-s">
	<div class="panel-heading main-module-title">
		<h3 class="panel-title">
			<i class="fas fa-dumbbell"></i>
			רשימת תבניות אימונים
		</h3>
	</div>
</div>

<!-- End skin file 'content' section -->
@endsection
