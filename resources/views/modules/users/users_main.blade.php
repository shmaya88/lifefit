<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
@include('modules.users.users_css')
@include('modules.gyms.gyms_css')
@include('modules.gyms.gyms_branches_css')

<script>
	currentModuleName = 'users';
</script>

@include('modules.users.users_js')
@include('modules.gyms.gyms_js')
@include('modules.gyms.gyms_branches_js')

<!-- Module grid panel -->
<div id="users_list_grid" class="main-modules-grids-s">
	<div class="panel-heading main-module-title">
		<h3 class="panel-title">
			<i class="fas fa-user-friends"></i>
			רשימת משתמשים
		</h3>
	</div>
</div>

<!-- End skin file 'content' section -->
@endsection
