<!-- Extends skin file code -->
@extends('skin')

<!-- Start skin file 'content' section -->
@section('content')

<!-- CSS & JS files includes -->
<link href="/css/start_page/login_form.css" rel="stylesheet">

<script src="/js/start_page/sp_main_script.js"></script>

<!-- Start page nav bar HTML tags -->
<nav id="startPageNavbarNavTag" class="navbar navbar-inverse">
	<form id="start-page-form-main" class="form-row" style="width: 40%;"></form>
	<img src="/images/logos/life-fit-logo-white1.png" style="height:81px; float:left;">
</nav>

<!-- Start page slide bar HTML tags -->
<div id="start_page_slide_bar" 
	style=
		"position:relative;margin:0 auto;top:0px;left:0px;
		width:980px;height:380px;overflow:hidden;visibility:hidden;"
>
	<!-- Loading Screen -->
	<div data-u="loading" 
		class="jssorl-009-spin" 
		style=
			"position:absolute;top:0px;left:0px;width:100%;height:100%;
			text-align:center;background-color:rgba(0,0,0,0.7);"
	>
		<img 
			src="/images/slider_banners/spin.svg" 
			style=
				"margin-top:-19px;position:relative;
				top:50%;width:38px;height:38px;" 
		/>
	</div>
	<div 
		data-u="slides" 
		style=
			"cursor:default;position:relative;top:0px;
			left:0px;width:980px;height:380px;overflow:hidden;"
	>
		<div>
			<img data-u="image" src="/images/slider_banners/clients_api_banner.jpg" />
		</div>
		<div>
			<img data-u="image" src="/images/slider_banners/import_db_banner.jpg" />
		</div>
	</div>
	<!-- Bullet Navigator -->
	<div 
		data-u="navigator" 
		class="jssorb052" 
		style="position:absolute;bottom:12px;right:12px;" 
		data-autocenter="1" 
		data-scale="0.5" 
		data-scale-bottom="0.75"
	>
		<div data-u="prototype" class="i" style="width:16px;height:16px;">
			<svg 
				viewbox="0 0 16000 16000" 
				style="position:absolute;top:0;left:0;width:100%;height:100%;"
			>
				<circle class="b" cx="8000" cy="8000" r="5800"></circle>
			</svg>
		</div>
	</div>
	<!-- Arrow Navigator -->
	<div 
		data-u="arrowleft" 
		class="jssora053" 
		style="width:55px;height:55px;top:0px;left:0px;" 
		data-autocenter="2" 
		data-scale="0.75" 
		data-scale-left="0.75"
	>
		<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
			<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
		</svg>
	</div>
	<div 
		data-u="arrowright" 
		class="jssora053" 
		style="width:55px;height:55px;top:0px;right:0px;" 
		data-autocenter="2" 
		data-scale="0.75" 
		data-scale-right="0.75"
	>
		<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
			<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
		</svg>
	</div>
</div>

<!-- Footer -->
<footer id="copyright_footer">Lifefit.co.il © 2018</footer>

<!-- End skin file 'content' section -->
@endsection
