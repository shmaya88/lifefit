<link href="/css/navbar/navbar.css" rel="stylesheet">

<script src="/js/common/navbar.js"></script>

<nav id="mainNavbarNavTag" class="navbar navbar-inverse">
	<div class="container-fluid buttons-menu">
		<!-- <a class="navbar-brand" href="/home_page"> -->
			<img class='navbar-brand-img' src="/images/logos/life-fit-logo-white1.png">
		<!-- </a> -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                      
			</button>
		</div>
		<div id="mainNavbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
	</div>
</nav>